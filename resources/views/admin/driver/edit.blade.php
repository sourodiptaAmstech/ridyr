@extends('admin.layout.base')

@section('title', 'Update Driver ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
    	    <a href="{{ route('admin.driver.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Update Driver</h5>

            <form class="form-horizontal" action="{{route('admin.driver.update', $driver->_id )}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
            	<input type="hidden" name="_method" value="PATCH">
				<div class="form-group row">
					<label for="first_name" class="col-xs-2 col-form-label">First Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $driver->driver_profile['first_name'] }}" name="first_name" required id="first_name" placeholder="First Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="last_name" class="col-xs-2 col-form-label">Last Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $driver->driver_profile['last_name'] }}" name="last_name" required id="last_name" placeholder="Last Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="email" class="col-xs-2 col-form-label">Email</label>
					<div class="col-xs-10">
						<input class="form-control" type="email" value="{{ $driver->username }}" name="email" required id="email" placeholder="Email">
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-2 col-form-label">Picture</label>
					<div class="col-xs-10">
					@if($driver->driver_profile['picture'] != '')
                    	@if(File::exists(storage_path('app/public' .str_replace("storage", "", $driver->driver_profile['picture']))))
                            <img style="height: 90px; margin-bottom: 15px;" src="{{URL::asset($driver->driver_profile['picture'])}}">
                        @else
                            <img src="{{$driver->driver_profile['picture']}}" style="height: 60px; margin-bottom: 15px;">
                        @endif
                    @else
                     	<img style="height: 60px; margin-bottom: 15px;" src="{{URL::asset('asset/NO_IMG.png')}}">
                    @endif
						<input type="file" accept="image/*" name="picture" class="dropify form-control-file" id="picture" aria-describedby="fileHelp">
					</div>
				</div>

				<div class="form-group row">
					<label for="mobile" class="col-xs-2 col-form-label">Mobile</label>
					<div class="col-xs-10">
						<input class="form-control" type="tel" value="{{ $driver->driver_profile['mobile'] }}" name="mobile" id="mobile" required>
						<input type="hidden" name="code" id="code" value="{{$driver->driver_profile['isdCode']}}">
					</div>
				</div>

				<div class="form-group row">
					<label for="dob" class="col-xs-2 col-form-label">Date of Birth</label>
					<div class="col-xs-10">
						<input class="form-control" type="date" name="dob" value="{{ $driver->driver_profile['dob'] }}" id="dob">
					</div>
				</div>
				<div class="form-group row">
					<label for="gender" class="col-xs-2 col-form-label">Gender</label>
					<div class="col-xs-10">
						<input type="radio" id="male" name="gender" value="male" {{ $driver->driver_profile['gender'] == 'male' ? 'checked' : ''}}>
						<label for="male">Male</label><br>
						<input type="radio" id="female" name="gender" value="female" {{ $driver->driver_profile['gender'] == 'female' ? 'checked' : ''}}>
						<label for="female">Female</label><br>
					</div>
				</div>

		{{-- 	@if($driver->driver_service)
				<div class="form-group row">
					<label for="car_make" class="col-xs-2 col-form-label">Car Brand</label>
					<div class="col-xs-10">
						<select name="car_make" id="car_make" class="form-control">
							<option value="">Select Car Brand</option>
							<option value="AC" {{ $driver->driver_service->car_make == 'AC' ? 'selected' : '' }}>AC</option>
							<option value="Acura" {{ $driver->driver_service->car_make == 'Acura' ? 'selected' : '' }}>Acura</option>
							<option value="Alfa Romeo" {{ $driver->driver_service->car_make == 'Alfa Romeo' ? 'selected' : '' }}>Alfa Romeo</option>
							<option value="Am General" {{ $driver->driver_service->car_make == 'Am General' ? 'selected' : '' }}>Am General</option>
							<option value="American Motors" {{ $driver->driver_service->car_make == 'American Motors' ? 'selected' : '' }}>American Motors</option>
							<option value="Aston Martin" {{ $driver->driver_service->car_make == 'Aston Martin' ? 'selected' : '' }}>Aston Martin</option>
							<option value="Auburn" {{ $driver->driver_service->car_make == 'Auburn' ? 'selected' : '' }}>Auburn</option>
							<option value="Audi" {{ $driver->driver_service->car_make == 'Audi' ? 'selected' : '' }}>Audi</option>
							<option value="Austin" {{ $driver->driver_service->car_make == 'Austin' ? 'selected' : '' }}>Austin</option>
							<option value="Austin-Healey" {{ $driver->driver_service->car_make == 'Austin-Healey' ? 'selected' : '' }}>Austin-Healey</option>
							<option value="Avanti Motors" {{ $driver->driver_service->car_make == 'Avanti Motors' ? 'selected' : '' }}>Avanti Motors</option>
							<option value="Bentley" {{ $driver->driver_service->car_make == 'Bentley' ? 'selected' : '' }}>Bentley</option>
							<option value="BMW" {{ $driver->driver_service->car_make == 'BMW' ? 'selected' : '' }}>BMW</option>
							<option value="Bricklin" {{ $driver->driver_service->car_make == 'Bricklin' ? 'selected' : '' }}>Bricklin</option>
							<option value="Bugatti" {{ $driver->driver_service->car_make == 'Bugatti' ? 'selected' : '' }}>Bugatti</option>
							<option value="Buick" {{ $driver->driver_service->car_make == 'Buick' ? 'selected' : '' }}>Buick</option>
							<option value="Cadillac" {{ $driver->driver_service->car_make == 'Cadillac' ? 'selected' : '' }}>Cadillac</option>
							<option value="Checker" {{ $driver->driver_service->car_make == 'Checker' ? 'selected' : '' }}>Checker</option>
							<option value="Chevrolet" {{ $driver->driver_service->car_make == 'Chevrolet' ? 'selected' : '' }}>Chevrolet</option>
							<option value="Chrysler" {{ $driver->driver_service->car_make == 'Chrysler' ? 'selected' : '' }}>Chrysler</option>
							<option value="Citroen" {{ $driver->driver_service->car_make == 'Citroen' ? 'selected' : '' }}>Citroen</option>
							<option value="Daewoo" {{ $driver->driver_service->car_make == 'Daewoo' ? 'selected' : '' }}>Daewoo</option>
							<option value="Daihatsu" {{ $driver->driver_service->car_make == 'Daihatsu' ? 'selected' : '' }}>Daihatsu</option>
							<option value="Datsun" {{ $driver->driver_service->car_make == 'Datsun' ? 'selected' : '' }}>Datsun</option>
							<option value="Delahaye"{{ $driver->driver_service->car_make == 'Delahaye' ? 'selected' : '' }}>Delahaye</option>
							<option value="Delorean"{{ $driver->driver_service->car_make == 'Delorean' ? 'selected' : '' }}>Delorean</option>
							<option value="Desoto"{{ $driver->driver_service->car_make == 'Desoto' ? 'selected' : '' }}>Desoto</option>
							<option value="DeTomaso"{{ $driver->driver_service->car_make == 'DeTomaso' ? 'selected' : '' }}>DeTomaso</option>
							<option value="Dodge"{{ $driver->driver_service->car_make == 'Dodge' ? 'selected' : '' }}>Dodge</option>
							<option value="Eagle"{{ $driver->driver_service->car_make == 'Eagle' ? 'selected' : '' }}>Eagle</option>
							<option value="Edsel"{{ $driver->driver_service->car_make == 'Edsel' ? 'selected' : '' }}>Edsel</option>
							<option value="Essex"{{ $driver->driver_service->car_make == 'Essex' ? 'selected' : '' }}>Essex</option>
							<option value="Ferrari"{{ $driver->driver_service->car_make == 'Ferrari' ? 'selected' : '' }}>Ferrari</option>
							<option value="FIAT"{{ $driver->driver_service->car_make == 'FIAT' ? 'selected' : '' }}>FIAT</option>
							<option value="Fisker"{{ $driver->driver_service->car_make == 'Fisker' ? 'selected' : '' }}>Fisker</option>
							<option value="Ford"{{ $driver->driver_service->car_make == 'Ford' ? 'selected' : '' }}>Ford</option>
							<option value="Franklin"{{ $driver->driver_service->car_make == 'Franklin' ? 'selected' : '' }}>Franklin</option>
							<option value="Genesis"{{ $driver->driver_service->car_make == 'Genesis' ? 'selected' : '' }}>Genesis</option>
							<option value="Geo"{{ $driver->driver_service->car_make == 'Geo' ? 'selected' : '' }}>Geo</option>
							<option value="GMC"{{ $driver->driver_service->car_make == 'GMC' ? 'selected' : '' }}>GMC</option>
							<option value="Honda"{{ $driver->driver_service->car_make == 'Honda' ? 'selected' : '' }}>Honda</option>
							<option value="Hudson"{{ $driver->driver_service->car_make == 'Hudson' ? 'selected' : '' }}>Hudson</option>
							<option value="Hummer"{{ $driver->driver_service->car_make == 'Hummer' ? 'selected' : '' }}>Hummer</option>
							<option value="Hupmobile"{{ $driver->driver_service->car_make == 'Hupmobile' ? 'selected' : '' }}>Hupmobile</option>
							<option value="Hyundai"{{ $driver->driver_service->car_make == 'Hyundai' ? 'selected' : '' }}>Hyundai</option>
							<option value="INFINITI"{{ $driver->driver_service->car_make == 'INFINITI' ? 'selected' : '' }}>INFINITI</option>
							<option value="International"{{ $driver->driver_service->car_make == 'International' ? 'selected' : '' }}>International</option>
							<option value="Isuzu"{{ $driver->driver_service->car_make == 'Isuzu' ? 'selected' : '' }}>Isuzu</option>
							<option value="Jaguar"{{ $driver->driver_service->car_make == 'Jaguar' ? 'selected' : '' }}>Jaguar</option>
							<option value="Jeep"{{ $driver->driver_service->car_make == 'Jeep' ? 'selected' : '' }}>Jeep</option>
							<option value="Jensen"{{ $driver->driver_service->car_make == 'Jensen' ? 'selected' : '' }}>Jensen</option>
							<option value="Kaiser"{{ $driver->driver_service->car_make == 'Kaiser' ? 'selected' : '' }}>Kaiser</option>
							<option value="Karma" {{ $driver->driver_service->car_make == 'Karma' ? 'selected' : '' }}>Karma</option>
							<option value="Kia"{{ $driver->driver_service->car_make == 'Kia' ? 'selected' : '' }}>Kia</option>
							<option value="Koenigsegg"{{ $driver->driver_service->car_make == 'Koenigsegg' ? 'selected' : '' }}>Koenigsegg</option>
							<option value="Lamborghini"{{ $driver->driver_service->car_make == 'Lamborghini' ? 'selected' : '' }}>Lamborghini</option>
							<option value="Lancia"{{ $driver->driver_service->car_make == 'Lancia' ? 'selected' : '' }}>Lancia</option>
							<option value="Land Rover"{{ $driver->driver_service->car_make == 'Land Rover' ? 'selected' : '' }}>Land Rover</option>
							<option value="LaSalle"{{ $driver->driver_service->car_make == 'LaSalle' ? 'selected' : '' }}>LaSalle</option>
							<option value="Lexus"{{ $driver->driver_service->car_make == 'Lexus' ? 'selected' : '' }}>Lexus</option>
							<option value="Lincoln"{{ $driver->driver_service->car_make == 'Karma' ? 'selected' : '' }}>Lincoln</option>
							<option value="Lotus"{{ $driver->driver_service->car_make == 'Lotus' ? 'selected' : '' }}>Lotus</option>
							<option value="Maserati"{{ $driver->driver_service->car_make == 'Maserati' ? 'selected' : '' }}>Maserati</option>
							<option value="Maybach"{{ $driver->driver_service->car_make == 'Maybach' ? 'selected' : '' }}>Maybach</option>
							<option value="Mazda"{{ $driver->driver_service->car_make == 'Mazda' ? 'selected' : '' }}>Mazda</option>
							<option value="McLaren"{{ $driver->driver_service->car_make == 'McLaren' ? 'selected' : '' }}>McLaren</option>
							<option value="Mercedes-Benz"{{ $driver->driver_service->car_make == 'Mercedes-Benz' ? 'selected' : '' }}>Mercedes-Benz</option>
							<option value="Mercury"{{ $driver->driver_service->car_make == 'Mercury' ? 'selected' : '' }}>Mercury</option>
							<option value="Merkur"{{ $driver->driver_service->car_make == 'Merkur' ? 'selected' : '' }}>Merkur</option>
							<option value="MG"{{ $driver->driver_service->car_make == 'MG' ? 'selected' : '' }}>MG</option>
							<option value="MINI"{{ $driver->driver_service->car_make == 'MINI' ? 'selected' : '' }}>MINI</option>
							<option value="Mitsubishi"{{ $driver->driver_service->car_make == 'Mitsubishi' ? 'selected' : '' }}>Mitsubishi</option>
							<option value="Morgan"{{ $driver->driver_service->car_make == 'Morgan' ? 'selected' : '' }}>Morgan</option>
							<option value="Morris"{{ $driver->driver_service->car_make == 'Morris' ? 'selected' : '' }}>Morris</option>
							<option value="Nash"{{ $driver->driver_service->car_make == 'Nash' ? 'selected' : '' }}>Nash</option>
							<option value="Nissan"{{ $driver->driver_service->car_make == 'Nissan' ? 'selected' : '' }}>Nissan</option>
							<option value="Oldsmobile"{{ $driver->driver_service->car_make == 'Oldsmobile' ? 'selected' : '' }}>Oldsmobile</option>
							<option value="Opel"{{ $driver->driver_service->car_make == 'Opel' ? 'selected' : '' }}>Opel</option>
							<option value="Packard"{{ $driver->driver_service->car_make == 'Packard' ? 'selected' : '' }}>Packard</option>
							<option value="Pagani"{{ $driver->driver_service->car_make == 'Pagani' ? 'selected' : '' }}>Pagani</option>
							<option value="Panoz"{{ $driver->driver_service->car_make == 'Panoz' ? 'selected' : '' }}>Panoz</option>
							<option value="Peugeot"{{ $driver->driver_service->car_make == 'Peugeot' ? 'selected' : '' }}>Peugeot</option>
							<option value="Plymouth"{{ $driver->driver_service->car_make == 'Plymouth' ? 'selected' : '' }}>Plymouth</option>
							<option value="Pontiac"{{ $driver->driver_service->car_make == 'Pontiac' ? 'selected' : '' }}>Pontiac</option>
							<option value="Porsche"{{ $driver->driver_service->car_make == 'Porsche' ? 'selected' : '' }}>Porsche</option>
							<option value="Qvale"{{ $driver->driver_service->car_make == 'Qvale' ? 'selected' : '' }}>Qvale</option>
							<option value="RAM"{{ $driver->driver_service->car_make == 'RAM' ? 'selected' : '' }}>RAM</option>
							<option value="Renault"{{ $driver->driver_service->car_make == 'Renault' ? 'selected' : '' }}>Renault</option>
							<option value="Rolls-Royce"{{ $driver->driver_service->car_make == 'Rolls-Royce' ? 'selected' : '' }}>Rolls-Royce</option>
							<option value="Rover"{{ $driver->driver_service->car_make == 'Rover' ? 'selected' : '' }}>Rover</option>
							<option value="Saab"{{ $driver->driver_service->car_make == 'Saab' ? 'selected' : '' }}>Saab</option>
							<option value="Saleen"{{ $driver->driver_service->car_make == 'Saleen' ? 'selected' : '' }}>Saleen</option>
							<option value="Saturn"{{ $driver->driver_service->car_make == 'Saturn' ? 'selected' : '' }}>Saturn</option>
							<option value="Scion"{{ $driver->driver_service->car_make == 'Scion' ? 'selected' : '' }}>Scion</option>
							<option value="Smart"{{ $driver->driver_service->car_make == 'Smart' ? 'selected' : '' }}>Smart</option>
							<option value="Spyker"{{ $driver->driver_service->car_make == 'Spyker' ? 'selected' : '' }}>Spyker</option>
							<option value="Sterling"{{ $driver->driver_service->car_make == 'Sterling' ? 'selected' : '' }}>Sterling</option>
							<option value="Studebaker"{{ $driver->driver_service->car_make == 'Studebaker' ? 'selected' : '' }}>Studebaker</option>
							<option value="Subaru"{{ $driver->driver_service->car_make == 'Subaru' ? 'selected' : '' }}>Subaru</option>
							<option value="Sunbeam"{{ $driver->driver_service->car_make == 'Sunbeam' ? 'selected' : '' }}>Sunbeam</option>
							<option value="Suzuki"{{ $driver->driver_service->car_make == 'Suzuki' ? 'selected' : '' }}>Suzuki</option>
							<option value="Tesla"{{ $driver->driver_service->car_make == 'Tesla' ? 'selected' : '' }}>Tesla</option>
							<option value="Toyota"{{ $driver->driver_service->car_make == 'Toyota' ? 'selected' : '' }}>Toyota</option>
							<option value="Triumph"{{ $driver->driver_service->car_make == 'Triumph' ? 'selected' : '' }}>Triumph</option>
							<option value="TVR"{{ $driver->driver_service->car_make == 'TVR' ? 'selected' : '' }}>TVR</option>
							<option value="Volkswagen"{{ $driver->driver_service->car_make == 'Volkswagen' ? 'selected' : '' }}>Volkswagen</option>
							<option value="Volvo"{{ $driver->driver_service->car_make == 'Volvo' ? 'selected' : '' }}>Volvo</option>
							<option value="Willys"{{ $driver->driver_service->car_make == 'Willys' ? 'selected' : '' }}>Willys</option>
							<option value="Yugo"{{ $driver->driver_service->car_make == 'Yugo' ? 'selected' : '' }}>Yugo</option>

						</select>
					</div>
				</div>

				<div class="form-group row">
					<label for="car_type" class="col-xs-2 col-form-label">Car Type <i class="spin fa fa-circle-o-notch fa-spin" style="float: right;"></i></label>
					<div class="col-xs-10">
						<select name="car_type" id="car_type" class="form-control">
							<option value="">Select Car Type</option>
						</select>
					</div>
				</div>

				<div class="form-group row">
					<label for="car_plate_number" class="col-xs-2 col-form-label">Car Plate Number</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" name="car_plate_number" value="{{ $driver->driver_service->car_plate_number }}" id="car_plate_number" placeholder="Car Plate Number" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="car_number_expire_date" class="col-xs-2 col-form-label">Car Number Plate Expiry Date</label>
					<div class="col-xs-10">
						<input class="form-control" type="date" name="car_number_expire_date" value="{{ $driver->driver_service->car_number_expire_date }}" id="car_number_expire_date" placeholder="Car Number Expire Date" required>
					</div>
				</div>
			@endif --}}
				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Update Driver</button>
						<a href="{{route('admin.driver.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>
 <style type="text/css">.iti { width: 100%; }</style>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/css/intlTelInput.css">
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/intlTelInput-jquery.min.js"></script>
 <script>
    	jq = jQuery.noConflict();
        use :
       		var s = jq("#mobile").intlTelInput({
       			autoPlaceholder: 'polite',
       			separateDialCode: true,
       			formatOnDisplay: true,
       			utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/utils.js"
       		});
       	insteadof :
       		var countryData = window.intlTelInputGlobals.getCountryData();
       		var iso2;
			var isdcode = jq("#code").val();
			for (var i = 0; i < countryData.length; i++) {
				if (countryData[i].dialCode == parseInt(isdcode)){
					iso2 = countryData[i].iso2;
					break;
				}
			}
			if(iso2){
				jq("#mobile").intlTelInput("setCountry", iso2);
			}
			jq(document).on('countrychange', function (e, countryData) {
            	jq("#code").val((jq("#mobile").intlTelInput("getSelectedCountryData").dialCode));
        	});
</script>
@endsection
{{-- @section('scripts')
<script>
	$('.spin').hide()
	var make = '{{$driver->driver_service->car_make}}';
	var model = '{{$driver->driver_service->car_model}}';
	if (make) {
	$('.spin').show()
	$.ajax({
        url: 'https://carmakemodeldb.com/api/v1/car-lists/get/all/models/'+make+'?api_token=8upB6H0FZ0ZCCRxmFEeurmKGYbZiyaGB0061nzhFv1Pkc4XZ1vfHguosd0TN',
        method: 'GET',
        success: function(response){
        	$('.spin').hide()
           	$('#car_type').empty();
           	var options = '';
			$.each(response, function(key, value) {
				options += '<option value="'+value.model+'">'+value.model+'</option>';
			});
			$('#car_type').append(options);
			$('#car_type option[value="'+model+'"]').attr('selected','selected');
        },
        error: function(response){
        	console.log(response);
        }
    });
	}
	$('#car_make').change(function(){
    	var make = $(this).val();
    	if (!make) {
    		make = ' ';
    	}
    	$('.spin').show()
    	$.ajax({
            url: 'https://carmakemodeldb.com/api/v1/car-lists/get/all/models/'+make+'?api_token=8upB6H0FZ0ZCCRxmFEeurmKGYbZiyaGB0061nzhFv1Pkc4XZ1vfHguosd0TN',
            method: 'GET',
            success: function(response){
            	$('.spin').hide()
               	$('#car_type').empty();
               	var options = '';
				$.each(response, function(key, value) {
				  options += '<option value="'+value.model+'">'+value.model+'</option>';
				});

				$('#car_type').append(options);

            },
            error: function(response){
            	console.log(response);
            }
        });
    });
</script>
@endsection --}}
