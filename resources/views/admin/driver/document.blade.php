@extends('admin.layout.base')

@section('title', 'Drivers')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Driver's Document
            </h5>
            <a href="{{ route('admin.driver.index') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-angle-left"></i> Back</a>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Document Type</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                {{-- ->document->name --}}
                <tbody>
                    @foreach($driver_documents as $index => $driver_document)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $driver_document->document_id }}</td>
                        <td>
                            @if(isset($driver_document->url))
                                @if(File::exists(storage_path('app/public' .str_replace("storage", "", $driver_document->url))))
                                    <img id="myImg" class="MyImg" style="width: 250px; height: 250px;" src="{{URL::asset($driver_document->url)}}">
                                @else
                                    <img id="myImg" class="MyImg" src="{{$driver_document->url}}" style="height: 60px; width: 90px;">
                                @endif
                            @else
                                <img src="{{URL::asset('asset/NO_IMG.png')}}" style="height: 60px; width: 90px;">
                            @endif
                        </td>
                        @if($driver_document->status == 'ACTIVE')
                            <td><p class="text-success">{{ $driver_document->status }}</p></td>
                        @elseif($driver_document->status == 'INVALID')
                            <td><p class="text-info">{{ $driver_document->status }}</p></td>
                        @elseif($driver_document->status == 'EXPIRE')
                            <td><p class="text-danger">{{ $driver_document->status }}</p></td>
                        @else
                            <td><p class="text-dark">{{ $driver_document->status }}</p></td>
                        @endif
                        <td>
                          <a href="javascript:void(0)" class="btn btn-success btn-block verification" data-value="ACTIVE" data-id="{{$driver_document->id}}">Active</a>

                          <a href="javascript:void(0)" class="btn btn-info btn-block verification" data-value="INVALID" data-id="{{$driver_document->id}}">
                          <span id="msg-invalid{{$driver_document->id}}">Invalid</span>
                          <i id="spin-invalid{{$driver_document->id}}" class="fa fa-circle-o-notch fa-spin" style="font-size: 20px; display: none;"></i>
                          </a>

                          <a href="javascript:void(0)" class="btn btn-danger btn-block verification" data-value="EXPIRE" data-id="{{$driver_document->id}}">
                          <span id="msg-expire{{$driver_document->id}}">Expire</span>
                          <i id="spin-expire{{$driver_document->id}}" class="fa fa-circle-o-notch fa-spin" style="font-size: 20px; display: none;"></i>
                          </a>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>S.No</th>
                        <th>Document Type</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

{{-- ..........Document Modal................. --}}
<div class="modal fade" id="documentModel" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content" style="background-color: lavender;">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
      </div>
      <div class="modal-body">
      
        <div class="d-flex justify-content-center" >
            <div class="col-lg-12">

                <div class="alert alert-danger reason-danger" role="alert" style="display: none; height: 42px;">
                    <p class="reason-error" style="color: red; padding: 0px;"></p>
                </div>

                <form>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="control-label text-dark">Reason</label>
                                <textarea name="reason" id="reason" rows="4" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="submit" class="btn btn-info float-left" id="storeReason" value="Submit">
                            <a href="" class="btn btn-secondary float-right">Close</a>
                        </div>
                    </div>
                </form>
                 
            </div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
{{-- end Document Modal --}}

<!-- The Modal -->
<div id="myModal" class="modal doc-modal">
  <span class="close doc-close">&times;</span>
  <img class="modal-content" id="img01">
</div>
<style>
#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}
#myImg:hover {opacity: 0.7;}
/* The Modal (background) */
.doc-modal {
  position: fixed;
  z-index: 10000;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgb(0,0,0);
  background-color: rgba(0,0,0,0.9);
  background-size: contain;
  cursor: zoom-out;
}
/* Modal Content (image) */
.modal-content {
  margin: auto;
  display: block;
  width: 90%;
}
/* Add Animation */
.modal-content{  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}
@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)} 
  to {-webkit-transform:scale(1)}
}
@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}
/* The Close Button */
.doc-close {
  position: absolute;
  top: 15px;
  right: 22px;
  color: #ffffff;
  font-size: 45px;
  font-weight: bold;
  transition: 0.3s;
}
.doc-close:hover,
.doc-close:focus {
  color: #ffffff;
  text-decoration: none;
  cursor: pointer;
}
/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}
</style>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $(document).on('click','.verification', function(e){
          e.preventDefault();
          var id = $(this).data("id");
          var user_id = '{{ $driver_document->user_id }}';
          var status = $(this).data("value");

          if (status == 'INVALID') {
            bootbox.confirm('Are you sure to '+status+' document?', function(result)
            {
              if(result){
                $.ajax({
                  url: "{{route('admin.driver.document.verification',[0])}}",
                  type: 'GET',
                  data: {
                    'document_id': id, 'status': status, 'user_id': user_id
                      },
                  success: function (response){
                      location.reload();
                  },
                  error: function (response){
                      console.log(response);
                  }
                });
              }
            });

            // $('#documentModel').modal('show');
            // $('.modal-title').html('Invalid Document');
            // $('#storeReason').click(function(e){
            //   e.preventDefault()
            //   $.ajaxSetup({
            //     headers:{
            //       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     }
            //   });
            //   var reason = $('#reason').val();
            //   $.ajax({
            //     url: '',
            //     method: 'post',
            //     data: { status:status, user_id:user_id, id:id, reason:reason },
            //     success: function(response){
            //         if (response.success) {
            //           $('#documentModel').modal('hide');
            //           $('#msg-invalid'+id).css({'display':'none'});
            //           $('#spin-invalid'+id).css({'display':'block'});
            //           $.ajax({
            //             url: "",
            //             type: 'GET',
            //             data: {
            //               'document_id': id, 'status': status, 'user_id': user_id, 'reason': response.reason
            //                 },
            //             success: function (response){
            //                 location.reload();
            //             },
            //             error: function (response){
            //                 console.log(response);
            //             }
            //           });
            //         }
            //     },
            //     error: function(response){
            //       $('.reason-danger').show();
            //       $('.reason-error').html(response.responseJSON.errors.reason);
            //     }
            //   });

            // });

          } else if (status == 'EXPIRE') {
            bootbox.confirm('Are you sure to '+status+' document?', function(result)
            {
              if(result){
                 $.ajax({
                    url: "{{route('admin.driver.document.verification',[0])}}",
                    type: 'GET',
                    data: {
                      'document_id': id, 'status': status, 'user_id': user_id
                        },
                    success: function (response){
                        location.reload();
                    },
                    error: function (response){
                        console.log(response);
                    }
                  });
                }
            });

            // $('#documentModel').modal('show')
            // $('.modal-title').html('Expire Document');
            // $('#storeReason').click(function(e){
            //   e.preventDefault()
            //   $.ajaxSetup({
            //     headers:{
            //       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     }
            //   });
            //   var reason = $('#reason').val();
            //   $.ajax({
            //     url: '',
            //     method: 'post',
            //     data: { status:status, user_id:user_id, id:id, reason:reason },
            //     success: function(response){
            //         if (response.success) {
            //           $('#documentModel').modal('hide');
            //           $('#msg-expire'+id).css({'display':'none'});
            //           $('#spin-expire'+id).css({'display':'block'});
            //           $.ajax({
            //             url: "",
            //             type: 'GET',
            //             data: {
            //               'document_id': id, 'status': status, 'user_id': user_id, 'reason': response.reason
            //                 },
            //             success: function (response){
            //                 location.reload();
            //             },
            //             error: function (response){
            //                 console.log(response);
            //             }
            //           });
            //         }
            //     },
            //     error: function(response){
            //       $('.reason-danger').show();
            //       $('.reason-error').html(response.responseJSON.errors.reason);
            //     }
            //   });

            // });

          } else {

          bootbox.confirm('Are you sure to '+status+' document?', function(result)
          {
            if(result){
            $.ajax({
                url: "{{route('admin.driver.document.verification',[0])}}",
                type: 'GET',
                data: {
                        'document_id': id, 'status': status, 'user_id': user_id
                    },
                success: function (response){
                    location.reload();
                },
                error: function (response){
                    console.log(response);
                }
              });
            }
          });
        }
        });

        $(document).on('click','.MyImg', function(){
            $('#myModal').css({'display':'block'});
            var imgsrc = $(this).attr("src");
            $('#img01').attr('src',imgsrc);
        });
        $('span').click(function(){
            $('#myModal').css({'display':'none'});
        });

    });
</script>
@endsection