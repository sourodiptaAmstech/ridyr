@extends('admin.layout.base')

@section('title', 'Car Images')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">Car Images</h5>
            <a href="{{ route('admin.driver.index') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-angle-left"></i> Back</a>
            <table class="table table-striped table-bordered dataTable" id="table-carimage">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Car Image</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($carimages as $index => $carimage)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>
                            @if(isset($carimage->image_url))
                                @if(File::exists(storage_path('app/public' .str_replace("storage", "", $carimage->image_url))))
                                    <img src="{{URL::asset($carimage->image_url)}}" style="height: 200px; width: 250px;">
                                @else
                                    <img src="{{URL::asset('asset/NO_IMG.png')}}" style="height: 50px; width: 65px;">
                                @endif
                            @else
                                <img src="{{URL::asset('asset/NO_IMG.png')}}" style="height: 50px; width: 65px;">
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>S.No</th>
                        <th>Car Image</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
$(document).ready(function(){
    $('#table-carimage').DataTable( {
        responsive: true,
        dom: 'Bfrtip',
        buttons: []
    } );
});
</script>
@endsection