@extends('admin.layout.base')

@section('title', 'Add Driver ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
    	    <a href="{{ route('admin.driver.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Add Driver</h5>

            <form class="form-horizontal" action="{{route('admin.driver.store' )}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
            	
				<div class="form-group row">
					<label for="first_name" class="col-xs-2 col-form-label">First Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" name="first_name" value="{{ old('first_name') }}" id="first_name" placeholder="First Name" required>
					</div>
				</div>

				<div class="form-group row">
					<label for="last_name" class="col-xs-2 col-form-label">Last Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" name="last_name" value="{{ old('last_name') }}" id="last_name" placeholder="Last Name" required>
					</div>
				</div>

				<div class="form-group row">
					<label for="email" class="col-xs-2 col-form-label">Email</label>
					<div class="col-xs-10">
						<input class="form-control" type="email" name="email" value="{{ old('email') }}" id="email" placeholder="Email" required>
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-2 col-form-label">Picture</label>
					<div class="col-xs-10">
						<input type="file" accept="image/*" name="picture" class="dropify form-control-file" id="picture" aria-describedby="fileHelp">
					</div>
				</div>

				<div class="form-group row">
					<label for="mobile" class="col-xs-2 col-form-label">Mobile</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" name="mobile" id="mobile" required>
						<input type="hidden" name="code" id="code">
					</div>
				</div>

				<div class="form-group row">
					<label for="dob" class="col-xs-2 col-form-label">Date of Birth</label>
					<div class="col-xs-10">
						<input class="form-control" type="date" name="dob" value="{{ old('dob') }}" id="dob">
					</div>
				</div>
				<div class="form-group row">
					<label for="gender" class="col-xs-2 col-form-label">Gender</label>
					<div class="col-xs-10">
						<input type="radio" id="male" name="gender" value="male">
						<label for="male">Male</label><br>
						<input type="radio" id="female" name="gender" value="female">
						<label for="female">Female</label><br>
					</div>
				</div>

				<div class="form-group row password">
					<label for="password" class="col-xs-2 col-form-label">Password</label>
					<div class="col-xs-10">
						<input class="form-control" type="password" name="password" id="password" placeholder="Password" value="{{ old('password') }}" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="password_confirmation" class="col-xs-2 col-form-label">Password Confirmation</label>
					<div class="col-xs-10">
						<input class="form-control" type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" id="password_confirmation" placeholder="Re-type Password">
					</div>
				</div>

				{{-- <div class="form-group row">
					<label for="car_make" class="col-xs-2 col-form-label">Car Brand</label>
					<div class="col-xs-10">
						<select name="car_make" id="car_make" class="form-control">
							<option value="">Select Car Brand</option>
							<option value="AC">AC</option>
							<option value="Acura">Acura</option>
							<option value="Alfa Romeo">Alfa Romeo</option>
							<option value="Am General">Am General</option>
							<option value="American Motors">American Motors</option>
							<option value="Aston Martin">Aston Martin</option>
							<option value="Auburn">Auburn</option>
							<option value="Audi">Audi</option>
							<option value="Austin">Austin</option>
							<option value="Austin-Healey">Austin-Healey</option>
							<option value="Avanti Motors">Avanti Motors</option>
							<option value="Bentley">Bentley</option>
							<option value="BMW">BMW</option>
							<option value="Bricklin">Bricklin</option>
							<option value="Bugatti">Bugatti</option>
							<option value="Buick">Buick</option>
							<option value="Cadillac">Cadillac</option>
							<option value="Checker">Checker</option>
							<option value="Chevrolet">Chevrolet</option>
							<option value="Chrysler">Chrysler</option>
							<option value="Citroen">Citroen</option>
							<option value="Daewoo">Daewoo</option>
							<option value="Daihatsu">Daihatsu</option>
							<option value="Datsun">Datsun</option>
							<option value="Delahaye">Delahaye</option>
							<option value="Delorean">Delorean</option>
							<option value="Desoto">Desoto</option>
							<option value="DeTomaso">DeTomaso</option>
							<option value="Dodge">Dodge</option>
							<option value="Eagle">Eagle</option>
							<option value="Edsel">Edsel</option>
							<option value="Essex">Essex</option>
							<option value="Ferrari">Ferrari</option>
							<option value="FIAT">FIAT</option>
							<option value="Fisker">Fisker</option>
							<option value="Ford">Ford</option>
							<option value="Franklin">Franklin</option>
							<option value="Genesis">Genesis</option>
							<option value="Geo">Geo</option>
							<option value="GMC">GMC</option>
							<option value="Honda">Honda</option>
							<option value="Hudson">Hudson</option>
							<option value="Hummer">Hummer</option>
							<option value="Hupmobile">Hupmobile</option>
							<option value="Hyundai">Hyundai</option>
							<option value="INFINITI">INFINITI</option>
							<option value="International">International</option>
							<option value="Isuzu">Isuzu</option>
							<option value="Jaguar">Jaguar</option>
							<option value="Jeep">Jeep</option>
							<option value="Jensen">Jensen</option>
							<option value="Kaiser">Kaiser</option>
							<option value="Karma">Karma</option>
							<option value="Kia">Kia</option>
							<option value="Koenigsegg">Koenigsegg</option>
							<option value="Lamborghini">Lamborghini</option>
							<option value="Lancia">Lancia</option>
							<option value="Land Rover">Land Rover</option>
							<option value="LaSalle">LaSalle</option>
							<option value="Lexus">Lexus</option>
							<option value="Lincoln">Lincoln</option>
							<option value="Lotus">Lotus</option>
							<option value="Maserati">Maserati</option>
							<option value="Maybach">Maybach</option>
							<option value="Mazda">Mazda</option>
							<option value="McLaren">McLaren</option>
							<option value="Mercedes-Benz">Mercedes-Benz</option>
							<option value="Mercury">Mercury</option>
							<option value="Merkur">Merkur</option>
							<option value="MG">MG</option>
							<option value="MINI">MINI</option>
							<option value="Mitsubishi">Mitsubishi</option>
							<option value="Morgan">Morgan</option>
							<option value="Morris">Morris</option>
							<option value="Nash">Nash</option>
							<option value="Nissan">Nissan</option>
							<option value="Oldsmobile">Oldsmobile</option>
							<option value="Opel">Opel</option>
							<option value="Packard">Packard</option>
							<option value="Pagani">Pagani</option>
							<option value="Panoz">Panoz</option>
							<option value="Peugeot">Peugeot</option>
							<option value="Plymouth">Plymouth</option>
							<option value="Pontiac">Pontiac</option>
							<option value="Porsche">Porsche</option>
							<option value="Qvale">Qvale</option>
							<option value="RAM">RAM</option>
							<option value="Renault">Renault</option>
							<option value="Rolls-Royce">Rolls-Royce</option>
							<option value="Rover">Rover</option>
							<option value="Saab">Saab</option>
							<option value="Saleen">Saleen</option>
							<option value="Saturn">Saturn</option>
							<option value="Scion">Scion</option>
							<option value="Smart">Smart</option>
							<option value="Spyker">Spyker</option>
							<option value="Sterling">Sterling</option>
							<option value="Studebaker">Studebaker</option>
							<option value="Subaru">Subaru</option>
							<option value="Sunbeam">Sunbeam</option>
							<option value="Suzuki">Suzuki</option>
							<option value="Tesla">Tesla</option>
							<option value="Toyota">Toyota</option>
							<option value="Triumph">Triumph</option>
							<option value="TVR">TVR</option>
							<option value="Volkswagen">Volkswagen</option>
							<option value="Volvo">Volvo</option>
							<option value="Willys">Willys</option>
							<option value="Yugo">Yugo</option>
							
						</select>
					</div>
				</div> --}}

				{{-- <div class="form-group row">
					<label for="car_type" class="col-xs-2 col-form-label">Car Type<i class="spin fa fa-circle-o-notch fa-spin" style="float: right; display: none;"></i></label>
					<div class="col-xs-10">
						<select name="car_type" id="car_type" class="form-control">
							<option value="">Select Car Type</option>
							
						</select>
					</div>
				</div> --}}

				{{-- <div class="form-group row">
					<label for="car_plate_number" class="col-xs-2 col-form-label">Car Plate Number</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" name="car_plate_number" value="{{ old('car_plate_number') }}" id="car_plate_number" placeholder="Car Plate Number" required>
					</div>
				</div> --}}
				{{-- <div class="form-group row">
					<label for="car_number_expire_date" class="col-xs-2 col-form-label">Car Number Plate Expiry Date</label>
					<div class="col-xs-10">
						<input class="form-control" type="date" name="car_number_expire_date" value="{{ old('car_number_expire_date') }}" id="car_number_expire_date" placeholder="Car Number Expire Date" required>
					</div>
				</div> --}}

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Add Driver</button>
						<a href="{{route('admin.driver.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>
<style type="text/css">.iti { width: 100%; }</style>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/css/intlTelInput.css">
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/intlTelInput-jquery.min.js"></script>
 <script>
	jq = jQuery.noConflict();

    use :
   		var s = jq("#mobile").intlTelInput({
			autoPlaceholder: 'polite',
   			separateDialCode: true,
   			formatOnDisplay: true,
   			utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/utils.js"   
   		});

   	insteadof :
   		var d = jq("#mobile").intlTelInput("getSelectedCountryData").dialCode;
   		jq("#code").val(d);
		jq(document).on('countrychange', function (e, countryData) {
        	jq("#code").val((jq("#mobile").intlTelInput("getSelectedCountryData").dialCode));
    	});
</script>

@endsection
{{-- @section('scripts')
<script>
  	$(document).ready(function() {
	    const carMake = '{{ old('car_make') }}';
	    if(carMake !== '') {
	      $('#car_make').val(carMake);
	    }

	    const servicetype = '{{ old('service_type_id') }}';
	    if(servicetype !== '') {
	      $('#service_type_id').val(servicetype);
	    }

	    $('#car_make').change(function(){
	    	var make = $(this).val();
	    	if (!make) {
	    		make = ' ';
	    	}
	    	$('.spin').css({'display': 'block'});
	    	$.ajax({
                url: 'https://carmakemodeldb.com/api/v1/car-lists/get/all/models/'+make+'?api_token=8upB6H0FZ0ZCCRxmFEeurmKGYbZiyaGB0061nzhFv1Pkc4XZ1vfHguosd0TN',
                method: 'GET',
                success: function(response){
	    			$('.spin').css({'display': 'none'});
                   	$('#car_type').empty();
                   	var options = '';
					$.each(response, function(key, value) {
					  options += '<option value="'+value.model+'">'+value.model+'</option>';
					});

					$('#car_type').append(options);

                },
                error: function(response){
                	console.log(response);
                }
            });
	    });

  	});
</script>
@endsection --}}