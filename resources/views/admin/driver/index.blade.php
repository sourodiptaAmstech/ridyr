@extends('admin.layout.base')

@section('title', 'Drivers ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Drivers
            </h5>
            <a href="{{ route('admin.driver.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Driver</a>
            <table class="table table-hover table-striped table-bordered dataTable" id="table-driv">
                <thead>
                    <tr>
                        {{-- <th>ID</th> --}}
                        <th>Driver Name</th>
                        {{-- <th>Last Name</th> --}}
                        <th>Email</th>
                        <th>Mobile</th>
                        {{--  <th>Avg Rating</th> --}}
                        <th>Action</th>
                    </tr>
                </thead>
               {{--  <tbody>
                    @foreach($drivers as $index => $driver)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $driver->first_name }}</td>
                        <td>{{ $driver->last_name }}</td>
                        <td>{{ $driver->email }}</td>
                        <td>{{ $driver->mobile }}</td>
                        @if($driver->driver_profile != null)
                        <td>{{ $driver->driver_profile->rating }}</td>
                        @else
                        <td></td>
                        @endif --}}
                        {{-- @if($driver->passenger_request!=null)
                            @php
                            $totalRating = null;
                            @endphp
                            @foreach($driver->passenger_request as $index => $passenger_request)
                            @php
                            $totalRating = $totalRating+$passenger_request->driver_rating;
                            $index++;
                            @endphp
                            @endforeach
                            <td>{{round($totalRating/$index,2)}}</td>
                        @else
                            <td></td>
                        @endif --}}
                       {{--  <td style="line-height: 34px;">
                            <a href="{{ route('admin.driver.edit', $driver->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                            <a href="{{ route('admin.drivers.document', $driver->id) }}" class="btn btn-info"><i class="fa fa-file"></i> Document</a>
                            <a href="{{ route('admin.driver.carimages', $driver->id) }}" class="btn btn-info"><i class="fa fa-file"></i> Car Images</a>
                            @if($driver->driver_profile)
                                @if($driver->driver_profile->status == 'onboarding' || $driver->driver_profile->status == 'banned')
                                <a href="{{ route('admin.driver.activation', $driver->id) }}" class="btn btn-danger driver-status" data-toggle="tooltip" data-value="Activate" data-id="{{$driver->id}}" title="Click to Active">Inactive</a>
                                @else
                                <a href="{{ route('admin.driver.activation', $driver->id) }}" class="btn btn-success driver-status" data-toggle="tooltip" data-value="Inactivate" data-id="{{$driver->id}}" title="Click to Inactive">Active</a>
                                @endif
                            @else
                            @endif
                            <a href="{{ route('admin.driver.transaction', $driver->id) }}" class="btn btn-info"><i class="fa fa-file"></i> Package</a>

                            <a href="{{ route('admin.driver.rating-review',46) }}" class="btn btn-info"><i class="fa fa-file"></i>Rating</a>
                            
                        </td>
                    </tr>
                    @endforeach
                </tbody> --}}
                <tfoot>
                    <tr>
                        {{-- <th>ID</th> --}}
                        <th>Driver Name</th>
                        {{-- <th>Last Name</th> --}}
                        <th>Email</th>
                        <th>Mobile</th>
                        {{-- <th>Avg Rating</th> --}}
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

{{-- ..........Document Modal................. --}}
<div class="modal fade" id="driverModel" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content" style="background-color: lavender;">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
      </div>
      <div class="modal-body">
      
        <div class="d-flex justify-content-center">
            <div class="col-lg-12">

                <div class="alert alert-danger reason-danger" role="alert" style="display: none; height: 42px;">
                    <p class="reason-error" style="color: red; padding: 0px;"></p>
                </div>

                <form>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="control-label text-dark">Reason</label>
                                <textarea name="reason" id="reason" rows="4" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            {{-- <input type="submit" class="btn btn-info float-left" id="saveReason" value="Submit"> --}}
                            <a href="javascript:void(0)" class="btn btn-info float-left" id="saveReason" style="width: 70px;">
                            <span id="submit">Submit</span>
                            <i id="submit_spin" class="fa fa-circle-o-notch fa-spin" style="font-size: 20px; display: none;"></i>
                            </a>
                            <a href="" class="btn btn-secondary float-right">Close</a>
                        </div>
                    </div>
                </form>
                 
            </div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
{{-- end Document Modal --}}
@endsection
@section('scripts')
<script>
    $(document).ready(function(){

        $('#table-driv').DataTable( {
            //"order": [[0, 'desc']],
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url":"{{route('admin.ajax-driver')}}",
                "dataType":"json",
                "type":"POST",
                "data":{"_token":"<?= csrf_token() ?>"}
            },
            "columns":[
                // {"data":"id"},
                {"data":"driver_name", "orderable":false},
                {"data":"email", "orderable":false},
                {"data":"mobile", "orderable":false},
                {"data":"action","searchable":false,"orderable":false}
            ],
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
        } );

        // $(document).on('click','.driver-status',function (e) {
        //     e.preventDefault();
        //     var href = $(this).attr('href');
        //     var value = $(this).data('value');
        //     var id = $(this).data("id");
        //     if (value == 'Activate') {
        //     bootbox.confirm('Do you really want to '+value+' driver?', function (res) {
        //     if (res){
        //         window.location = href;
        //     }  
        //     });
        //   }
        //   if (value == 'Inactivate') {
        //     $('#driverModel').modal('show');
        //     $('.modal-title').html('Driver Inactivate');
        //     $('#saveReason').click(function(e){
        //       e.preventDefault()
        //       $.ajaxSetup({
        //         headers:{
        //           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         }
        //       });
        //       var reason = $('#reason').val();
        //       $('#submit').css({'display':'none'});
        //       $('#submit_spin').css({'display':'block'});
        //       $.ajax({
        //         url: '',
        //         method: 'post',
        //         data: { id:id, reason:reason },
        //         success: function(response){
        //             if (response.success) {
        //               $('#submit').css({'display':'block'});
        //               $('#submit_spin').css({'display':'none'});
        //               $('#driverModel').modal('hide');
        //               window.location = href;
        //             }
        //         },
        //         error: function(response){
        //           $('#submit').css({'display':'block'});
        //           $('#submit_spin').css({'display':'none'});
        //           $('.reason-danger').show();
        //           $('.reason-error').html(response.responseJSON.errors.reason);
        //         }
        //       });

        //     });
        //   }

        // });

    });
</script>
@endsection