@extends('admin.layout.base')

@section('title', 'Add Passenger ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
    	    <a href="{{ route('admin.passenger.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Add Passenger</h5>

            <form class="form-horizontal" action="{{route('admin.passenger.store' )}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
            	
				<div class="form-group row">
					<label for="first_name" class="col-xs-2 col-form-label">First Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" name="first_name" value="{{ old('first_name') }}"  id="first_name" placeholder="First Name" required>
					</div>
				</div>

				<div class="form-group row">
					<label for="last_name" class="col-xs-2 col-form-label">Last Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" name="last_name" value="{{ old('last_name') }}" id="last_name" placeholder="Last Name" required>
					</div>
				</div>

				<div class="form-group row">
					<label for="email" class="col-xs-2 col-form-label">Email</label>
					<div class="col-xs-10">
						<input class="form-control" type="email" name="email" value="{{ old('email') }}" id="email" placeholder="Email" required>
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-2 col-form-label">Picture</label>
					<div class="col-xs-10">
						<input type="file" accept="image/*" name="picture" class="dropify form-control-file" id="picture" aria-describedby="fileHelp">
					</div>
				</div>
				
				<div class="form-group row">
					<label for="mobile_no" class="col-xs-2 col-form-label">Mobile</label>
					<div class="col-xs-10">
						<input class="form-control" type="tel" name="mobile_no" id="mobile_no" required>
						<input type="hidden" name="code" id="code">
					</div>
				</div>

				<div class="form-group row">
					<label for="dob" class="col-xs-2 col-form-label">Date of Birth</label>
					<div class="col-xs-10">
						<input class="form-control" type="date" name="dob" value="{{ old('dob') }}" id="dob">
					</div>
				</div>
				<div class="form-group row">
					<label for="gender" class="col-xs-2 col-form-label">Gender</label>
					<div class="col-xs-10">
						<input type="radio" id="male" name="gender" value="male">
						<label for="male">Male</label><br>
						<input type="radio" id="female" name="gender" value="female">
						<label for="female">Female</label><br>
					</div>
				</div>

				<div class="form-group row password">
					<label for="password" class="col-xs-2 col-form-label">Password</label>
					<div class="col-xs-10">
						<input class="form-control" type="password" name="password" id="password" placeholder="Password" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="password_confirmation" class="col-xs-2 col-form-label">Password Confirmation</label>
					<div class="col-xs-10">
						<input class="form-control" type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" id="password_confirmation" placeholder="Re-type Password">
					</div>
				</div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Add Passenger</button>
						<a href="{{route('admin.passenger.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>
<style type="text/css">.iti { width: 100%; }</style>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/css/intlTelInput.css">
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/intlTelInput-jquery.min.js"></script>
 <script>
	jq = jQuery.noConflict();
    use :
   		var s = jq("#mobile_no").intlTelInput({
			autoPlaceholder: 'polite',
   			separateDialCode: true,
   			formatOnDisplay: true,
   			utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/utils.js"
   		});

   	insteadof :
   		var d = jq("#mobile_no").intlTelInput("getSelectedCountryData").dialCode;
   		jq("#code").val(d);
		jq(document).on('countrychange', function (e, countryData) {
        	jq("#code").val((jq("#mobile_no").intlTelInput("getSelectedCountryData").dialCode));
    	});
</script>
@endsection