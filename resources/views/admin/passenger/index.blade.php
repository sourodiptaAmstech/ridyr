@extends('admin.layout.base')

@section('title', 'Passengers ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Passengers
            </h5>
            <a href="{{ route('admin.passenger.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New passenger</a>
            <table class="table table-hover table-striped table-bordered dataTable" id="table-pass">
                <thead>
                    <tr>
                        {{-- <th>ID</th> --}}
                        <th>Passenger Name</th>
                      {{--   <th>Last Name</th> --}}
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Action</th>
                    </tr>
                </thead>
                {{-- <tbody>
                    @foreach($passengers as $index => $passenger)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $passenger->first_name }}</td>
                        <td>{{ $passenger->last_name }}</td>
                        <td>{{ $passenger->email }}</td>
                        <td>{{ $passenger->mobile }}</td>
                        @if($passenger->passenger_profile != null)
                        <td>{{ $passenger->passenger_profile->rating }}</td>
                        @else
                        <td></td>
                        @endif
                        <td>
                            <a href="{{ route('admin.passenger.edit', $passenger->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                            <form action="{{ route('admin.passenger.destroy', $passenger->id) }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE">
                                <button class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody> --}}
                <tfoot>
                    <tr>
                        {{-- <th>ID</th> --}}
                        <th>Passenger Name</th>
                        {{-- <th>Last Name</th> --}}
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $('#table-pass').DataTable( {
            // "order": [[0, 'desc']],
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url":"{{route('admin.ajax-passenger')}}",
                "dataType":"json",
                "type":"POST",
                "data":{"_token":"<?= csrf_token() ?>"}
            },
            "columns":[
                // {"data":"id"},
                {"data":"passenger_name", "orderable":false},
                {"data":"email", "orderable":false},
                {"data":"mobile", "orderable":false},
                {"data":"action","searchable":false,"orderable":false}
            ],
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
        } );
    </script>
@endsection