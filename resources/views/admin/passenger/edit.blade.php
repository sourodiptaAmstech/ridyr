@extends('admin.layout.base')

@section('title', 'Update Passenger ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
    	    <a href="{{ route('admin.passenger.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Update Passenger</h5>

            <form class="form-horizontal" action="{{route('admin.passenger.update', $passenger->_id )}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
            	<input type="hidden" name="_method" value="PATCH">
				<div class="form-group row">
					<label for="first_name" class="col-xs-2 col-form-label">First Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $passenger->passenger_profile['first_name'] }}" name="first_name" required id="first_name" placeholder="First Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="last_name" class="col-xs-2 col-form-label">Last Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $passenger->passenger_profile['last_name'] }}" name="last_name" required id="last_name" placeholder="Last Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="email" class="col-xs-2 col-form-label">Email</label>
					<div class="col-xs-10">
						<input class="form-control" type="email" value="{{ $passenger->username }}" name="email" required id="email" placeholder="Email">
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-2 col-form-label">Picture</label>
					<div class="col-xs-10">
					@if($passenger->passenger_profile['picture'] != '')
                    	@if(File::exists(storage_path('app/public' .str_replace("storage", "", $passenger->passenger_profile['picture']))))
                            <img style="height: 90px; margin-bottom: 15px;" src="{{URL::asset($passenger->passenger_profile['picture'])}}">
                        @else
                            <img src="{{$passenger->passenger_profile['picture']}}" style="height: 60px; margin-bottom: 15px;">
                        @endif
                    @else
                     	<img style="height: 60px; margin-bottom: 15px;" src="{{URL::asset('asset/NO_IMG.png')}}">
                    @endif
						<input type="file" accept="image/*" name="picture" class="dropify form-control-file" id="picture" aria-describedby="fileHelp">
					</div>
				</div>

				<div class="form-group row">
					<label for="mobile" class="col-xs-2 col-form-label">Mobile</label>
					<div class="col-xs-10">
						<input class="form-control" type="tel" value="{{ $passenger->passenger_profile['mobile'] }}" name="mobile" id="mobile" required>
						<input type="hidden" name="code" id="code" value="{{$passenger->passenger_profile['isdCode']}}">
					</div>
				</div>

				<div class="form-group row">
					<label for="dob" class="col-xs-2 col-form-label">Date of Birth</label>
					<div class="col-xs-10">
						<input class="form-control" type="date" name="dob" value="{{ $passenger->passenger_profile['dob'] }}" id="dob">
					</div>
				</div>
				<div class="form-group row">
					<label for="gender" class="col-xs-2 col-form-label">Gender</label>
					<div class="col-xs-10">
						<input type="radio" id="male" name="gender" value="male" {{ $passenger->passenger_profile['gender'] == 'male' ? 'checked' : ''}}>
						<label for="male">Male</label><br>
						<input type="radio" id="female" name="gender" value="female" {{ $passenger->passenger_profile['gender'] == 'female' ? 'checked' : ''}}>
						<label for="female">Female</label><br>
					</div>
				</div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Update Passenger</button>
						<a href="{{route('admin.passenger.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>
<style type="text/css">
	.iti { width: 100%; }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/css/intlTelInput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/intlTelInput-jquery.min.js"></script>
<script>
	jq = jQuery.noConflict();
    use :
   		var s = jq("#mobile").intlTelInput({
   			autoPlaceholder: 'polite',
   			separateDialCode: true,
   			formatOnDisplay: true,
   			utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/utils.js"
   		});
   	insteadof :
   		var countryData = window.intlTelInputGlobals.getCountryData();
   		var iso2;
		var isdcode = jq("#code").val();
		for (var i = 0; i < countryData.length; i++) {
			if (countryData[i].dialCode == parseInt(isdcode)){
				iso2 = countryData[i].iso2;
				break;
			}
		}
		if(iso2){
			jq("#mobile").intlTelInput("setCountry", iso2);
		}
		jq(document).on('countrychange', function (e, countryData) {
        	jq("#code").val((jq("#mobile").intlTelInput("getSelectedCountryData").dialCode));
    	});
</script>
@endsection
