<!-- Header -->
<div class="site-header">
	<nav class="navbar navbar-light">
		<div class="navbar-left">
			<a class="navbar-brand" href="{{url('admin/dashboard')}}">
			<img class="image" src="{{URL::asset(App\Http\Controllers\Controller::setting()[1]->value)}}" style="height: 100%; width: 100%">
			</a>
			<div class="toggle-button dark sidebar-toggle-first float-xs-left hidden-md-up">
				<span class="hamburger"></span>
			</div>
			<div class="toggle-button-second dark float-xs-right hidden-md-up">
				<i class="ti-arrow-left"></i>
			</div>
			<div class="toggle-button dark float-xs-right hidden-md-up" data-toggle="collapse" data-target="#collapse-1">
				<span class="more"></span>
			</div>
		</div>
		<div class="navbar-right navbar-toggleable-sm collapse" id="collapse-1">
			<div class="toggle-button light sidebar-toggle-second float-xs-left hidden-sm-down">
				<span class="hamburger"></span>
			</div>

			<ul class="nav navbar-nav">
				<li class="nav-item hidden-sm-down">
					<a class="nav-link toggle-fullscreen" href="javascript:void(0)">
						<i class="ti-fullscreen"></i>
					</a>
				</li>
			</ul>
			<ul class="nav navbar-nav float-md-right">
				<li class="nav-item dropdown hidden-sm-down">
					<a href="#" data-toggle="dropdown" aria-expanded="false">
						{{App\Http\Controllers\Controller::adminProfile()->first_name}}
						<span class="avatar box-48">

						@if(isset(App\Http\Controllers\Controller::adminProfile()->picture))
	                    	@if(File::exists(storage_path('app/public' .str_replace("storage", "", App\Http\Controllers\Controller::adminProfile()->picture))))
                                <img src="{{URL::asset(App\Http\Controllers\Controller::adminProfile()->picture)}}" style="height: 100%;">
                            @else
                                <img src="{{URL::asset('main/avatar.jpg')}}">
                            @endif
	                   	@else
                     		<img src="{{URL::asset('main/avatar.jpg')}}">
	                    @endif
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right animated fadeInUp">
						<a class="dropdown-item" href="{{route('admin.profile')}}">
							<i class="ti-user mr-0-5"></i> Profile
						</a>
						<a class="dropdown-item" href="{{route('admin.password')}}">
							<i class="ti-settings mr-0-5"></i> Change Password
						</a>
						<div class="dropdown-divider"></div>

						<a class="dropdown-item" href="{{ route('admin.logout') }}" onclick="event.preventDefault(); document.getElementById('logout_form').submit();">
							<i class="ti-power-off mr-0-5"></i> Sign out</a>
						<form id="logout_form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                    	{{ csrf_field() }}
                		</form>

					</div>
				</li>
			</ul>

		</div>
	</nav>
</div>