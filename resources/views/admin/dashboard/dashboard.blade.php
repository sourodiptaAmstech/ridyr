@extends('admin.layout.base')

@section('title', 'Dashboard ')

@section('styles')
	<link rel="stylesheet" href="{{asset('main/vendor/jvectormap/jquery-jvectormap-2.0.3.css')}}">
@endsection

@section('content')

<div class="content-area py-1">
<div class="container-fluid">
    <div class="row row-md">
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right"><span class="bg-danger"></span><i class="ti-rocket"></i></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">Total No. of Rides</h6>
					<h1 class="mb-1">{{-- {{$rides->count()}} --}}</h1>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right"><span class="bg-success"></span><i class="ti-bar-chart"></i></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">Revenue</h6>
					<h1 class="mb-1">
						{{-- @if(isset($totalEarn))
							${{$totalEarn}}
						@else
							0
						@endif --}}
					</h1>
					<i class="fa fa-caret-up text-success mr-0-5"></i><span>from {{-- {{$rides->count()}} --}} Rides</span>
				</div>
			</div>
		</div>
		{{-- <div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right"><span class="bg-primary"></span><i class="ti-view-grid"></i></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">No. of Service Types</h6>
					<h1 class="mb-1">{{$service}}</h1>
				</div>
			</div>
		</div> --}}
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right"><span class="bg-warning"></span><i class="ti-archive"></i></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">Total Cancelled Rides</h6>
					<h1 class="mb-1">
						{{-- @if(isset($total_cancel_rides))
							{{$total_cancel_rides}}
						@else
						0
						@endif --}}
					</h1>
					<i class="fa fa-caret-down text-danger mr-0-5"></i><span>for  
						{{-- @if(isset($total_cancel_rides)) 
							@if($total_cancel_rides == 0) 
								0.00%
							@else 
								{{round($total_cancel_rides*100/$rides->count(),2)}}% 
							@endif
						@else
						0.00%
						@endif --}}
					Rides</span>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right"><span class="bg-primary"></span><i class="ti-view-grid"></i></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">User Cancelled Count</h6>
					<h1 class="mb-1">{{-- {{$cancel_bypassenger}} --}}</h1>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right"><span class="bg-danger"></span><i class="ti-bar-chart"></i></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">Driver Cancelled Count</h6>
					<h1 class="mb-1">{{-- {{$cancel_bydriver}} --}}</h1>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right"><span class="bg-warning"></span><i class="ti-rocket"></i></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">Total No. of Reg. Drivers</h6>
					<h1 class="mb-1">{{-- {{$total_reg_driver}} --}}</h1>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right"><span class="bg-success"></span><i class="ti-bar-chart"></i></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">No. of Scheduled Rides</h6>
					<h1 class="mb-1">{{-- {{$scheduled_rides}} --}}</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="row row-md mb-2">
		<div class="col-md-12">
				<div class="box bg-white">
					<div class="box-block clearfix">
						<h5 class="float-xs-left">Recent Rides</h5>
					</div>
					<table class="table mb-md-0">
						<tbody>
						{{-- @foreach($completed_rides as $index => $completed_ride)
							<tr>
								<th scope="row">{{$index + 1}}</th>
								<td>{{$completed_ride->userDriver->first_name}} {{$completed_ride->userDriver->last_name}}</td>
								<td>
									<a class="text-primary" href="{{route('admin.statement.details',[$completed_ride->passenger_request_id,'dashboard'])}}"><span class="underline">View Ride Details</span></a>								
								</td>
								<td>
									<span class="text-muted">{{$completed_ride->created_at->diffForHumans()}}</span>
								</td>
								<td>
									<span class="tag tag-success">{{$completed_ride->request_status}}</span>
								</td>
							</tr>
						@endforeach --}}
						</tbody>
					</table>
				</div>
			</div>

		</div>

	</div>
</div>
@endsection