@extends('admin.layout.base')

@section('title', 'Site Setting')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">Site Setting</h5>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Key</th>
                        <th>Value</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($site_settings as $index => $site_setting)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $site_setting->key }}</td>
                        <td>
                            @if($site_setting->key == 'site_title'||$site_setting->key == 'site_copyright')
                            {{ $site_setting->value }}
                            @else
                                @if(isset($site_setting->value))
                                    <img src="{{URL::asset($site_setting->value)}}" style="height: 50px; width: 65px;">
                                @else
                                    <img src="{{URL::asset('asset/NO_IMG.png')}}" style="height: 50px; width: 65px;">
                                @endif
                            @endif
                        </td>
                        <td style="line-height: 34px;">
                            <a href="{{ route('admin.setting.edit', $site_setting->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Key</th>
                        <th>Value</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection