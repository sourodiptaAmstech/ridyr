@extends('admin.layout.base')

@section('title', 'Change Password ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
    		<a href="{{ route('admin.dashboard.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Change Password</h5>

            <form class="form-horizontal" action="{{route('admin.password.update')}}" method="POST" role="form">
            	{{csrf_field()}}

            	<div class="form-group row">
					<label for="old_password" class="col-xs-2 col-form-label">Old Password</label>
					<div class="col-xs-10">
						<input class="form-control" type="password" value="{{ old('old_password') }}" name="old_password" id="old_password" placeholder="Old Password" required>
					</div>
				</div>

				<div class="form-group row">
					<label for="password" class="col-xs-2 col-form-label">Password</label>
					<div class="col-xs-10">
						<input class="form-control" type="password" name="password" value="{{ old('password') }}" id="password" placeholder="New Password" required>
					</div>
				</div>

				<div class="form-group row">
					<label for="password_confirmation" class="col-xs-2 col-form-label">Password Confirmation</label>
					<div class="col-xs-10">
						<input class="form-control" type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" id="password_confirmation" placeholder="Re-type New Password" required>
					</div>
				</div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Change Password</button>
						<a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>

			</form>
		</div>
    </div>
</div>

@endsection
