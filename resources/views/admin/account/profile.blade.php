@extends('admin.layout.base')

@section('title', 'Update Profile ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
    		<a href="{{ route('admin.dashboard.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Update Profile</h5>

            <form class="form-horizontal" action="{{route('admin.profile.update')}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}

				<div class="form-group row">
					<label for="first_name" class="col-xs-2 col-form-label">First Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ App\Http\Controllers\Controller::adminProfile()->first_name }}" name="first_name" id="first_name" placeholder="First Name" required>
					</div>
				</div>

				<div class="form-group row">
					<label for="last_name" class="col-xs-2 col-form-label">Last Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ App\Http\Controllers\Controller::adminProfile()->last_name }}" name="last_name" required id="last_name" placeholder="Last Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="email" class="col-xs-2 col-form-label">Email</label>
					<div class="col-xs-10">
						<input class="form-control" type="email" required name="email" value="{{ isset(Auth::user()->username) ? Auth::user()->username : '' }}" id="email" placeholder="Email">
					</div>
				</div>

				<div class="form-group row">
					<label for="mobile" class="col-xs-2 col-form-label">Mobile</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ App\Http\Controllers\Controller::adminProfile()->mobile_no }}" name="mobile" required id="mobile" placeholder="Mobile">
						<input type="hidden" name="code" id="code" value="{{ App\Http\Controllers\Controller::adminProfile()->isd_code }}">
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-2 col-form-label">Picture</label>
					<div class="col-xs-10">
						@if(isset(App\Http\Controllers\Controller::adminProfile()->picture))
	                    	@if(File::exists(storage_path('app/public' .str_replace("storage", "", App\Http\Controllers\Controller::adminProfile()->picture))))
                                <img style="height: 90px; margin-bottom: 15px;" src="{{URL::asset(App\Http\Controllers\Controller::adminProfile()->picture)}}">
                            @else
                                <img style="height: 60px; margin-bottom: 15px;" src="{{URL::asset('asset/NO_IMG.png')}}">
                            @endif
	                   	@else
                     		<img style="height: 60px; margin-bottom: 15px;" src="{{URL::asset('asset/NO_IMG.png')}}">
	                    @endif
						<input type="file" accept="image/*" name="picture" class="dropify form-control-file" aria-describedby="fileHelp">
					</div>
				</div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Update Profile</button>
						<a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>
<style type="text/css">.iti { width: 100%; }</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/css/intlTelInput.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/intlTelInput-jquery.min.js"></script>
<script>
	jq = jQuery.noConflict();
    use :
   		var s = jq("#mobile").intlTelInput({
   			autoPlaceholder: 'polite',
   			separateDialCode: true,
   			formatOnDisplay: true,
   			utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/utils.js"
   		});
   	insteadof :
   		var countryData = window.intlTelInputGlobals.getCountryData();
   		var iso2;
		var isdcode = jq("#code").val();
		for (var i = 0; i < countryData.length; i++) {
			if (countryData[i].dialCode == parseInt(isdcode)){
				iso2 = countryData[i].iso2;
				break;
			}
		}
		if(iso2){
			jq("#mobile").intlTelInput("setCountry", iso2);
		}
		jq(document).on('countrychange', function (e, countryData) {
        	jq("#code").val((jq("#mobile").intlTelInput("getSelectedCountryData").dialCode));
    	});
</script>
@endsection
