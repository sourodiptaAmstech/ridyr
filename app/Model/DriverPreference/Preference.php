<?php

namespace App\Model\DriverPreference;

//use Illuminate\Database\Eloquent\Model;
//use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\Model;
class Preference extends Model
{
    //
    //use Notifiable;
    //
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $collection = 'drivers_preference';
   // protected $primaryKey = 'driver_id';
}
