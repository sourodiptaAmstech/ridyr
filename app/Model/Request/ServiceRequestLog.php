<?php

namespace App\Model\Request;

//use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;

class ServiceRequestLog extends Model
{
    //
   // use Notifiable;
    //
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $collection = 'service_request_log';
  //  protected $primaryKey = 'driver_service_type_id';
}
