<?php

namespace App\Model\Preference;

//use Illuminate\Database\Eloquent\Model;
//use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\Model;
class Preference extends Model
{
    //
    //use Notifiable;
    //
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $collection = 'preference_mst';
}
