<?php

namespace App\Model\Setting;

use Jenssegers\Mongodb\Eloquent\Model;

class Setting extends Model
{
    //
   // use Notifiable;
    //
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $collection = 'settings';

}
