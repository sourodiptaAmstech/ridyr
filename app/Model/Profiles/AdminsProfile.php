<?php

namespace App\Model\Profiles;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;

class AdminsProfile extends Model
{
    protected $collection = 'admins_profile';

}
