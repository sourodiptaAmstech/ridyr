<?php

namespace App\Model\ServiceType;

//use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;
class MstServiceType extends Model
{
    //
   // use Notifiable;
    //
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $collection = 'service_types';
  //  protected $primaryKey = 'id';
}
