<?php

namespace App\Model\ServiceType;

//use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;

class DriverServiceType extends Model
{
    //
   // use Notifiable;
    //
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $collection = 'driver_service_type';
  //  protected $primaryKey = 'driver_service_type_id';
}
