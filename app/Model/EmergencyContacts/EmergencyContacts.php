<?php

namespace App\Model\EmergencyContacts;

//use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;

class EmergencyContacts extends Model
{
   
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $collection = 'user_emergency_contacts';
  //  protected $primaryKey = 'emergency_id';
    
}
