<?php

namespace App\Services;

use App\Model\Preference\Preference;
use Storage;

class PreferencesService
{
    private $Preferences;
    private function checkNull($field){
        if($field===null)
        return "";
        else
        return $field;
    }
    private function createPreferences($data){
        $Preferences = new Preference();
        $Preferences->user_id=$data->user_id;
        $Preferences->preference_name=$data->preference_name;
        $Preferences->save();
        return $Preferences;
    }
    private function updatePreferences($data){
        try{
            $Preferences=Preference::where("_id",$data->prefernce_id)->firstOrFail();
            $Preferences->preference_name=$data->preference_name;
            $Preferences->save();
            return ['message'=>"Preferences is successfully updated","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>200];

        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"Your profile cannot be updated!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    private function getPreferences(){
        try{
            $Preferences=Preference::get();

            return ['message'=>"Preference Data","data"=>$Preferences,"errors"=>array("exception"=>["Everything OK"],"error"=>[]),"statusCode"=>200];

        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    public function accessUpdatePreferences($data){

        return $this->updatePreferences($data);

    }
    public function accessGetPreferences(){

        return $this->getPreferences();

    }
    public function accessCreatePreferences($data){

        return $this->createPreferences($data);

    }
}
