<?php
namespace App\Services;



use App\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\UsersDevices;
use Illuminate\Support\Facades\DB;
use App\Model\Profiles\PassengersProfile;
use App\Model\Profiles\DriverProfiles;
use App\Services\TwilioSMS;
use App\Services\UserService;



class EstimatedFareService 
{
   public function metersToMiles($meters){
        $number=$meters * 0.000621371;
        return number_format((float)$number, 2, '.', '');
    }
   public function metersToKilometers($meters){
       $number=$meters * 0.001;
       return number_format((float)$number, 2, '.', '');
    }
    public function fareCalucation($distance,$duration,$waitTime,$perDistanceInKmCost,$perMinuteCost,$insurence,$perMinWaitCost,$minimumWaitTime){
        $costPerKm=($this->metersToKilometers($distance)*(float)$perDistanceInKmCost);
        $costPerSecond=(float)$duration*($perMinuteCost/60);
        $waitTimeCost=0;
        if(($waitTime-$minimumWaitTime)>$minimumWaitTime){
            $waitTimeCost=($waitTime-$minimumWaitTime)*$perMinWaitCost;
        }
        $total=$costPerKm+$costPerSecond+$insurence+$waitTimeCost;
        return number_format((float)$total, 2, '.', '');
    }
    public function toGetDuration($seconds){
        $init = $seconds;
        $hours = floor($init / 3600);
        $minutes = floor(($init / 60) % 60);
        $seconds = $init % 60;
        return ["hours"=>$hours,"minutes"=>$minutes,"seconds"=>$seconds];
    }

    private function getFare($services,$data){
        try{
            //  Mapbox to get the estimated time and distances
            $distance=0;
            $duration=0;
            $waitTime=0;
            $source=$data->source;
            $latLog="";
            $latLog=$source['longitude'].",".$source['latitude'];
            $destination=$data->destination;
            $waypoint=$data->waypoint;
            foreach($waypoint as $key=>$val){
                $latLog=$latLog.";".$val['longitude'].",".$val['latitude'];
            }
            $latLog=$latLog.";".$destination['longitude'].",".$destination['latitude'];
          //  echo "https://api.mapbox.com/directions/v5/mapbox/driving-traffic/".$latLog."?access_token=pk.eyJ1IjoicmlkeXJvcHMiLCJhIjoiY2thZXFpYXU3MmthbzJ6b3phdnhqcmYyayJ9.FcLBkWmiEPZcaNi3dGpfsw";exit;
            $estimatedFareByServices=[];
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.mapbox.com/directions/v5/mapbox/driving-traffic/".$latLog."?access_token=pk.eyJ1IjoicmlkeXJvcHMiLCJhIjoiY2thZXFpYXU3MmthbzJ6b3phdnhqcmYyayJ9.FcLBkWmiEPZcaNi3dGpfsw",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));

            $response = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);

            if($httpcode===200){
                $response=json_decode($response,true);                
                if(array_key_exists("routes",$response)){                    
                    if(array_key_exists(0,$response['routes'])){
                        if(array_key_exists("duration",$response['routes'][0]) && array_key_exists("duration",$response['routes'][0])){
                            $duration=$response['routes'][0]['duration'];
                            $distance=$response['routes'][0]['distance'];
                        }
                    }
                }
            }
            //echo $duration; echo "!=0 &&";echo $distance; echo "ss"; echo $this->metersToKilometers($distance);

            $durationArray=$this->toGetDuration($duration);
            $distanceInKm=$this->metersToKilometers($distance);
            $distanceInMiles=$this->metersToMiles($distance);

            if($duration!=0 && $distance!=0){
                $serviceData=(array)($services['data']);
                foreach($serviceData as $key =>$val){
                    $estimatedFareByServices[]=array(
                        "name"=>$val['name'],
                        "request_id"=>$data->request_id,
                        "image"=>$val['image'],
                        "service_type_id"=>((array)$val['_id'])['oid'],
                        "estimated_fare"=>[
                            "ride_insurance"=>$val['insure_price'],
                            "per_minute"=>$val['minute'],
                            "per_distance_km"=>$val['distance'],
                            "minimum_waiting_time_in_minutes"=>$val['min_waiting_time'],
                            "waiting_charge_per_min"=>$val['min_waiting_charge'],
                            "waitTime"=>$waitTime,
                            "estimated_duration_hr"=> $durationArray["hours"],
                            "estimated_duration_min"=> $durationArray["minutes"],
                            "estimated_duration_sec"=> $durationArray["seconds"],
                            "estimated_duration"=>$duration,
                            "estimated_distance_km"=>$distanceInKm,
                            "estimated_distance_miles"=>$distanceInMiles,
                            "estimated_distance_meters"=>$distance,
                            "estimated_cost"=>$this->fareCalucation($distance,$duration,$waitTime,$val['distance'],$val['minute'],$val['insure_price'],$val['min_waiting_charge'],$val['min_waiting_time']),
                            "currency"=>"$"
                            ]
                        );
                    }
                }
            


            if(!empty($estimatedFareByServices)){
                return ['message'=>"Cars are available for your location at that given moment.","data"=>$estimatedFareByServices,"errors"=>array("exception"=>["Service found"],"error"=>[]),"statusCode"=>200];
            }

            return ['message'=>"No cars are available for your location at that given moment.","data"=>(object)[],"errors"=>array("exception"=>["Service not found estimated"],"error"=>[]),"statusCode"=>404];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    private function getServicesInRadius($radius,$data){
        try{
            $source=$data->source;
            $driversServies = DB::collection("drivers_profile")
            ->where("status","online")
            ->where("service_status","active")           
            ->where('location', 'near', [
                '$geometry' => [
                    'type' => 'Point',
                    'coordinates' => [
                        (float)$source['longitude'], // longitude
                        (float)$source['latitude'], // latitude
                    ],
                ],'$maxDistance' => $radius*1000,
            ])->select('active_service_type')
            ->distinct()->get()->toArray();
            if(empty($driversServies)){
                return ['message'=>"No cars are available for your location at that given moment.","data"=>(object)[],"errors"=>array("exception"=>["Service not found"],"error"=>[]),"statusCode"=>404];
            }
            else{
                // get services details
                $serviceTypesDetails=[];
                foreach($driversServies as $key =>$val){
                    $servics=DB::collection("service_types")->where("_id",$val)->get()->toArray();
                    if(!empty($servics)){
                        $serviceTypesDetails[]=$servics[0];
                    }
                }
            }
           // print_r((array)$serviceTypesDetails[0]["_id"]); exit;
            
            if(!empty($serviceTypesDetails)){
                return ['message'=>"Cars are available for your location at that given moment.","data"=>(object)$serviceTypesDetails,"errors"=>array("exception"=>["Service found"],"error"=>[]),"statusCode"=>200];
            }
            return ['message'=>"No cars are available for your location at that given moment.","data"=>(object)[],"errors"=>array("exception"=>["Service not found"],"error"=>[]),"statusCode"=>404];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    public function getFareByServiceType($data){
        try{
            $serviceTypesDetails=[];
            $servics=DB::collection("service_types")->where("_id",$data->service_type_id)->get()->toArray();
            if(!empty($servics)){
                $serviceTypesDetails[]=$servics[0];
            }
            if(!empty($serviceTypesDetails)){
                return ['message'=>"Cars are available for your location at that given moment.","data"=>(object)$serviceTypesDetails,"errors"=>array("exception"=>["Service found"],"error"=>[]),"statusCode"=>200];
            }
            return ['message'=>"No cars are available for your location at that given moment.","data"=>(object)[],"errors"=>array("exception"=>["Service not found"],"error"=>[]),"statusCode"=>404];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }



    // Accessing the services 

    public function accessGetFare($data){
        $retuns=$this->getServicesInRadius(8,$data);
        if($retuns['statusCode']==200){
          $retuns=$this->getFare($retuns,$data);
        }
        return $retuns;
    }

    public function accessGetFareByServiceType($data){
        $retuns=$this->getFareByServiceType($data);
        if($retuns['statusCode']==200){
            $retuns=$this->getFare($retuns,$data);
          }
          return $retuns; 

    }
}