<?php
namespace App\Services;

class UnitConvertionService
{
    public function convertToUTC($date,$timeZone){
        $timeZoneArray=explode(":",trim(trim(preg_replace('/\s+/', '', str_replace("GMT","",trim(preg_replace('/\s+/', '', $timeZone))))),''));
        $time="";
        if(is_array($timeZoneArray)){
            if($timeZoneArray[0]!==""){
                if((int)$timeZoneArray[0]>0){
                    $time=-$timeZoneArray[0]." hour";
                    if(array_key_exists(1,$timeZoneArray)){
                        $time=$time." -".$timeZoneArray[1]." minutes";
                    }
                }
                else{
                    $time=-$timeZoneArray[0]." hour";
                    if(array_key_exists(1,$timeZoneArray)){
                        $time=$time." +".$timeZoneArray[1]." minutes";
                    }
                }
            }
            if($time!==""){
                $date=date("Y-m-d H:i",strtotime($time,strtotime($date)));
            }
        }
        return $date;
    }
    public function convertFromUTC($date,$timeZone){
        $timeZoneArray=explode(":",trim(trim(preg_replace('/\s+/', '', str_replace("GMT","",trim(preg_replace('/\s+/', '', $timeZone))))),''));
        $time="";
        if(is_array($timeZoneArray)){
            if($timeZoneArray[0]!==""){
                if((int)$timeZoneArray[0]>0){
                    $time=$timeZoneArray[0]." hour";
                    if(array_key_exists(1,$timeZoneArray)){
                        $time=$time." +".$timeZoneArray[1]." minutes";
                    }
                }
                else{
                    $time=$timeZoneArray[0]." hour";
                    if(array_key_exists(1,$timeZoneArray)){
                        $time=$time." -".$timeZoneArray[1]." minutes";
                    }
                }
            }
            if($time!==""){
                $date=date("Y-m-d H:i",strtotime($time,strtotime($date)));
            }
        }
        //   echo $timeZone;
        //  echo $date; exit;
        return $date;
    }
}