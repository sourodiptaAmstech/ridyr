<?php
namespace App\Services;


use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use App\Model\ServiceType\DriverServiceType;
use App\Services\ServiceTypeMst;

class DriversServiceType 
{
    private function create($data){
        try{
            $ServiceType=new DriverServiceType();
            $ServiceType->user_id=$data->user_id;
            $ServiceType->service_type_id=$data->service_type_id;
            $ServiceType->registration_no=$data->registration_no;
            $ServiceType->registration_expire=$data->registration_expire;
            $ServiceType->model=$data->model;
          //  $ServiceType->vechile_identification_no=$data->vechile_identification_no;
            $ServiceType->model_year=$data->model_year;
            $ServiceType->vechile_make=$data->vechile_make;
            $ServiceType->vechile_make=$data->vechile_make;
            $ServiceType->save();
            return ['message'=>"Services Added","data"=>$ServiceType,"errors"=>array("exception"=>["OK"],"error"=>[]),"statusCode"=>201];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>401];
        }
    }

    private function update($data){
        try{
            $ServiceType=DriverServiceType::where("user_id",$data->user_id)->first();
            //$ServiceType->user_id=$data->user_id;
            $ServiceType->service_type_id=$data->service_type_id;
            $ServiceType->registration_no=$data->registration_no;
            $ServiceType->registration_expire=$data->registration_expire;
            $ServiceType->model=$data->model;
            $ServiceType->vechile_identification_no=$data->vechile_identification_no;
            $ServiceType->save();
            return ['message'=>"Services Added","data"=>$ServiceType,"errors"=>array("exception"=>["OK"],"error"=>[]),"statusCode"=>201];
      
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }


    private function getService($data){
        try{
            $ServiceType=DriverServiceType::where("user_id",$data->user_id)->first();
            $ServiceTypeMst=new ServiceTypeMst();
          //  print_r($ServiceType);// exit;
            $ServiceTypeMstData=$ServiceTypeMst->accessGetNameByID($ServiceType);
        //    print_r($ServiceTypeMstData); exit;
   // echo $ServiceTypeMstData->name; exit;
            $ServiceType->serviceName=$ServiceTypeMstData->name;
            return ['message'=>"Services get","data"=>$ServiceType,"errors"=>array("exception"=>["OK"],"error"=>[]),"statusCode"=>201];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>401];
        }
    }
  
    
    public function accessCreate($data){
        return $this->create($data);
    }
    public function accessUpdate($data){
        return $this->update($data);
    }
    public function accessGetService($data){
        return $this->getService($data); 
    }
    
}