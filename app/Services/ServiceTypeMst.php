<?php

namespace App\Services;

use App\Model\ServiceType\MstServiceType;

class ServiceTypeMst
{
    private function create($data){
        return true;
    }
    private function update($data){
        return true;

    }
    private function get(){
        $service=MstServiceType::where("status",1)->get()->toArray();
        return $service;
    }
    private function getNameByID($data){
     //   echo $data->service_type_id ; exit;
        $service =MstServiceType::select("name","provider_name")->where("_id",$data->service_type_id)->first();
        return $service;
    }
    public function accessCreate($data){
        return $this->create($data);
    }
    public function accessGet(){
        return $this->get();
    }
    public function accessUpdate($data){
        return $this->update($data);
    }
    public function accessGetNameByID($data){
        return $this->getNameByID($data);
    }
}


