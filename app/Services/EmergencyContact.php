<?php

namespace App\Services;

use App\Model\EmergencyContacts\EmergencyContacts;

class EmergencyContact
{
    private function createContact($data){
        $EmergencyContacts=new EmergencyContacts();
        $EmergencyContacts->contact_no=$data->contact_no;
        $EmergencyContacts->name=$data->name;
        $EmergencyContacts->isd_code=$data->isd_code;
        $EmergencyContacts->user_id=$data->user_id;
        $EmergencyContacts->save();
        return $EmergencyContacts;
    }
    private function updateContact($data){
        $EmergencyContacts=EmergencyContacts::where("_id",$data->emergency_id)->first();
        $EmergencyContacts->contact_no=$data->contact_no;
        $EmergencyContacts->name=$data->name;
        $EmergencyContacts->isd_code=$data->isd_code;
        $EmergencyContacts->user_id=$data->user_id;
        $EmergencyContacts->save();
        return $EmergencyContacts;

    }
    private function getContact($data){
        $EmergencyContacts=EmergencyContacts::select('_id','user_id','name','contact_no','isd_code')->where("user_id",$data->user_id)->get()->toArray();
        return $EmergencyContacts;
    }
    public function accessCreateContact($data){
        return $this->createContact($data);
    }
    public function accessGetContact($data){
        return $this->getContact($data);
    }
    public function accessUpdateContact($data){
        return $this->updateContact($data);
    }
}


