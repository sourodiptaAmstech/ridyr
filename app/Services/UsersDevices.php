<?php

namespace App\Services;

use App\Model\Device\UserDevices;
class UsersDevices 
{
    private $Devices;
    private function createDevices($data){
        $Devices = new UserDevices();
        $Devices->user_id=$data->user_id;
        $Devices->uid=$data->device_id;
        $Devices->token=$data->device_token;
        $Devices->device_type=$data->device_type;
        $Devices->latitude=$data->device_latitude;
        $Devices->longitude=$data->device_longitude;
        $Devices->location=(object)["type"=>"Point","coordinates"=>[(float)$data->device_longitude,(float)$data->device_latitude]];
        $Devices->save();
        return $Devices;
    }
 
    private function updateDevices($data){
        $Devices =UserDevices::where("user_id",$data->user_id)->first();
        $Devices->uid=$data->device_id;
        $Devices->token=$data->device_token;
        $Devices->device_type=$data->device_type;
        //$Devices->latitude=$data->device_latitude;
        //$Devices->longitude=$data->device_longitude;
       // $Devices->location=(object)["type"=>"Point","coordinates"=>[$data->device_longitude,$data->device_latitude]];
        $Devices->save();
        return $Devices;
    }
    private function updateLocation($data){
        $Devices =UserDevices::where("user_id",$data->user_id)->first();       
        $Devices->latitude=(float)$data->device_latitude;
        $Devices->longitude=(float)$data->device_longitude;
        $Devices->location=(object)["type"=>"Point","coordinates"=>[(float)$data->device_longitude,(float)$data->device_latitude]];
        $Devices->save();
        return $Devices;
    }

    private function getDevice($data){
        $Devices =UserDevices::where("user_id",$data->user_id)->first();
        return $Devices;
    }
    
    public function accessCreateDevices($data){
        return $this->createDevices($data);
    }
    public function accessUpdateDevices($data){
        return $this->updateDevices($data);
    }
    public function accessUpdateLocation($data){
        return $this->updateLocation($data);
    }

    public function accessGetDevice($data){
        return $this->getDevice($data);
    }

}
