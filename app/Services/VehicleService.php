<?php

namespace App\Services;

use App\Model\Vehicle\Vehicle;

class VehicleService
{
    private function create($data){
        return true;
    }
    private function update($data){
        return true;

    }
    private function get(){
         try{
             //https://stackoverflow.com/questions/38963608/setup-mongodb-extension-for-php7
            $vehicle=Vehicle::whereIn('make', ['Toyota', 'Honda', 'Hundai','Nissan'])->orderBy('year', 'DESC')->get()->toArray();
            if(!empty($vehicle))
            return ['message'=>"Vehicle data send.","data"=>$vehicle,"errors"=>array("exception"=>["Everything is OK"],"error"=>[]),"statusCode"=>200];            
            else
            return ['message'=>"Vehicle not found.","data"=>[],"errors"=>array("exception"=>["No Content"],"error"=>[]),"statusCode"=>404];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e),"statusCode"=>500];            
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e),"statusCode"=>500];
        }
        catch (Exception $e) {
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["Internal Server Error"],"e"=>$e),"statusCode"=>500];
        }
    }
    public function accessCreate($data){
        return $this->create($data);
    }
    public function accessGet(){
        return $this->get();
    }
    public function accessUpdate($data){
        return $this->update($data);
    }
}


