<?php
namespace App\Services;



use App\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\UsersDevices;
use Illuminate\Support\Facades\DB;
use App\Model\Profiles\PassengersProfile;
use App\Model\Profiles\DriverProfiles;
use App\Services\TwilioSMS;
use App\Services\UserService;

use App\Services\TokenServices;

class AuthService 
{
    private function login($data){
        try{
            if($data->login_by=="manual"){
                if(!Auth::attempt(['username' => $data->username, 'password' => $data->password,'user_scope'=>$data->user_scope])){
                    return ['message'=>"The email address or password you entered is incorrect","data"=>(object)[],"errors"=>array("exception"=>["Invalid credentials"],"error"=>(object)[]),"statusCode"=>401];
                }
            }
            else if($data->login_by=="facebook" || $data->login_by=="google"){
                $user=User::where("social_unique_id",$data->social_unique_id)->where("login_type",$data->login_by)->where("user_scope",$data->user_scope)->firstOrFail();
                if(!Auth::loginUsingId($user->_id)){
                    return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Invalid credentials"],"error"=>(object)[]),"statusCode"=>401];
                }
            }
            else{
                return ['message'=>"Incorrect method of authentication. ","data"=>(object)[],"errors"=>array("exception"=>["Invalid credentials"],"error"=>(object)[]),"statusCode"=>401];
            }
            $access_token=Auth::user()->createToken('PersonalAccessToken',[$data->user_scope])->accessToken;
            return ['message'=>'Login successfully','data'=>(object)array(
                "access_token"=>$access_token,
                "token_type"=>'Bearer',
                "user_id"=>Auth::user()->id
            ),"errors"=>array("exception"=>["false"],"error"=>(object)[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>401];
        }
    }
    private function logout($data){
        try{
            $userID=Auth::user()->id;
            DB::table('oauth_access_tokens')->where('scopes', 'LIKE', '%'.$data->user_scope.'%')->where("user_id",$userID)->delete();
            $UserDevice=new UsersDevices();
            $DevicesData=$UserDevice->accessUpdateDevices((object)[
            'device_latitude'=>null,
            'device_longitude'=>null,
            'device_type' => null,
            'device_token' => '',
            'device_id' => '',
            'user_id' => $userID]);
          
            return ['message'=>"You have logout successfully.","data"=>(object)[],"errors"=>array("exception"=>["false"],"error"=>(object)[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    private function autoLogin2($data){
        try{
            $user=User::where("_id",$data->user_id)->firstOrFail();
            $TokenServices=new TokenServices();
            $access_token= $TokenServices->saveToken($data);
            return ['message'=>'Login successfully','data'=>(object)array(
                "access_token"=>$access_token,
                "token_type"=>'Bearer',
            ),"errors"=>array("exception"=>["false"],"error"=>(object)[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
           // return ['message'=>"User not found","data"=>(object)[],"errors"=>array("exception"=>["Invalid credentials"],"error"=>$e,"statusCode"=>401];
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>401];
        }
    }
    private function autoLogin($data){
        try{
          //  print_r($data); exit;
            if(!Auth::loginUsingId($data->user_id)){
                return ['message'=>"User not found","data"=>(object)[],"errors"=>array("exception"=>["Invalid credentials"],"error"=>(object)[]),"statusCode"=>401];
            }
            $access_token=Auth::user()->createToken('PersonalAccessToken',[$data->user_scope])->accessToken;
            return ['message'=>'Login successfully','data'=>(object)array(
                "access_token"=>$access_token,
                "token_type"=>'Bearer',
            ),"errors"=>array("exception"=>["false"],"error"=>(object)[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>401];
        }
    }


    public function reLogin($data){
        try{
          //  print_r($data); exit;
           
            $access_token=Auth::user()->createToken('PersonalAccessToken',[$data->user_scope])->accessToken;
            return ['message'=>'Login successfully','data'=>(object)array(
                "access_token"=>$access_token,
                "token_type"=>'Bearer',
            ),"errors"=>array("exception"=>["false"],"error"=>(object)[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>401];
        }
    }
    private function logoutTempUser($data){
        try{
            $userID=Auth::user()->id;
            DB::table('oauth_access_tokens')->where('scopes', 'LIKE', '%'.$data->user_scope.'%')->where("user_id",$userID)->delete();
            
            return ['message'=>"You have logout successfully.","data"=>(object)[],"errors"=>array("exception"=>["false"],"error"=>(object)[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
   
   
    private function forgetPassword($data){
        try{
            $PassengersProfile=PassengersProfile::where("email_id",$data->email_id)->firstOrFail();
            $User=User::where("_id",$PassengersProfile->user_id)->firstOrFail();
            if($User->login_type!=="manual"){
                return ['message'=>"you have registered using social platform. Reset your password from the respective social platform.","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>422];
            }
            if($PassengersProfile->mobile_no==null || $PassengersProfile->mobile_no=="")
            {
                return ['message'=>"No mobile number found","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>400];
            }
            //send otp.
            $dateOfGen=date('Y-m-d H:i:s');
            $digits_otp = 4;
            $otp= rand(pow(10, $digits_otp-1), pow(10, $digits_otp)-1);
            $UserService=new UserService();
            $UserService->accessResetPasswordOTP((object)["user_id"=>$PassengersProfile->user_id,"otpDate"=>$dateOfGen,"otp"=>$otp]);
            $data->body="Your one time password to reset your account password is ".$otp;
            $data->otp=$otp;
            $data->isdCode=$PassengersProfile->isd_code;
            $data->mobile_no=$PassengersProfile->mobile_no;  
            $TwilioSMS=new TwilioSMS();
            $TwilioSMSReturn=$TwilioSMS->accessSendSMS($data);
            return ['message'=>$TwilioSMSReturn['message'],"data"=>(object)["otp"=>$otp],"errors"=>$TwilioSMSReturn['errors'],"statusCode"=>$TwilioSMSReturn['statusCode']];
        
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    private function resetPassword($data){
        try{
            $PassengersProfile=PassengersProfile::where("email_id",$data->email_id)->firstOrFail();
            $UserService=new UserService();
            $UserData=$UserService->accessUpdatePassword((object)["user_id"=>$PassengersProfile->user_id,"password"=>$data->password]);
            return ['message'=>$UserData['message'],"data"=>$UserData['data'],"errors"=>$UserData['errors'],"statusCode"=>$UserData['statusCode']];
        
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    private function forgetPasswordDriver($data){
        try{
            $DriverProfiles=DriverProfiles::where("email_id",$data->email_id)->firstOrFail();
            if($DriverProfiles->mobile_no==null || $DriverProfiles->mobile_no=="")
            {
                return ['message'=>"No mobile number found","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>400];
            }
            //send otp.
            $dateOfGen=date('Y-m-d H:i:s');
            $digits_otp = 4;
            $otp= rand(pow(10, $digits_otp-1), pow(10, $digits_otp)-1);
            $UserService=new UserService();
            $UserService->accessResetPasswordOTP((object)["user_id"=>$DriverProfiles->user_id,"otpDate"=>$dateOfGen,"otp"=>$otp]);
            $data->body="Your one time password to reset your account password is ".$otp;
            $data->otp=$otp;
            $data->isdCode=$DriverProfiles->isd_code;
            $data->mobile_no=$DriverProfiles->mobile_no;  
            $TwilioSMS=new TwilioSMS();
            $TwilioSMSReturn=$TwilioSMS->accessSendSMS($data);
            return ['message'=>$TwilioSMSReturn['message'],"data"=>(object)["otp"=>$otp],"errors"=>$TwilioSMSReturn['errors'],"statusCode"=>$TwilioSMSReturn['statusCode']];
        
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    private function resetPasswordDriver($data){
        try{
            $DriverProfiles=DriverProfiles::where("email_id",$data->email_id)->firstOrFail();
            $UserService=new UserService();
            $UserData=$UserService->accessUpdatePassword((object)["user_id"=>$DriverProfiles->user_id,"password"=>$data->password]);
            return ['message'=>$UserData['message'],"data"=>$UserData['data'],"errors"=>$UserData['errors'],"statusCode"=>$UserData['statusCode']];
        
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }





    public function accessAutoLogin($data){
        return $this->autoLogin($data);
    }
    public function accessLogin($data){
        return $this->login($data);
    }
    public function accessLogout($data){
        return $this->logout($data);
    }
    public function accessForgetPassword($data){
        return $this->forgetPassword($data);
    }
    public function accessforgetPasswordDriver($data){
        return $this->forgetPasswordDriver($data);
    }

    public function accessresetPasswordDriver($data){
        return $this->resetPasswordDriver($data);
    }
    
    public function accessResetPassword($data){
        return $this->resetPassword($data);
    }
    public function accesslogoutTempUser($data){
        return $this->logoutTempUser($data);
    }
    
}