<?php

namespace App\Services;

use App\Model\DriverPreference\Preference;
use Storage;

class DriverPreferencesService
{
    private $DriverPreferences;
    private function checkNull($field){
        if($field===null)
        return "";
        else
        return $field;
    }
    private function updatePreferences($data){
        try{

            $DriverPreferences=Preference::where("user_id",$data->user_id)->where("preference_id",$data->preference_id)->first();
            if(empty($DriverPreferences)){
                $DriverPreferencesNew = new Preference();
                $DriverPreferencesNew->user_id=$data->user_id;
                $DriverPreferencesNew->preference_id=$data->preference_id;
                $DriverPreferencesNew->checked=$data->checked;
                $DriverPreferencesNew->save();
            }
            else{
                $DriverPreferences->checked=$data->checked;
                $DriverPreferences->save();
            }
            return ['message'=>"Your preferences is successfully updated","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>200];

        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"Your profile cannot be updated!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    private function getPreferences($data){
        try{
            $DriverPreferences=Preference::where("user_id",$data->user_id)->get();
            return ['message'=>"Profile Data","data"=>$DriverPreferences,"errors"=>array("exception"=>["Everything OK"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    public function accessCreatePreferences($data){

        $data->wheelChair='No';
        $data->petFriendlyRide='No';
        $data->babySeat ='No';
        $data->babyOnboardNotification='No';
        $data->engSpeakingDriver='No';
        $data->russianSpeakingDriver='No';
        $data->frenchSpeakingDriver='No';
        $data->spanishSpeakingDriver='No';
        $data->childSafatyFeature='No';
        $data->femaleFriendly=$data->femaleFriendly;


        return $this->createPreferences($data);
    }

    public function accessUpdatePreferences($data){

        return $this->updatePreferences($data);

    }
    public function accessGetPreferences($data){

        return $this->getPreferences($data);

    }
    public function setProfileData($data,$login_by){
        if($login_by=="manual"){

            if($this->checkNull($data->picture)!==""){

                $data->picture=$data->picture;

            }
        }
        return [
            "first_name"=>$data->first_name,
            "last_name"=>$data->last_name,
            "email_id"=>$data->email_id ,
            "mobile"=>$this->checkNull($data->mobile_no),
            "isdCode"=>$this->checkNull($data->isd_code),
            "picture"=>$this->checkNull($data->picture),
            "isMobileVerified"=>$data->isMobileverified,
            "gender"=>$this->checkNull($data->gender),
            "dob"=>$this->checkNull($data->dob),
            "login_by"=>$login_by,
            "active_service"=>$data->active_service,
            "status"=>$data->status,
            "status"=>$data->service_status
           // "scope"=>$user_scope
        ];
    }






}
