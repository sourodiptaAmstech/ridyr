<?php

namespace App\Services;

use App\Model\Auth\TokenStorages;


class TokenServices
{
    private $token;
    private $tokenIdentifier;

    private function tokenGen($length_of_string){
        $str_result = 'Dxq0T8rhfjc1133c30f740130cf2731ef98f8cf1514c5aef45a7b30e063c2f7471702e68d0rP2m-FW6RQQcuBntq3jk0M9Ivy1UcdfZf1CjHp-5Uh2ezK6bYtxpi7GDTKrCPvOn3TzskEuwyh95gNxWFS4oAaDwqsARXLJE880mafb905614781d1068b1fcdb63ed93af30123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-';
        return substr(str_shuffle($str_result),0, $length_of_string);
    }
    private function createTokens(){
        $this->token=$this->tokenGen(rand(99,999)).md5(uniqid($this->tokenGen(rand(99,999)), true)).md5(uniqid($this->tokenGen(rand(99,999)), true)).$this->tokenGen(rand(99,999)).md5(uniqid($this->tokenGen(rand(99,999)), true));
        return true;
    }
    private function createIdentifer(){
        $this->tokenIdentifier=md5(uniqid($this->token, true));
        return true;
    }


    public function checkTokens(){

    }
    public function saveToken($data){
        $this->createTokens();
        $this->createIdentifer();


        $TokenStorages =new TokenStorages();
        $TokenStorages->scope=$data->user_scope;
        $TokenStorages->tokenIdentifier=$this->tokenIdentifier;
        $TokenStorages->user_id=$data->user_id;
        $TokenStorages->revoke=0;
        $TokenStorages->name="AccessToken";
        $TokenStorages->save();
        return $this->token;

    }
    public function deleteTokensFromDB(){

    }

  
}


