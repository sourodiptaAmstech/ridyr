<?php
namespace App\Services;



use App\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Illuminate\Support\Facades\DB;
use App\Model\Profiles\PassengersProfile;
use App\Model\Profiles\DriverProfiles;
use App\Model\Request\ServiceRequest;
use App\Services\TwilioSMS;


class ServiceRequestService
{
      // Accessing the services
      private function createRequest($data){
          $ServiceRequest=new ServiceRequest();
          $ServiceRequest->request_no=$data->request_no;
          $ServiceRequest->passenger_id=$data->passenger_id;
          $ServiceRequest->driver_id=$data->driver_id;
          $ServiceRequest->request_type=$data->request_type;
          $ServiceRequest->request_status=$data->request_status;
          $ServiceRequest->locationDetails=$data->locationDetails;
          $ServiceRequest->service_preferences=$data->preferences;
          $ServiceRequest->request_service_type=null;

          $ServiceRequest->save();
          return $ServiceRequest;

      }
      private function updateRequestNo($data){
        $ServiceRequest=ServiceRequest::where("_id",$data->request_id)->first();
        $ServiceRequest->request_no=$data->request_no;
        $ServiceRequest->save();
        return $ServiceRequest;
    }
    private function getRequestByID($data){
        $ServiceRequest=ServiceRequest::where("_id",$data->request_id)->first();
        return $ServiceRequest;
    }

    private function updateRideStatus($data){
        $ServiceRequest=ServiceRequest::where("_id",$data->request_id)->first();
        $ServiceRequest->request_status=$data->request_status;
        $ServiceRequest->request_service_type=$data->service_type_id;
        $ServiceRequest->save();
        return $ServiceRequest;
    }

    private function getRiderActiveRequests($data){
        $ServiceRequest=ServiceRequest::where("passenger_id",$data->user_id)
        ->where("request_status","<>","serviceSearch")
        ->where("request_status","<>","completed")
        ->where("request_status","<>","noServiceFound")
        ->get()->toArray();
        return $ServiceRequest;
    }

    private function updateEstimatedFare($data){
        $ServiceRequest=ServiceRequest::where("_id",$data->request_id)->first();
        $ServiceRequest->estimatedFare=$data->estimatedFare;
        $ServiceRequest->save();
        return $ServiceRequest;
    }
    private function updateDriver($data){
        $ServiceRequest=ServiceRequest::where("_id",$data->request_id)->first();
        $ServiceRequest->request_status=$data->request_status;
        $ServiceRequest->driver_id=$data->driver_id;
        if($data->request_status==="ACCEPTED"){
            $ServiceRequest->accepted_on=date("Y-m-d H:i:s");
        }
        else if($data->request_status=="STARTED"){
            $ServiceRequest->started_from_source=date("Y-m-d H:i:s");
        }
        else if($data->request_status=="DROP"){
            $ServiceRequest->dropped_on_destination=date("Y-m-d H:i:s");
        }
        else if($data->request_status=="PAYMENT"){
            if($data->payment_method=="CASH"){
                   $ServiceRequest->payment_status=$data->payment_status;
            }
        }
        else if($data->request_status=="RATING"){
            $ServiceRequest->rating_by_driver=$data->rating_by_driver;
            $ServiceRequest->comment_by_driver=$data->comment_by_driver;
            $ServiceRequest->driver_rating_status=$data->driver_rating_status;
        }
        $ServiceRequest->save();
        if($data->request_status=="RATING"){
            $this->calculateOveralRatingPassenger($ServiceRequest);
        }
        return $ServiceRequest;
    }
    private function calculateOveralRatingPassenger($data){
        $sql='SELECT ROUND(avg(rating_by_driver),2) as overall_rating FROM service_requests where passenger_id="'.$data->passenger_id.'" and driver_rating_status="GIVEN"';
        $overAllRating=  DB::select($sql);
        $DriverProfiles=PassengersProfile::where("user_id",$data->passenger_id)->first();
        $DriverProfiles->overall_rating=$overAllRating[0]->overall_rating;
        $DriverProfiles->save();
        return true;
    }

    public function accessCreateRequest($data){
        return $this->createRequest($data);
    }
    public function accessUpdateRequestNo($data){
        return $this->updateRequestNo($data);
    }

    public function accessGetRequestByID($data){
        return $this->getRequestByID($data);
    }

    public function accessUpdateRideStatus($data){
        return $this->updateRideStatus($data);
    }

    public function accessGetRiderActiveRequests($data){
        return $this->getRiderActiveRequests($data);
    }

    public function accessUpdateEstimatedFare($data){
        return $this->updateEstimatedFare($data);
    }
    public function accessUpdateDriver($data){
        return $this->updateDriver($data);
    }
}
