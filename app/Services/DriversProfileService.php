<?php

namespace App\Services;

use App\Model\Profiles\DriverProfiles;
use App\Services\TwilioSMS;
use Storage;

class DriversProfileService 
{
    private $Profile;
    private function checkNull($field){
        if($field===null)
        return "";
        else
        return $field;
    }
    private function createProfile($data){
        $Profile = new DriverProfiles();
        $Profile->user_id=$data->user_id;
        $Profile->email_id=$data->email_id;
        $Profile->first_name=$data->first_name;
        $Profile->last_name=$data->last_name;
        $Profile->isMobileverified=0;
        $Profile->picture=$data->picture;
        $Profile->gender=$data->gender;
        $Profile->active_service=$data->active_service;
        $Profile->status=$data->status;
        $Profile->service_status=$data->service_status;
        $Profile->active_service_type=$data->active_service_type;
        $Profile->latitude=(float)$data->device_latitude;
        $Profile->longitude=(float)$data->device_longitude;
        $Profile->location=(object)["type"=>"Point","coordinates"=>[(float)$data->device_longitude,(float)$data->device_latitude]];
        
        
        
        /*
        $Profile->isd_code=$data->isd_code;
        $Profile->mobile_no=$data->mobile_no;
        $Profile->dob=$data->dob;
        
       
        $Profile->address_line1=$data->address_line1;
        $Profile->address_line2=$data->address_line2;
        $Profile->address_line3=$data->address_line3;
        $Profile->state=$data->state;
        $Profile->city=$data->city;
        $Profile->pincode=$data->pincode;
        $Profile->country=$data->country;
        $Profile->mobile_verification_code=$data->mobile_verification_code;
        $Profile->isMobileverified=$data->isMobileverified;
        $Profile->verification_code_generated_on=$data->verification_code_generated_on;
        */
        $Profile->save();
        return $Profile;
    } 
    private function updateProfile($data){
        try{
            $Profile=DriverProfiles::where("user_id",$data->user_id)->firstOrFail();
           // $Profile->email_id=$data->email_id;
            $Profile->first_name=$data->first_name;
            $Profile->last_name=$data->last_name;
            $Profile->gender=$data->gender;
            $Profile->save();
            return ['message'=>"Your profile is successfully updated","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>200];
        
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"Your profile cannot be updated!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    private function updateLocation($data){
        try{
            $Profile=DriverProfiles::where("user_id",$data->user_id)->firstOrFail();
            $Profile->latitude=(float)$data->device_latitude;
            $Profile->longitude=(float)$data->device_longitude;
            $Profile->location=(object)["type"=>"Point","coordinates"=>[(float)$data->device_longitude,(float)$data->device_latitude]];
            $Profile->save();
            return ['message'=>"Your profile is successfully updated","data"=>$Profile,"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>200];
        
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"Your profile cannot be updated!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    private function updateOnlyMobile($data){
        try{
            // generate otp
            $dateOfGen=date('Y-m-d H:i:s');
            $digits_otp = 4;
            $otp= rand(pow(10, $digits_otp-1), pow(10, $digits_otp)-1);
            $Profile=DriverProfiles::where("user_id",$data->user_id)->update(['isd_code'=>$data->isdCode,'mobile_no'=>$data->mobile_no,'mobile_verification_code'=>$otp,'verification_code_generated_on'=>$dateOfGen]);
            $data->body="Your one time password for your mobile number verification is ".$otp;
            $data->otp=$otp;
            $TwilioSMS=new TwilioSMS();
            $TwilioSMSReturn=$TwilioSMS->accessSendSMS($data);
            return ['message'=>$TwilioSMSReturn['message'],"data"=>(object)["otp"=>$otp],"errors"=>$TwilioSMSReturn['errors'],"statusCode"=>$TwilioSMSReturn['statusCode']];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    private function verifyMobileNo($data){
        try{
            $Profile=DriverProfiles::where("user_id",$data->user_id)->update(['isMobileverified'=>$data->isValid,'mobile_verification_code'=>'']);
            if($data->isValid==1)
            return ['message'=>"Thank you for verifying your mobile number","data"=>(object)[],"errors"=>array("exception"=>["Everything OK"],"error"=>[]),"statusCode"=>200];
            else
            return ['message'=>"Sorry cannot verify your mobile number","data"=>(object)[],"errors"=>array("exception"=>["Everything OK"],"error"=>[]),"statusCode"=>200];


        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }


    }
    private function getProfile($data){
        try{
            $Profile=DriverProfiles::where("user_id",$data->user_id)->first();
           
            return ['message'=>"Profile Data","data"=>$Profile,"errors"=>array("exception"=>["Everything OK"],"error"=>[]),"statusCode"=>200];
            
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    private function profileImageUpdate($data){
        try{
            $Profile = DriverProfiles::where("user_id",$data->user_id)->firstOrFail();
            if ($data->picture != "") {
                $Storage=Storage::delete($Profile->picture);
                $Profile->picture = $data->picture->store('public/users/'.$data->user_id.'/profile');
                $Profile->picture=str_replace("public", "storage", $Profile->picture);
                $Profile->picture="http://demos.mydevfactory.com/debarati/ridyr/public/".$Profile->picture;
            }
            $Profile->save();
           
            return ['message'=>"Thank you for uploading your profile image!","data"=>(object)["picture"=>$Profile->picture],"errors"=>array("exception"=>["Resoures Created"],"error"=>[]),"statusCode"=>201];
            
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    private function onOff($data){
        try{
            $Profile = DriverProfiles::where("user_id",$data->user_id)->firstOrFail();
            $Profile->status=$data->status;
            $Profile->save();
            return ['message'=>"OK","data"=>(object)[],"errors"=>array("exception"=>["Resoures Created"],"error"=>[]),"statusCode"=>201];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    private function setRequestUpdateToDriver($data,$service_status="active"){
        try{
            
            $Profile = DriverProfiles::where("user_id",$data->driver_id)->where("status","online")->where("service_status",$service_status)->first(); 
            if(!empty($Profile)){
                $Profile->service_status=$data->driver_service_status;
                $Profile->save();
                return ['message'=>"OK","data"=>(object)[],"errors"=>array("exception"=>["Resoures Created"],"error"=>[]),"statusCode"=>200];
            }
            else{
                return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Driver not found"],"error"=>[]),"statusCode"=>404];
            }
          
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    





    public function accessCreateProfile($data){
        return $this->createProfile($data);
    }
    public function accessUpdateProfile($data){
        return $this->updateProfile($data);
    }
    public function accessUpdateOnlyMobile($data){
        return $this->updateOnlyMobile($data);
    }
    public function accessVerifyMobileNo($data){
        return $this->verifyMobileNo($data);
    }
    public function accessGetProfile($data){
        return $this->getProfile($data);
    }

    public function setProfileData($data,$login_by){
        if($login_by=="manual"){
            if($this->checkNull($data->picture)!==""){
                $data->picture=$data->picture;
            }
        }
        return [
            "first_name"=>$data->first_name,
            "last_name"=>$data->last_name,
            "email_id"=>$data->email_id ,
            "mobile"=>$this->checkNull($data->mobile_no),
            "isdCode"=>$this->checkNull($data->isd_code),
            "picture"=>$this->checkNull($data->picture),
            "isMobileVerified"=>$data->isMobileverified,
            "gender"=>$this->checkNull($data->gender),
            "dob"=>$this->checkNull($data->dob),
            "login_by"=>$login_by,
            "active_service"=>$data->active_service,
            "status"=>$data->status,
            "service_status"=>$data->service_status
           // "scope"=>$user_scope
        ];
    }
    public function accessProfileImageUpdate($data){
        return $this->profileImageUpdate($data);
    }
    public function accessUpdateLocation($data){
        return $this->updateLocation($data);
    }

    public function accessOnOff($data){
        return $this->onOff($data);
    }

    public function accessSetRequestToDriver($data,$service_status="active"){
        return $this->setRequestUpdateToDriver($data,$service_status);
    }
    
}
