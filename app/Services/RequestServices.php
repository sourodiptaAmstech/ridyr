<?php

namespace App\Services;

use App\Model\Request\ServiceRequest;
use App\Model\Request\ServiceRequestLog;
use Illuminate\Support\Facades\DB;
use Storage;

class RequestServices
{
    private function insertIntoServiceRequestLog($data){
        $ServiceRequestLog=new ServiceRequestLog();
        $ServiceRequestLog->request_id=$data->request_id;
        $ServiceRequestLog->passenger_id=$data->passenger_id;
        $ServiceRequestLog->driver_id=$data->driver_id;
        $ServiceRequestLog->service_type_id=$data->service_type_id;
        $ServiceRequestLog->request_type=$data->request_type;
        $ServiceRequestLog->status="requested";
        $ServiceRequestLog->save();
        return $ServiceRequestLog;
    }
    // find the driver
    private function getDriverWithInRadius($radius,$data,$serviceLog){
        try{

            $source=$data->source;
            if(empty($serviceLog)){
                $driversServies = DB::collection("drivers_profile")
                ->where("status","online")
                ->where("service_status","active")
                ->where("active_service_type",$data->service_type_id)
                ->where('location', 'near', [
                    '$geometry' => [
                        'type' => 'Point',
                        'coordinates' => [
                            (float)$source['longitude'], // longitude
                            (float)$source['latitude'], // latitude
                        ],
                    ],'$maxDistance' => $radius*1000,
                    ])->first();
            }
            else{
                $notIn=[];
                foreach($serviceLog as $key=>$val){
                    $notIn[]=$val["driver_id"];
                }
                $driversServies = DB::collection("drivers_profile")
                ->where("status","online")
                ->where("service_status","active")
                ->where("active_service_type","$data->service_type_id")
                ->whereNotIn('user_id',$notIn)
                ->where('location', 'near', [
                    '$geometry' => [
                        'type' => 'Point',
                        'coordinates' => [
                            (float)$source['longitude'], // longitude
                            (float)$source['latitude'], // latitude
                        ],
                    ],'$maxDistance' => $radius*1000,
                    ])->first();

            }
            if(empty($driversServies)){
                return ['message'=>"No cars are available for your location at that given moment.","data"=>(object)[],"errors"=>array("exception"=>["Service not found"],"error"=>[]),"statusCode"=>404];
            }
            else{
                // get services details
                $serviceTypesDetails=[];
                $servics=DB::collection("driver_service_type")->where("_id",$driversServies['active_service'])->get()->toArray();
                if(!empty($servics)){
                    $serviceTypesDetails=$servics[0];
                }

            }
           // print_r((array)$serviceTypesDetails[0]["_id"]); exit;

            if(!empty($serviceTypesDetails)){
                return ['message'=>"Cars are available for your location at that given moment.","data"=>["service_details"=>$serviceTypesDetails,"driver_details"=>$driversServies],"errors"=>array("exception"=>["Service found"],"error"=>[]),"statusCode"=>200];
            }
            return ['message'=>"No cars are available for your location at that given moment.","data"=>(object)[],"errors"=>array("exception"=>["Service not found"],"error"=>[]),"statusCode"=>404];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }



    private function getSerReqLog($data){
        $ServiceRequestLog=ServiceRequestLog::where("request_id",$data->request_id)->where("service_type_id",$data->service_type_id)->get()->toArray();
        return $ServiceRequestLog;
    }

    private function getSerReqLogBYDriver($data){
        $ServiceRequestLog=ServiceRequestLog::where("status","requested")->where("driver_id",$data->driver_id)->where("service_type_id",$data->service_type_id)->get()->toArray();
        return $ServiceRequestLog;
    }

    private function updateStatus($data){
        $ServiceRequestLog=ServiceRequestLog::where("request_id",$data->request_id)->where("driver_id",$data->driver_id)->first();
        $ServiceRequestLog->status=$data->status;
        return $ServiceRequestLog->save();
    }



    public function accessInsertSerReqLog($data){
        return $this->insertIntoServiceRequestLog($data);
    }

    public function accessGetSerReqLog($data){
        return $this->getSerReqLog($data);
    }

    public function accessGetDriverWithInRadius($data,$serviceLog){
        return $this->getDriverWithInRadius(8,$data,$serviceLog);
    }

    public function accessGetSerReqLogBYDriver($data){
        return $this->getSerReqLogBYDriver($data);

    }
    public function accessUpdateStatus($data){
        return $this->updateStatus($data);
    }



}
