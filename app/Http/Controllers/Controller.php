<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Validator;
use App\Model\Profiles\AdminsProfile;
use Auth;
use App\Model\Setting\Setting;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected function requestValidation($request,$rule){
        $validator =  Validator::make($request,$rule);
        if ($validator->fails()) {
            $array=$validator->getMessageBag()->toArray();
            foreach($array as $key=>$val){
                return (object)['message'=>$val[0],"field"=>$key,"status"=>"false"];
            }
        }
        return (object)['message'=>null,"field"=>null,"status"=>"true"];;

    }
    protected function checkNull($field){
        if($field===null)
        return "";
        else
        return $field;
    }

    public static function adminProfile()
    {
        $admin_profile = AdminsProfile::where('user_id', Auth::user()->id)->first();
        return $admin_profile;
    }

    public static function setting()
    {
        $site_settings = Setting::where('key', 'like', "%site%")->get();
        return $site_settings;
    }
}
