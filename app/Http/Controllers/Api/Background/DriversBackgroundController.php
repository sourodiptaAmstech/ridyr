<?php
namespace App\Http\Controllers\Api\Background;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Document\DriverDocuments;
use App\Model\Document\Document;
use App\Services\DriversProfileService;
use App\Services\UsersDevices;
use App\Services\PassengersProfileService;
use App\Services\ServiceRequestService;
use App\Services\UnitConvertionService;
use App\Services\RequestServices;
use App\Services\EstimatedFareService;
use App\Model\Device\UserDevices;


use Validator;








class DriversBackgroundController extends Controller
{
    public function DocumentCompletion(){
        try{
            $total=0;
            $profileFillUp=0;

            $user_id=Auth::user()->id;
            $DriverDocument=DriverDocuments::where("user_id",$user_id)->get()->toArray();
            $profileFillUp+=count($DriverDocument);

            $Document=Document::where("deleted_at",null)->where("status",1)->get()->toArray();
            $total+=count($Document);
            $percentage=($profileFillUp*100)/$total;

            return $percentage;
        }
        catch(\Illuminate\Database\QueryException  $e){
            return 0;
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return 0;
        }
        catch(ModelNotFoundException $e)
        {
            return 0;
        }
    }

    private function requestedRide($profileData){
        // check the service request log and get the ride details
        $RequestServices=new RequestServices();
       // print_r($profileData);
        $RequestServicesReturn=$RequestServices->accessGetSerReqLogBYDriver((object)['driver_id'=>$profileData->user_id,"service_type_id"=>$profileData->active_service_type]);
        $requestData=[];
        if(!empty($RequestServicesReturn)){
            // get request details and passanger details
            $ServiceRequestService =new ServiceRequestService();
            $ServiceRequestServiceReturn = $ServiceRequestService->accessGetRequestByID((object)$RequestServicesReturn[0]);
            $PassengersProfileService=new PassengersProfileService();
            $PassengersProfileServiceReturn=$PassengersProfileService->accessGetProfile((object)['user_id'=>$RequestServicesReturn[0]['passenger_id']]);
            $UsersDevices= new UsersDevices();
            $UsersDevicesReturn=$UsersDevices->accessGetDevice((object)['user_id'=>$RequestServicesReturn[0]['passenger_id']]);

          //  print_r($PassengersProfileServiceReturn); exit;
            $requestData=[
                'passenger_firstName'=>$PassengersProfileServiceReturn['data']->first_name,
                "passenger_lastName"=>$PassengersProfileServiceReturn['data']->last_name,
                "passenger_img"=>$PassengersProfileServiceReturn['data']->picture,
                "passenger_mobile"=>$PassengersProfileServiceReturn['data']->mobile_no,
                "passenger_isdcode"=>$PassengersProfileServiceReturn['data']->isd_code,
                "passenger_location"=>["latitude"=>$UsersDevicesReturn->latitude,"longitude"=>$UsersDevicesReturn->longitude],
                "passenger_prefrence"=>"","travel_details"=>$ServiceRequestServiceReturn->locationDetails,
                "estimatedFare"=>$ServiceRequestServiceReturn->estimatedFare,
                "request_no"=>$ServiceRequestServiceReturn->request_no,
                "request_type"=>$ServiceRequestServiceReturn->request_type,];

        }
        return $requestData;
    }

    public function get(Request $request){
        try{

            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                "device_latitude"=>"required",
                "device_longitude"=>"required"
            ];
            $requestedRide=[];
            $status="normal";
            $message="";
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_scope="driver-service";
            $request->user_id=Auth::user()->id;
            $DriversProfileService=new DriversProfileService();
            $UsersDevices=new UsersDevices();

            //update lat log
            $driverDeatils= $DriversProfileService->accessUpdateLocation($request);
            $UsersDevices->accessUpdateLocation($request);

            ##### On off #######
            $onOffStatus= $driverDeatils['data']->status;

            ###### Check For request ########
        //    print_r($driverDeatils['data']);

            switch($driverDeatils['data']->service_status){
                case "active":
                    $status="normal";
                    $message="";
                break;
                case "requested":
                    $requestedRide=$this->requestedRide($driverDeatils['data']);
                    $status="request";
                    $message="You have new request.";
                break;

            }







            $DocumentCompletion=$this->DocumentCompletion();
            $Background=[
                "document_completion"=>$DocumentCompletion,
                "onOffStatus"=>$onOffStatus,
                "status"=>$status,
                "message"=>$message,
                "requestData"=>(object)$requestedRide
            ];
            return response(['message'=>"Driver Background","data"=>(object)$Background,"errors"=>array("exception"=>["OK"],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

}
