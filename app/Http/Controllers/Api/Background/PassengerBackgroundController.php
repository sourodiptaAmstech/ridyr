<?php
namespace App\Http\Controllers\Api\Background;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Document\DriverDocuments;
use App\Model\Document\Document;
use App\Services\DriversProfileService;
use App\Services\UsersDevices;
use App\Services\ServiceRequestService;

use App\Services\UnitConvertionService;
use App\Services\RequestServices;
use App\Services\EstimatedFareService;

use Validator;

class PassengerBackgroundController extends Controller
{
    function searchMultiArray($value, $array,$keys) {
        foreach ($array as $key => $val) {
            if ($val[$keys] === $value) {
                return $value;
            }
        }
        return null;
     }
    public function get(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                "device_latitude"=>"required",
                "device_longitude"=>"required"
            ];
            $status="normal";
            $messsage="";
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_scope="passenger-service";
            $request->user_id=Auth::user()->id;
            $UsersDevices=new UsersDevices();
            $ServiceRequestService=new ServiceRequestService();
            $requestServices=new RequestServices();
            //update lat log
            $UsersDevices->accessUpdateLocation($request);
            // get request details ;
            $requestDetails=$ServiceRequestService->accessGetRiderActiveRequests($request);
             //print_r($requestDetails); exit;
            foreach($requestDetails as $key=>$val){
                switch($val['request_status']){
                    case "rideSearch":
                        $isSearch= (strtotime(date("Y-m-d H:i:s"))<strtotime("+3 minutes", strtotime($val['updated_at'])))?"true":"false";
                        $data=[
                            'request_id'=>$val['_id'],
                            "service_type_id"=>$val['request_service_type'],
                            "source"=>$val['locationDetails']['source'],
                            "waypoint"=>$val['locationDetails']['waypoint'],
                            "destination"=>$val['locationDetails']['destination'],
                            "passenger_id"=>$request->user_id,
                            "request_type"=>$val["request_type"],
                            "driver_service_status"=>"requested"
                        ];
                        //get data from the service request log
                        $serviceLog=$requestServices->accessGetSerReqLog((object)$data);
                        if(empty($serviceLog)){
                            // check if still service can be serach
                            if($isSearch=="false"){
                                $status="noServiceFound";
                                $messsage="Sorry no driver is available at this moment. Please try after some time.";
                                // update the service request table's request_status with noServiceFound
                                $data['request_status']=$status;
                                $result=$ServiceRequestService->accessUpdateRideStatus((object)$data);
                            break;
                            }
                            /**
                             * system will first search the estimanted fare; as the estimated fare may change due to
                             * various factor like traffic, avalibility of the driver, as it posible that customer press the request button from the mobile app
                             * after 10 -15min or more;
                             */
                            /**** ESTIMATED FARE */
                            $EstimatedFareService =new EstimatedFareService();
                            $retunEsti=$EstimatedFareService->accessGetFareByServiceType((object)$data);
                            if($retunEsti["statusCode"]!=200){
                                 $status="noServiceFound";
                                 $messsage="Sorry no driver is available at this moment. Please try after some time.";
                                break;
                            }
                            /**** End */
                            /*** Driver Search   */

                            $driverDetails=$requestServices->accessGetDriverWithInRadius((object)$data,$serviceLog);

                            /*** Driver Not Found */

                            if($driverDetails['statusCode']!=200){

                                $status="noServiceFound";
                                $messsage="Sorry no driver is available at this moment. Please try after some time.";

                                // update the service request table's request_status with noServiceFound

                                $data['request_status']=$status;
                                $result=$ServiceRequestService->accessUpdateRideStatus((object)$data);
                                break;
                            }

                            /*** End Driver Not Found */

                            /**
                             * Driver found!! Ya Ya.. :)
                             * Now insert the driver and request details in the service request log
                             * and update the service request with the estimated fare and update the driver profile service status as requested
                             * Now send the response as RideSearch
                             *
                             */

                             $data["driver_id"]=$driverDetails["data"]['driver_details']['user_id'];
                             $DriversProfileService=new DriversProfileService();
                             $driverProfile=$DriversProfileService->accessSetRequestToDriver((object)$data);

                             if($driverProfile['statusCode']!=200){
                                 $status="rideSearch";
                                 $messsage="Connecting with near by drivers.";
                                break;
                                }

                            $reqSer=$requestServices->accessInsertSerReqLog((object)$data);
                            $data["estimatedFare"]=$retunEsti["data"][0];
                            $ServiceRequestService->accessUpdateEstimatedFare((object)$data);
                            $status="rideSearch";
                            $messsage="Connecting with near by drivers.";

                            ############### END OF DRIVER SEARCH FOR FIRST EVER DRIVE ############################
                        }
                        else{

                            if($this->searchMultiArray("requested", $serviceLog,"status")=="requested"){
                                $status="rideSearch";
                                $messsage="Connecting with near by drivers.";
                            }
                            else if($this->searchMultiArray("decline", $serviceLog,"status")=="decline"){

                                if($isSearch=="false"){

                                    $status="noServiceFound";
                                    $messsage="Sorry no driver is available at this moment. Please try after some time.";

                                    // update the service request table's request_status with noServiceFound

                                    $data['request_status']=$status;
                                    $result=$ServiceRequestService->accessUpdateRideStatus((object)$data);
                                    break;
                                }

                                /*** Driver Search   */
                                $driverDetails=$requestServices->accessGetDriverWithInRadius((object)$data,$serviceLog);

                                /*** Driver Not Found */
                                if($driverDetails['statusCode']!=200){
                                    $status="noServiceFound";
                                    $messsage="Sorry no driver is available at this moment. Please try after some time.";

                                    // update the service request table's request_status with noServiceFound

                                    $data['request_status']=$status;
                                    $result=$ServiceRequestService->accessUpdateRideStatus((object)$data);
                                    break;
                                }

                                /*** End Driver Not Found */

                                /**
                                 * * Driver found!! Ya Ya.. :)
                                 * * Now insert the driver and request details in the service request log
                                 * * and  update the driver profile service status as requested
                                 * * Now send the response as RideSearch
                                 * *
                                 * */

                                 $data["driver_id"]=$driverDetails["data"]['driver_details']['user_id'];
                                 $DriversProfileService=new DriversProfileService();

                                 $driverProfile=$DriversProfileService->accessSetRequestToDriver((object)$data);
                                 $status="rideSearch";
                                 $messsage="Connecting with near by drivers.";
                                 if($driverProfile['statusCode']!=200){
                                     break;
                                    }
                                    $reqSer=$requestServices->accessInsertSerReqLog((object)$data);
                                }
                            }
                        break;
                    }
            }

            $Background=[
                "status"=>$status,
                "messsage"=>$messsage,
                "rideDetails"=>[]
            ];
            return response(['message'=>"Driver Background","data"=>(object)$Background,"errors"=>array("exception"=>["OK"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
                return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
            }
            catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
                return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
            }
    }

}
