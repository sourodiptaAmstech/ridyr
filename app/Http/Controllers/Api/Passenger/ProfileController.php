<?php

namespace App\Http\Controllers\Api\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Services\PassengersProfileService;
use App\Services\EmergencyContact;
use App\Services\UserService;
use App\Services\UsersDevices;
use App\Services\AuthService;
use Illuminate\Support\Facades\Hash;
use Validator;

class ProfileController extends Controller
{
    private function addEmergencyNo($data){
        $emergencyNumber=[];
        $ruleOne=[
            'emergOne_contact_no' => 'required',
            'emergOne_name'=>'required',
            'emergOne_isdCode'=>'required'
        ];
        $ruleTwo=[
            'emergTwo_contact_no' => 'required',
            'emergTwo_name'=>'required',
            'emergTwo_isdCode'=>'required'
        ];
        $EmergencyContact=new EmergencyContact();
        $validator=$this->requestValidation($data->all(),$ruleOne);
        if($validator->status!=="false"){ 
            if($data->emergOne_id==0)
                $EmergencyContact->accessCreateContact((object)["contact_no"=>$data->emergOne_contact_no,"name"=>$data->emergOne_name,"user_id"=>$data->user_id,"isd_code"=>$data->emergOne_isdCode]);
            else
                $EmergencyContact->accessUpdateContact((object)["contact_no"=>$data->emergOne_contact_no,"name"=>$data->emergOne_name,"user_id"=>$data->user_id,"emergency_id"=>$data->emergOne_id,"isd_code"=>$data->emergOne_isdCode]);
        };
        $validator=$this->requestValidation($data->all(),$ruleTwo);
        if($validator->status!=="false"){
            if($data->emergTwo_id==0)
                $EmergencyContact->accessCreateContact((object)["contact_no"=>$data->emergTwo_contact_no,"name"=>$data->emergTwo_name,"user_id"=>$data->user_id,"isd_code"=>$data->emergTwo_isdCode]);
            else
                $EmergencyContact->accessUpdateContact((object)["contact_no"=>$data->emergTwo_contact_no,"name"=>$data->emergTwo_name,"user_id"=>$data->user_id,"emergency_id"=>$data->emergTwo_id,"isd_code"=>$data->emergTwo_isdCode]);

        }
        return true ;
    }
    
    public function updateMobileNo(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'mobile_no' => ['required',Rule::unique('passengers_profile')->where(function($query){
                    return $query->where("user_id","!=",Auth::user()->id);
                })],
                'timeZone'=>'required',
                'isdCode'=>'required'                
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_scope="passenger-service";
            $request->user_id=Auth::user()->id;
            
            $this->addEmergencyNo($request);

            $Profile=new PassengersProfileService();
            $ProfileRetun=$Profile->accessUpdateOnlyMobile($request);
            return response(['message'=>$ProfileRetun['message'],"data"=>$ProfileRetun['data'],"errors"=>$ProfileRetun['errors']],(int)$ProfileRetun['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }
    
    public function verifyMobileNo(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'isValid'=>'required'
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            
            $Profile=new PassengersProfileService();
            $EmergencyContact=new EmergencyContact();
            $UserAuth=new AuthService();

            $request->user_scope="passenger-service";
            $request->user_id=Auth::user()->id;
            $request->login_by=Auth::user()->login_type;
          
            $ProfileRetun=$Profile->accessVerifyMobileNo($request);
            
            // logout
           //logut all access user 
           $UserAuth->accesslogoutTempUser((object)['user_id'=>$request->user_id,"user_scope"=>"temporary-customer-service"]);

           // $UserAuth->accessLogoutTempUser($request,"Passenger-Temp");

          
            $ProfileData=$Profile->accessGetProfile($request);
            $EmergencyContactData=$EmergencyContact->accessGetContact($request);  
            
           
            // login 
            if(!empty($request)){
                if($request->user_id>0){
                    
                    $UserToken=$UserAuth->reLogin((object)['user_id'=>$request->user_id,"user_scope"=>$request->user_scope]);
                    if($UserToken['statusCode']==200){
                        $UserAccessToken=$UserToken['data']->access_token;
                        $token_type=$UserToken['data']->token_type;
                        return response(['message'=>$ProfileRetun['message'],"data"=>(object)["access_token"=>$UserAccessToken,"token_type"=>$token_type,"user_profile"=>$Profile->setProfileData($ProfileData['data'],$request->login_by),"emergency_contacts"=>$EmergencyContactData],"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],201);
                    }
                    else{
                        return response(['message'=>$UserToken['message'],"data"=>$UserToken['data'],"errors"=>$UserToken['errors']],$UserToken['statusCode']);
                    }
                }
            }
            return response(['message'=>$ProfileRetun['message'],"data"=>$ProfileRetun['data'],"errors"=>$ProfileRetun['errors']],(int)$ProfileRetun['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function resetMobileVerificationOtp(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_scope="passenger-service";
            $request->user_id=Auth::user()->id; 

            $Profile=new PassengersProfileService();
            $ProfileData=$Profile->accessGetProfile($request);
         //  print_r($ProfileData); exit;
            if($ProfileData['statusCode']==200){
                $request->mobile_no=$ProfileData['data']->mobile_no;
                $request->isdCode=$ProfileData['data']->isd_code;
            }
         //   print_r($ProfileData); exit; 
            $ProfileRetun=$Profile->accessUpdateOnlyMobile($request);
            
            return response(['message'=>$ProfileRetun['message'],"data"=>$ProfileRetun['data'],"errors"=>$ProfileRetun['errors']],(int)$ProfileRetun['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function getProfile(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_scope="passenger-service";
            $request->user_id=Auth::user()->id;
            $request->login_by=Auth::user()->login_type;
            $Profile=new PassengersProfileService();
            $EmergencyContact=new EmergencyContact();
            $ProfileData=$Profile->accessGetProfile($request);
            $EmergencyContactData=$EmergencyContact->accessGetContact($request); 
            return response(['message'=>"Profile Data","data"=>(object)["user_profile"=>$Profile->setProfileData($ProfileData['data'],$request->login_by),"emergency_contacts"=>$EmergencyContactData],"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],200);
        
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function profileImageUpdate(Request $request){
        try{
            $rule=['picture' => 'required|mimes:jpeg,bmp,png'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422); };
            $request->user_id=Auth::user()->id;
            $Profile=new PassengersProfileService();
            $ProfileData=$Profile->accessProfileImageUpdate($request);
            
            return response(['message'=>$ProfileData['message'],"data"=>$ProfileData['data'],"errors"=>$ProfileData['errors']],$ProfileData['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>"User Not Found","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"])],403);
        }
    }

    public function updateProfile(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            
            $rule=[
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'gender'=>'sometimes|nullable|in:Male,Female,Transgender,Others',
               /* 'email_id' =>  ['required',Rule::unique('passengers_profile')->where(function($query){
                    return $query->where("user_id","!=",Auth::user()->id);
                })],*/
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_scope="passenger-service";
            $request->user_id=Auth::user()->id;
            $request->login_by=Auth::user()->login_type;
            $this->addEmergencyNo($request);
            $Profile=new PassengersProfileService();
            $EmergencyContact=new EmergencyContact();
            $ProfileRetun=$Profile->accessUpdateProfile($request);          
            $ProfileData=$Profile->accessGetProfile($request);
            $EmergencyContactData=$EmergencyContact->accessGetContact($request);

            return response(['message'=>$ProfileRetun['message'],"data"=>(object)["user_profile"=>$Profile->setProfileData($ProfileData['data'],$request->login_by),"emergency_contacts"=>$EmergencyContactData],"errors"=>$ProfileRetun['errors']],(int)$ProfileRetun['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function changePassword(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'old_password' => 'required|between:6,255',
                'password' => 'required|between:6,255|confirmed'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            
            if (!(Hash::check($request->old_password, Auth::user()->password))) {
                return response(['message'=>'Your old password does not matches with the password you provided. Please try again',"field"=>'old_password',"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); 
	       	
	        }
	        if(strcmp($request->old_password, $request->password) == 0){
                return response(['message'=>'New Password cannot be same as your old password. Please choose a different password',"field"=>'password',"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); 
            }
            
            $request->user_id=Auth::user()->id;
            $request->user_scope="passenger-service"; 
            $UserService=new UserService();
            $UserServiceReturn=$UserService->accessUpdatePassword($request);
            return response(['message'=>$UserServiceReturn['message'],"data"=>$UserServiceReturn['data'],"errors"=>$UserServiceReturn['errors']],$UserServiceReturn['statusCode']);
            
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }


}