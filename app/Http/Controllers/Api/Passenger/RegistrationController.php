<?php

namespace App\Http\Controllers\Api\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\PassengersProfileService;
use App\Services\UsersDevices;
use App\Services\AuthService;
use App\Services\EmergencyContact;
use Validator;

class RegistrationController extends Controller
{
   // private function 

    public function register(Request $request){
        try{
               // 'mobile' => 'required|unique:users',
               $request['timeZone']=$timeZone=$request->header("timeZone");
               $rule=['social_unique_id' => ['required_if:login_by,facebook,google','unique:users'],
               'device_type' => 'required|in:android,ios',
               'device_token' => 'required',
               'device_id' => 'required',
               'login_by' => 'required|in:manual,facebook,google',
               'first_name' => 'required|max:255',
               'last_name' => 'required|max:255',
               'email_id' => 'required|email|max:255|unique:passengers_profile',              
               'password' => 'required_if:login_by,manual|between:6,255|confirmed',
               'timeZone'=>'required',
               'gender'=>'sometimes|nullable|in:Male,Female,Transgender,Others',
               "device_latitude"=>"sometimes",
               "device_longitude"=>"sometimes"
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_scope="passenger-service";
             
            $User=new UserService(); 
            $Profile=new PassengersProfileService();
            $UserDevice=new UsersDevices();
            $EmergencyContact=new EmergencyContact();
         

            $ProfileData=(object)[];
            $DevicesData=(object)[];
            $EmergencyContactData=[];
            $UserData=$User->accessCreateUser($request);   //echo 10; exit;

            //print_r($UserData->_id); exit;
            if($UserData->_id!==""){
                $request->user_id=$UserData->_id;
                $ProfileData=$Profile->accessCreateProfile($request);
                $DevicesData=$UserDevice->accessCreateDevices($request);
                $EmergencyContactData=$EmergencyContact->accessGetContact($request);         
            }
          
            if(!empty($UserData)){
                if($UserData->_id!==""){
                    //autologin to the system.
                    $UserAuth=new AuthService();
                    $UserToken=$UserAuth->accessAutoLogin((object)['user_id'=>$UserData->_id,"user_scope"=>"temporary-customer-service"]);
                    if($UserToken['statusCode']==200){
                        $UserAccessToken=$UserToken['data']->access_token;
                        $token_type=$UserToken['data']->token_type;
                        return response(['message'=>"Thank you for registering with us!","data"=>(object)["access_token"=>$UserAccessToken,"token_type"=>$token_type,"user_profile"=>$Profile->setProfileData($ProfileData,$request->login_by),"emergency_contacts"=>$EmergencyContactData],"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],201);
                    }
                    else{
                        return response(['message'=>$UserToken['message'],"data"=>$UserToken['data'],"errors"=>$UserToken['errors']],$UserToken['statusCode']);
                    }
                }
            }
            return response(['message'=>"Registration not possible. Contact Administrator.","data"=>(object)[],"errors"=>array("exception"=>["Not Found"],"e"=>[])],404);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
    
}
