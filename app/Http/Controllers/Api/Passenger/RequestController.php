<?php

namespace App\Http\Controllers\Api\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\EstimatedFareService;
use App\Services\ServiceRequestService;
use App\Services\PreferencesService;

use Validator;



class RequestController extends Controller
{
    public function estimated(Request $request){
        try{
            $rule=[
                'source' =>'required',
                'destination' => 'required',
                'waypoint' => 'sometimes',
                'preferences' => 'sometimes',
                'request_type'=>'required|in:RideNow,Schedule'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $request->passenger_id=Auth::user()->id;
            $request->driver_id=null;
            $request->request_status="serviceSearch";
            $request->locationDetails=["source"=>$request->source,"waypoint"=>$request->waypoint,"destination"=>$request->destination];
            $ServiceRequest=new ServiceRequestService();


            // find open request and on going trip

            // $ServiceRequest->request_no=$data->request_no;

           $ServiceRequestResult=$ServiceRequest->accessCreateRequest($request);

           $request->request_id=$ServiceRequestResult->_id;
           $request->request_no="RIDYR"."/".date("ymd/hms");

           $ServiceRequestResult=$ServiceRequest->accessUpdateRequestNo($request);

           $EstimatedFareService =new EstimatedFareService();
           $retunEsti=$EstimatedFareService->accessGetFare($request);

           // print_r($retunEsti['message']);

            //accessGetFare
            return response(['message'=>$retunEsti['message'],"data"=>$retunEsti['data'],"errors"=>$retunEsti['errors']],$retunEsti['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }

    public function requestRide(Request $request){
        try{
            $rule=[
                'request_id' =>'required',
                'service_type_id' => 'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $request->passenger_id=Auth::user()->id;
            $request->driver_id=null;
            $request->request_status="rideSearch";
            $ServiceRequest=new ServiceRequestService();
            // get the data from request table
            $getRequestData=$ServiceRequest->accessGetRequestByID($request);
           // print_r($getRequestData); exit;
            if($getRequestData->request_type=="RideNow"){
                switch ($getRequestData->request_status) {
                    case "serviceSearch":
                        $result=$ServiceRequest->accessUpdateRideStatus($request);
                        $request_status=$request->request_status;
                        $message="Thank you for choosing us, we are connecting with near by drivers.";
                    break;
                    case "rideSearch":
                        $request_status=$getRequestData->request_status;
                        $message="We are connecting with near by drivers, we appreciate your patience.";
                    break;
                    }
            }

            return response(['message'=>$message,"data"=>(object)["request_status"=>$request_status,"request_id"=>$request->request_id],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
    public function getPreferences(Request $request){
        try{
            $PreferencesService=new PreferencesService();
            $Preferences=$PreferencesService->accessGetPreferences();
            return response(['message'=>$Preferences['message'],"data"=>(object)$Preferences['data'],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
}
