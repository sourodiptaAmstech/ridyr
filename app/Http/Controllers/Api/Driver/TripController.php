<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Services\DriversProfileService;
use App\Services\EmergencyContact;
use App\Services\UserService;
use App\Services\UsersDevices;
use App\Services\AuthService;
use App\Services\DriversServiceType;
use App\Services\DriverPreferencesService;
use App\Services\ServiceRequestService;
use App\Services\UnitConvertionService;
use App\Services\RequestServices;
use App\Services\NotificationServices;

use Validator;
use Exception;

class TripController extends Controller
{
    public function reject(Request $request){
        try{

            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'request_id' => "required",
                'timeZone'=>'required',
            ];
          //  echo 1; exit;
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $request->user_id=Auth::user()->id;
            $DriversProfileService=new DriversProfileService();

            $DriversProfileServiceReturn=$DriversProfileService->accessSetRequestToDriver((object)["driver_service_status"=>"active","driver_id"=>$request->user_id],"requested");
            $RequestServices=new RequestServices();
            $RequestServicesReturn=$RequestServices->accessUpdateStatus((object)["request_id"=>$request->request_id,"status"=>"decline","driver_id"=>$request->user_id]);
            return response(['message'=>"You have decline, a request","data"=>(object)[],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
    public function accpect(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'request_id' => "request",
                'timeZone'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $DriversProfileService=new DriversProfileService();

            $DriversProfileServiceReturn=$DriversProfileService->accessSetRequestToDriver((object)["driver_service_status"=>"active","driver_id"=>$request->user_id],"requested");
            $RequestServices=new RequestServices();
            $RequestServicesReturn=$RequestServices->accessUpdateStatus((object)["request_id"=>$request->request_id,"status"=>"decline","driver_id"=>$request->user_id]);
            return response(['message'=>"You have decline, a request","data"=>(object)[],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
    public function tripControl(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'request_id' => "required",
                'timeZone'=>'required',
                'tripStatus'=>'required|in:ACCEPTED,REACHED,STARTED,DROP,PAYMENT,RATING,COMPLETED',
                 'payment_method'=>'required_if:tripStatus,PAYMENT|in:CARD,CASH',
                 'rating'=>'required_if:tripStatus,RATING',
                 'comment'=>'required_if:tripStatus,RATING'

            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $DriversProfileService=new DriversProfileService();
            $RequestServices=new RequestServices();
            $ServiceRequestService=new ServiceRequestService();
           // $LocationService = new LocationService();
            $NotificationServices =new NotificationServices();
            $message="";
            $status="";
            $nextStep="";
            switch($request->tripStatus){
                case "ACCEPTED":
                    //updating the driver profile
                    $DriversProfileServiceReturn=$DriversProfileService->accessSetRequestToDriver((object)["driver_service_status"=>"ONRIDE","driver_id"=>$request->user_id],"requested");
                    // updating the service request log
                    $RequestServicesReturn=$RequestServices->accessUpdateStatus((object)["request_id"=>$request->request_id,"status"=>"ONRIDE","driver_id"=>$request->user_id],"REQUESTED");
                    // updating the service request
                    $ServiceRequestServiceReturn= $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"ACCEPTED","driver_id"=>$request->user_id]);
                    $message="Thank you for accepting the request, please proceed towards the pickup location.";
                    $status="ACCEPTED";
                    $nextStep="REACHED";
                    // push
                      $pushDate=[
                        "title"=>"Ride Accepted",
                        "text"=>"Driver has been accepted your ride request. Soon be reached in your pickup location.",
                        "body"=>"Driver has been accepted your ride request. Soon be reached in your pickup location.",
                        "type"=>"RIDEACCEPTED",
                        "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        "setTo"=>'SINGLE',
                        "request_id"=>$ServiceRequestServiceReturn->request_id
                        ];
                        //$NotificationServices->sendPushNotification((object)$pushDate);
                        // send sms
                       // $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                        // email
                       // echo 1;

                break;
                case "REACHED":
                    // updating the service request
                    $ServiceRequestServiceReturn= $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"REACHED","driver_id"=>$request->user_id]);
                   // $LocationServiceReturn= $LocationService->accessUpdateLocationReachedTime((object)["request_id"=>$request->request_id,"types"=>"source"]);
                    $message="Please wait for the customer";
                    $status="REACHED";
                    $nextStep="STARTED";
                     // push
                     $pushDate=[
                        "title"=>"Ride Reached",
                        "text"=>"Driver has been arrived at your pickup location.",
                        "body"=>"Driver has been arrived at your pickup location.",
                        "type"=>"RIDEREACHED",
                        "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        "setTo"=>'SINGLE',
                        "request_id"=>$ServiceRequestServiceReturn->request_id
                        ];
                        $NotificationServices->sendPushNotification((object)$pushDate);
                        // send sms
                        $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                        // email
                break;
                case "STARTED":
                      //caculate waiting time;
                      $ServiceRequestServiceReturn= $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"STARTED","driver_id"=>$request->user_id]);
                      //$LocationServiceReturn= $LocationService->accessUpdateLocationStartedTime((object)["request_id"=>$request->request_id,"types"=>"source"]);
                      $message="Please proceed towards the destination.";
                      $status="STARTED";
                      $nextStep="DROP";
                break;
                case "DROP":
                    $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"DROP","driver_id"=>$request->user_id]);
                    //$LocationServiceReturn = $LocationService->accessUpdateLocationReachedTime((object)["request_id"=>$request->request_id,"types"=>"destination"]);
                    $ServiceRequestServiceReturn = $ServiceRequestService->accessGetRequestByID((object)['request_id'=>$request->request_id]);
                    //   print_r( $ServiceRequestServiceReturn ); exit;
                    if($ServiceRequestServiceReturn->payment_method=="CARD"){
                        $requestedRide=[
                            "request_no"=>$ServiceRequestServiceReturn->request_no,
                            "request_id"=>$ServiceRequestServiceReturn->request_id,
                            "request_type"=>$ServiceRequestServiceReturn->request_type,
                            "request_status"=>$ServiceRequestServiceReturn->request_status,
                            "payment_method"=>$ServiceRequestServiceReturn->payment_method,
                            "staredFromSource_on"=>$ServiceRequestServiceReturn->started_from_source,
                            "dropped_on_destination"=>$ServiceRequestServiceReturn->dropped_on_destination,
                            "driver_rating_status"=>$ServiceRequestServiceReturn->driver_rating_status,
                            "user_cards_id"=>$ServiceRequestServiceReturn->card_id,
                            "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        ];
                        $reqRq = $this->calculateFinalPayable($requestedRide);
                        if((float)($reqRq['cost'])-(float)($reqRq['promo_code_value'])>0){
                            $PaymentService = new PaymentService();
                            $retunPayment=$PaymentService->accesssMakeCardPayment((object)$reqRq,(object)$requestedRide);
                            //print_r($retunPayment); exit;
                            if($retunPayment['statusCode']==200){
                                // if payment is success update the transcation table
                                $TransactionLogService = new TransactionLogService();
                                $reqRq['payment_gateway_charge']=$retunPayment['data']['fees'];
                                $reqRq['payment_gateway_transaction_id']=$retunPayment['data']['reference'];
                                $TransactionReturn=  $TransactionLogService->updateTransactionDetails((object)$reqRq);
                                if($TransactionReturn['statusCode']===200){
                                    $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                                    "request_status"=>"PAYMENT","payment_method"=>"CASH","payment_status"=>"COMPLETED","driver_id"=>$request->user_id]);
                                    $message="Thank you for collecting the payment";
                                    $status="PAYMENT";
                                    $nextStep="RATING";
                                }
                            }
                            else{
                                // handle paymanet failed
                            }
                        }
                        else{
                             // if payment is success update the transcation table
                             $TransactionLogService = new TransactionLogService();
                             $reqRq['payment_gateway_charge']=0;
                             $reqRq['payment_gateway_transaction_id']=$reqRq['promo_code'];
                             $TransactionReturn=  $TransactionLogService->updateTransactionDetails((object)$reqRq);
                             if($TransactionReturn['statusCode']===200){
                                 $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                                 "request_status"=>"PAYMENT","payment_method"=>"CASH","payment_status"=>"COMPLETED","driver_id"=>$request->user_id]);
                                 $message="Thank you for collecting the payment";
                                 $status="PAYMENT";
                                 $nextStep="RATING";
                             }
                        }
                    }
                    else{
                        $message="Please wait a while, as we calculate the payable fare";
                        $status="DROP";
                        $nextStep="PAYMENT";
                    }
                break;
                case "PAYMENT":
                    if($request->payment_method=="CASH"){
                      // update the trascation log table as payament completed,
                      // update the service request table to PAYMENT and mark payment completed,
                      $TransactionLogService = new TransactionLogService();
                      $TransactionLogService->updateCreditNoteStatusForRide((object)["request_id"=>$request->request_id,"status"=>"COMPLETED"]);
                      $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                      "request_status"=>"PAYMENT","payment_method"=>"CASH","payment_status"=>"COMPLETED","driver_id"=>$request->user_id]);
                      $message="Thank you for collecting the payment";
                      $status="PAYMENT";
                      $nextStep="RATING";
                    }
                   // else if()

                break;
                case "RATING":
                    // update the service request with rating comment

                    $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                    "request_status"=>"RATING","rating_by_driver"=>$request->rating,"comment_by_driver"=>$request->comment,
                    "driver_rating_status"=>"GIVEN","driver_id"=>$request->user_id]);
                    $message="Thank you, for rating the customer.";
                    $status="RATING";
                    $nextStep="COMPLETED";
                     // push
                     $pushDate=[
                        "title"=>"Ride Completed",
                        "text"=>"Thank you for choosing us.",
                        "body"=>"Thank you for choosing us.",
                        "type"=>"RIDECOMPLETED",
                        "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        "setTo"=>'SINGLE',
                        "request_id"=>$ServiceRequestServiceReturn->request_id
                    ];
                    $NotificationServices->sendPushNotification((object)$pushDate);
                    // send sms
                    $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                    // email
                break;
                case "COMPLETED":
                    $message="Thank you for completing the ride safely. ";
                    $status="COMPLETED";
                    $nextStep="NORMAL";
                break;
            }
            return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }

}
