<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\DriversProfileService;
use App\Services\UsersDevices;
use App\Services\AuthService;
use App\Services\EmergencyContact;
use App\Services\DriversServiceType;
use App\Services\DriverPreferencesService;

use Validator;

class RegistrationController extends Controller
{
    public function register(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'social_unique_id' => ['required_if:login_by,facebook,google','unique:users'],
                'device_type' => 'required|in:android,ios',
                'device_token' => 'required',
                'device_id' => 'required',
                'login_by' => 'required|in:manual,facebook,google',
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email_id' => 'required|email|max:255|unique:drivers_profile',
                'password' => 'required_if:login_by,manual|between:6,255|confirmed',
                'timeZone'=>'required',
                'service_type_id'=>'required',
                'model'=>'required',
                'model_year'=>'required',
                'registration_no'=>'required',
               // 'registration_expire'=>'required',
                "vechile_make"=>'required',
                'gender'=>'sometimes|nullable|in:Male,Female,Transgender,Others',
                "device_latitude"=>"sometimes",
                "device_longitude"=>"sometimes"
               // 'vechile_identification_no'=>'required'

            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_scope="driver-service";

            $User=new UserService();

            $Profile=new DriversProfileService();
            $UserDevice=new UsersDevices();
            $EmergencyContact=new EmergencyContact();
            $DriversServiceType=new DriversServiceType();
            $DriverPreferencesService=new DriverPreferencesService();

            $ProfileData=(object)[];
            $DevicesData=(object)[];
            $EmergencyContactData=[];
            $UserData=$User->accessCreateUser($request);
            if($UserData->_id!==""){
                $request->user_id=$UserData->_id;
                // modified during the request dev mpved the creat of serviece type first
                $request->status="offline";
                $request->service_status="inactive";

                $DriversServiceTypeData=$DriversServiceType->accessCreate($request);
                $request->active_service=$DriversServiceTypeData['data']->_id;
                $request->active_service_type=$DriversServiceTypeData['data']->service_type_id;

                //==============================
                $ProfileData=$Profile->accessCreateProfile($request);
                $DevicesData=$UserDevice->accessCreateDevices($request);
                $EmergencyContactData=$EmergencyContact->accessGetContact($request);



              //  print_r($ProfileData); exit;

                // Intergate Driver service to its profile
                //$request->profile_id=0;







                $request->femaleFriendly="No";
                if($request->gender=="Female")
                   $request->femaleFriendly="Yes";

               // $DriverPreferencesService =$DriverPreferencesService->accessCreatePreferences($request);
            }
            if(!empty($UserData)){
                if($UserData->_id!==""){
                    //autologin to the system.
                    $UserAuth=new AuthService();
                    $UserToken=$UserAuth->accessAutoLogin((object)['user_id'=>$UserData->_id,"user_scope"=>"temporary-customer-service"]);
                    if($UserToken['statusCode']==200){
                        $UserAccessToken=$UserToken['data']->access_token;
                        $token_type=$UserToken['data']->token_type;
                        return response(['message'=>"Thank you for registering with us!","data"=>(object)["access_token"=>$UserAccessToken,"token_type"=>$token_type,"user_profile"=>$Profile->setProfileData($ProfileData,$request->login_by),"emergency_contacts"=>$EmergencyContactData,"driver_services"=>$DriversServiceTypeData['data']],"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],201);
                    }
                    else{
                        return response(['message'=>$UserToken['message'],"data"=>$UserToken['data'],"errors"=>$UserToken['errors']],$UserToken['statusCode']);
                    }
                }
            }
            return response(['message'=>"Registration not possible. Contact Administrator.","data"=>(object)[],"errors"=>array("exception"=>["Not Found"],"e"=>[])],404);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }

}
