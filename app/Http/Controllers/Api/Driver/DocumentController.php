<?php 

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Model\Document\DriverDocuments;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use App\Model\Document\Document;

use Validator;

class DocumentController extends Controller
{
       /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $document_id
     * @return \Illuminate\Http\Response
     */
    public function uploadDocument(Request $request,$document_id){
        try{
            $id=Auth::user()->id;
            $rule=['file' => 'required|mimes:jpeg,bmp,png'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>["Request Validation Failed"])],422);
            }
            $Document = DriverDocuments::where('user_id', $id)->where('document_id', $document_id)->get()->toArray();
            if(!empty($Document)){
                $DriverDocuments = DriverDocuments::where('user_id', $id)->where('document_id', $document_id)->firstOrFail();
                if(isset($request->file) && !empty($request->file)){
                    $picture = $request->file->store('public/driver/'.$id.'/documents');
                    $picture=str_replace("public", "storage", $picture);
                    $DriverDocuments->url="http://demos.mydevfactory.com/debarati/ridyr/public/".$picture;
                    $DriverDocuments->status='ASSESSING';
                    $DriverDocuments->save();
                }
            }
            else{
                if(isset($request->file) && !empty($request->file)){
                    $picture = $request->file->store('public/driver/'.$id.'/documents');
                    $picture=str_replace("public", "storage", $picture);
                    $DriverDocuments=new DriverDocuments;
                    $DriverDocuments->url="http://demos.mydevfactory.com/debarati/ridyr/public/".$picture;
                    $DriverDocuments->user_id=$id;
                    $DriverDocuments->document_id=$document_id;
                    $DriverDocuments->status='ASSESSING';
                    $DriverDocuments->save();
                }
            }

            return response(['message'=>"Document updated successfully","data"=>[],"errors"=>array("exception"=>["Document updated successfully."])],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }

    }
    public function list(Request $request)
    {
        try{
            $DocumentList=[
                'VehicleDocuments' => [],
                'DriverDocuments' => []
            ];
            $id=Auth::user()->id;
            $Document = Document::where('status', 1)->get()->toArray();
            foreach($Document as $key=>$val){
                $DriverDocuments=DriverDocuments::where('user_id',$id)->where("document_id",$val['_id'])->get()->toArray();
                $list=[
                    "id"=> $val['_id'],
                    "name"=> $val['name'],
                    "type"=> $val['type'],
                    "status"=> $val['status'],
                    "created_at"=>$val['created_at'],
                    "updated_at"=>$val['updated_at'],
                    "deleted_at"=> "",
                    "driver_document_status"=>"",
                    "driver_document_url"=>"",
                    "driver_document_expires_at"=>""
                ];
                if(count($DriverDocuments)>0){
                    foreach($DriverDocuments as $key =>$vals){
                        $list["driver_document_status"]=$vals["status"];
                        $list["driver_document_url"]=$vals["url"];
                    }
                }
                if($val['type']=="VEHICLE"){
                    $DocumentList['VehicleDocuments'][]=$list;
                }
                if($val['type']=="DRIVER"){
                    $DocumentList['DriverDocuments'][]=$list;
                }
                if($val['type']=="VEHICLE IMAGES"){
                    $DocumentList['VEHICLEIMAGES'][]=$list;
                }
            }
            return response(['message'=>"Document List send","data"=>(array)$DocumentList,"errors"=>array("exception"=>["Everything is OK."])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }
}

