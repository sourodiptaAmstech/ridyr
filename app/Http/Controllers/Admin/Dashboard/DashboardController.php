<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\Request\PassengerRequest;
use App\Model\Request\BidRequest;


class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $total_reg_driver = User::where('user_scope', 'driver-service')->orWhere('user_scope', 'driver-passenger-service')->orderBy('created_at' , 'desc')->count();
        // $rides = PassengerRequest::all();
        // $totalRides = $rides->count();
        // $cancel_bydriver = PassengerRequest::where('request_status','Cancel_By_Driver')->count();
        // $cancel_bypassenger = PassengerRequest::where('request_status','Cancel_By_Passenger')->count();
        // $scheduled_rides = PassengerRequest::where('booking_type','schedule_ride')->count();
        // $total_cancel_rides = $cancel_bydriver + $cancel_bypassenger;
        // $totalEarn = null;
        // if (count($rides)>0) {
        //     foreach ($rides as $key => $ride) {
        //         if ($ride->request_status == 'Completed') {
        //             $bidRequest = BidRequest::where('passenger_request_id',$ride->passenger_request_id)->where('driver_id',$ride->driver_id)->first();
        //             if (isset($bidRequest)) {
        //                 $totalEarn = $totalEarn+$bidRequest->bid_cost;
                       
        //             }
        //         }
        //     }
        // }
        // $completed_rides = PassengerRequest::where('request_status','Completed')->orderBy('created_at' , 'desc')->take(10)->get();
        // , compact('total_reg_driver','rides','total_cancel_rides','cancel_bydriver','cancel_bypassenger','scheduled_rides','totalEarn','completed_rides')
        return view('admin.dashboard.dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
