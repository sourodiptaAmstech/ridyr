<?php

namespace App\Http\Controllers\Admin\Document;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Document\Document;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use App\Model\Document\DriverDocuments;

class DocumentController extends Controller
{

    public function index()
    {
        $documents = Document::orderBy('created_at' , 'desc')->get();
        return view('admin.document.index', compact('documents'));
    }


    public function create()
    {
        return view('admin.document.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'type' => 'required|in:VEHICLE,DRIVER,VEHICLE IMAGE',
        ]);

        try{

            Document::create($request->all());
            return redirect()->route('admin.document.index')->with('flash_success','Document Saved Successfully');

        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Document Not Found');
        }

    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        try {
            $document = Document::findOrFail($id);
            return view('admin.document.edit',compact('document'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'type' => 'required|in:VEHICLE,DRIVER,VEHICLE IMAGE',
        ]);

        try {
            Document::where('_id',$id)->update([
                    'name' => $request->name,
                    'type' => $request->type,
                ]);
            return redirect()->route('admin.document.index')->with('flash_success', 'Document Updated Successfully');    
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Document Not Found');
        }
    }


    public function destroy($id)
    {
        try {
            $doc_count = Document::count();
            if ($doc_count>1) {
                $driver_document = DriverDocuments::where('document_id', $id)->get();

                if ($driver_document->count() > 0) {
                    return back()->with('flash_error', 'Document already used by driver. Does not allow to delete it');
                } else {
                    Document::find($id)->delete();
                    return back()->with('flash_success', 'Document deleted successfully');
                }
            } else {
                return back()->with('flash_error', 'Does not allowe to delete all document');
            }        

        } 
        catch (Exception $e) {
            return back()->with('flash_error', 'Document Not Found');
        }
    }

    public function changeStatus(Request $request, $id)
    {
        $document = Document::find($id);

        if ($document->status == 1) {
            $doc_count = Document::where('status', 1)->count();
            if ($doc_count>1) {
                $document->status = 0;
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Not allow to inactivate all document.'
                ], 400);
            }

        } else {
            $document->status = 1;
        }
        $document->save();
        
        return response()->json([
            'success' => true,
            'message' => 'Document status updated successfully'
        ], 200);
    }

}
