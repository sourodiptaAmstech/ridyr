<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;


class LoginController extends Controller
{
    protected $redirectTo = 'admin/dashboard';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


	public function showLoginForm()
	{
		return view('admin.auth.login');
	}

	public function login(Request $request)
	{
        
		$request->validate([
                'email'=>'email|required',
                'password'=>'required'
            ]);

        //$remember = $request->has('remember') ? true : false;
        //$remember = $request->get('remember');
        
    	if(Auth::attempt(['username' => $request->email, 'password' => $request->password, 'user_scope'=>'admin-service']))
        {
            return redirect()->route('admin.dashboard.index')->with('flash_success','Logged in Successfully');
        } else {
            return redirect()->route('/login')->with('flash_error','Invalid credentials');
        }
	}

    public function logout(Request $request) {

        Auth::logout();
        return redirect()->route('/login');
    }

}
