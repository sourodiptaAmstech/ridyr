<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Facades\Notification;
use App\Notifications\ResetPasswordOTP;


class ForgotPasswordController extends Controller
{
    
	public function otpRequestForm()
	{
		return view('admin.auth.passwords.email');
	}

	public function sendOTP(Request $request)
	{
        try {
            $request->validate(['email'=>'email|required']);
            $user = User::where('username' , $request->email)->where('user_scope', 'admin-service')->first();
            if(empty($user)){
                return response([
                    'success' => false,
                    'message'=>"Couldn't find your Account",
                    "errors"=>array("email"=>["Couldn't find your Account"])
                ],404);
            }

            $otp = mt_rand(1000, 9999);
            $user->resetpasssword_otp = $otp;
            $user->save();
            Notification::send($user, new ResetPasswordOTP($otp));
    
            return response([
                'success' => true,
                'message'=>"OTP sent to your email ID",
                "errors"=>(object)array()
            ],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
    }

    public function otpForm()
    {

        return view('admin.auth.passwords.otp');
    }

    public function otpVarification(Request $request)
    {
        try {
            $code1 = $request->code1;
            $code2 = $request->code2;
            $code3 = $request->code3;
            $code4 = $request->code4;
            $otp = $code1.$code2.$code3.$code4;
            $user = User::where('resetpasssword_otp' , (int)$otp)->where('user_scope', 'admin-service')->first();
            if(empty($user)){
                return response([
                    'message'=>"Invalid Otp, try again!",
                    "field"=>"resetpasssword_otp",
                    "errors"=>array("exception"=>["Request Validation Failed"])
                ],422);
            } else {
                return response([
                    'success' => true,
                    'message' => "Otp validated",
                ],201);
            }
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","errors"=>array("exception"=>["Bad Request"])],400);
        }
    }

    public function passworUpdateForm()
    {
        return view('admin.auth.passwords.reset');
    }
   

    public function updatePassword(Request $request)
    {
       $validatedData = $request->validate([
            'email'=>'required|email',
            'password' => 'required|string|min:6|same:password_confirmation',
            'password_confirmation' => 'required'
        ]);
        try {
            $user = User::where('username' , $request->email)->where('user_scope', 'admin-service')->first();
            if (empty($user)) {
               return back()->with('flash_error','Couldn\'t find your Account! Enter correct Email');
            }
            $user->password = bcrypt($request->get('password'));
            $user->save();
            return redirect()->route('/login')->with('flash_success','Password Updated');
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }

}
