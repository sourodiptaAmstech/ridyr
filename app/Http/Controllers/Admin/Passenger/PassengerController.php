<?php

namespace App\Http\Controllers\Admin\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Profiles\PassengersProfile;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use App\User;
// use Storage;
use App\Services\PassengersProfileService;



class PassengerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.passenger.index');
    }

    public function ajaxPassenger(Request $request)
    {
        $columns = array(
            0 => 'first_name',
            1 => 'email_id',
            2 => 'isd_code',
        );
        
        $totalData =  User::where('user_scope', 'passenger-service')->count();
        $limit = $request->input('length');
        $limit = (integer)$limit;
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if ($dir == 'asc') {
            $dir = 1;
        } else {
            $dir = -1;
        }

        if(empty($request->input('search.value'))){
            $passengers = User::where('user_scope', 'passenger-service')
                ->get();
            foreach ($passengers as $key => $passenger) {
                $pf = PassengersProfile::where('user_id',$passenger->_id)
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->first();
                $passenger->first_name = $pf->first_name;
                $passenger->last_name  = $pf->last_name;
                $passenger->email_id   = $pf->email_id;
                $passenger->mobile_no  = $pf->mobile_no;
                $passenger->isd_code   = $pf->isd_code;
            }
            
            $totalFiltered = User::where('user_scope', 'passenger-service')->count();
        }else{
            $search = $request->input('search.value');
            $passengers = PassengersProfile::where(function($q) use ($search){
                        $q->where('first_name', 'like', "%{$search}%")
                        ->orWhere('last_name','like',"%{$search}%")
                        ->orWhere('email_id','like',"%{$search}%")
                        ->orWhere('isd_code','like',"%{$search}%")
                        ->orWhere('mobile_no','like',"%{$search}%");
                    })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
            
            foreach ($passengers as $key => $passenger) {
                $user = User::where('_id', $passenger->user_id)->first();
                $passenger->_id = $user->_id;
                $passenger->username = $user->username;  
            }
            
            $totalFiltered = PassengersProfile::where(function($q) use ($search){
                    $q->where('first_name', 'like', "%{$search}%")
                    ->orWhere('last_name','like',"%{$search}%")
                    ->orWhere('email_id','like',"%{$search}%")
                    ->orWhere('isd_code','like',"%{$search}%")
                    ->orWhere('mobile_no','like',"%{$search}%");
                })
                ->count();
        }

        $data = array();

        if($passengers){
            foreach($passengers as $p){
                $nestedData['passenger_name']   = $p->first_name." ".$p->last_name;
                $nestedData['email']   = $p->username;
                $nestedData['mobile']  = $p->isd_code.'-'.$p->mobile_no;
                $nestedData['action'] = '<a href="passenger/'.$p->_id.'/edit" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        
        echo json_encode($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.passenger.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name'        => 'required|max:255',
            'last_name'         => 'required|max:255',
            'email'             => 'required|email|max:255|unique:users,username',
            'picture'           => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'password'          => 'required|confirmed|min:6',
            'mobile_no'         => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:6|max:16|unique:passengers_profile'
        ]);
        try{
            
            if (empty($request->mobile_no)) {
                $request->code = null;
            } else {
                $request->code = '+'.$request->code;
            }
            // creating new users
            $User = new User();
            $User->password = bcrypt(trim($request->password));
            $User->user_scope = "passenger-service";
            $User->username = $request->email;
            $User->save();

            // create the user's profile
            $PassengersProfile = new PassengersProfile();
            $PassengersProfile->user_id = $User->_id;
            $PassengersProfile->email_id = $request->email;
            $PassengersProfile->first_name = $request->first_name;
            $PassengersProfile->last_name = $request->last_name;
            if(isset($request->picture) && !empty($request->picture)){
                $picture = $request->picture->store('public/passenger/profile');
                $picture = str_replace("public", "storage", $picture);
                $PassengersProfile->picture = $picture;
            }
            $PassengersProfile->mobile_no = $request->mobile_no;
            $PassengersProfile->isd_code = $request->code;
            $PassengersProfile->dob=$request->dob;
            $PassengersProfile->gender=$request->gender;
            $PassengersProfile->save();
           
            if(!empty($User)){

                if($User->_id>0){
                    return redirect()->back()->with('flash_success', 'Thank you for registering with us!');
                }
            }
            return redirect()->back()->with('flash_error', 'Registration not possible!');
        }

        catch(\Illuminate\Database\QueryException  $e){
            return redirect()->back()->with('flash_error', 'Something went wrong!');
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return redirect()->back()->with('flash_error', 'Something went wrong!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        try {
            $passenger = User::find($id);
            $Profile=new PassengersProfileService();
            $request->user_id = $id;
            $ProfileData = $Profile->accessGetProfile($request);
            $userProfile = $Profile->setProfileData($ProfileData['data'],$passenger->login_type);
            $passenger->passenger_profile = $userProfile;
            return view('admin.passenger.edit',compact('passenger'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name'    => 'required|max:255',
            'last_name'     => 'required|max:255',
            'email'         => 'required|email|max:255|unique:users,username,'.$id.',_id',
            'picture'       => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'mobile'        => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:6|max:16|unique:passengers_profile,mobile_no,'.$id.',user_id',
        ]);
        if (empty($request->mobile)) {
            $request->code = null;
        } else {
            $request->code = '+'.(integer)$request->code;
        }
        try {
            $passenger = User::find($id);
            $passenger->username = $request->email;
            $passenger->save();

            $passenger_profile = PassengersProfile::where('user_id', $id)->first();
            if (isset($passenger_profile)) {
                $passenger_profile->first_name    = $request->first_name;
                $passenger_profile->last_name     = $request->last_name;
                $passenger_profile->email_id      = $request->email;
                if(isset($request->picture) && !empty($request->picture)){
                    //$Storage=Storage::delete($passenger->picture);
                    $picture = $request->picture->store('public/passenger/'.$id.'/profile');
                    $picture = str_replace("public", "storage", $picture);
                    $passenger_profile->picture = $picture;
                }
                $passenger_profile->mobile_no = $request->mobile;
                $passenger_profile->isd_code  = $request->code;
                $passenger_profile->dob       = $request->dob;
                $passenger_profile->gender    = $request->gender;
                $passenger_profile->save();
            }
            
            return redirect()->route('admin.passenger.index')->with('flash_success', 'Passenger Updated Successfully');    
        }
        catch (Exception $e) {
            return back()->with('flash_error', 'Passenger Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
