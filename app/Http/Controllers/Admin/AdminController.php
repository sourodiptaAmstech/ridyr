<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Hash;
use Auth;
use Validator;
use App\Model\Profiles\AdminsProfile;

class AdminController extends Controller
{
    public function password()
    {
    	return view('admin.account.change-password');
    }

    public function changePassword(Request $request)
    {
   
        $validatedData = $request->validate([
            'old_password' => 'required',
            'password' => 'required|string|min:6|same:password_confirmation',
            'password_confirmation' => 'required'
        ]);

        try {

	        if (!(Hash::check($request->get('old_password'), Auth::user()->password))) {

	        	return redirect()->back()->with('flash_error','Your old password does not matches with the password you provided. Please try again');
	        }
	        if(strcmp($request->get('old_password'), $request->get('password')) == 0){

	        	return redirect()->back()->with('flash_error','New Password cannot be same as your old password. Please choose a different password');
	        }
	     
	        $user = Auth::user();
	        $user->password = bcrypt($request->get('password'));
	        $user->save();

	        return redirect()->back()->with('flash_success','Password Updated');

	    } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }

    }

    public function profile()
    {
    	return view('admin.account.profile');
    }

    public function profileUpdate(Request $request)
    {
    	$validatedData = $request->validate([
            'first_name'    => 'required|max:255',
            'last_name'     => 'required|max:255',
            'email'         => 'required|email|max:255|unique:users,username,'.Auth::user()->id.',_id',
            'mobile'        => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:6|max:16|unique:admins_profile,mobile_no,'.Auth::user()->id.',user_id',
            'picture'       => 'mimes:jpeg,jpg,bmp,png|max:5242880',
           
        ]);

        if (empty($request->mobile)) {
            $request->code = null;
        } else {
            $request->code = '+'.(integer)$request->code;
        }

        try{
            $user = Auth::user();
            $user->username = $request->email;
            $user->save();

            $admin_profile = AdminsProfile::where('user_id',Auth::user()->id)->first();
            $admin_profile->first_name   = $request->first_name;
            $admin_profile->last_name    = $request->last_name;
            $admin_profile->email_id     = $request->email;
            $admin_profile->mobile_no    = $request->mobile;
            $admin_profile->isd_code     = $request->code;
            if(isset($request->picture) && !empty($request->picture)){
                $picture = $request->picture->store('public/admin/profile');
                $picture=str_replace("public", "storage", $picture);
                $admin_profile->picture=$picture;
            }
            $admin_profile->save();
            
            return redirect()->back()->with('flash_success','Profile Updated');
        }

        catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
        
    }

}
