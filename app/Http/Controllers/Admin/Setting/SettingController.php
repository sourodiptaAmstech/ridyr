<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Setting\Setting;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $site_settings = Setting::where('key', 'like', "%site%")->get();
        return view('admin.setting.index', compact('site_settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $site_setting = Setting::findOrFail($id);
            return view('admin.setting.edit',compact('site_setting'));
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Site Setting Not Found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $site_setting = Setting::findOrFail($id);
            if ($request->key == 'site_title'||$request->key == 'site_copyright') {
                $this->validate($request, [
                    'value' => 'required',
                ]);
                $site_setting->value = $request->value;

            } else {
                $this->validate($request, [
                    'value' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
                ]);
                if(isset($request->value) && !empty($request->value)){
                    //$Storage=Storage::delete($site_setting->value);
                    $value = $request->value->store('public/setting/'.$id);
                    $value=str_replace("public", "storage", $value);
                    $site_setting->value=$value;
                }
            }
            $site_setting->save();
            return redirect()->route('admin.setting.index')->with('flash_success', 'Site Setting Updated Successfully');    
        } 

        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Site Setting Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
