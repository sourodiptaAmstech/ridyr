<?php

namespace App\Http\Controllers\Admin\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Profiles\DriverProfiles;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use App\User;
// use App\Model\Driver\DriverServices;
// use App\Model\ServiceType;
use App\Model\Document\DriverDocuments;
use App\Model\Document\Document;
use App\Model\Driver\DriverCarImages;
use App\Http\Controllers\Api\Transaction\TransactionController;
use Illuminate\Support\Facades\Notification;
use App\Notifications\EmailNotification;
use App\Model\Driver\DriverDocumentReason;
use App\Model\Driver\DriverReason;

use App\Model\ServiceType\DriverServiceType;
use App\Services\ServiceTypeMst;

use App\Services\DriversProfileService;



class DriverController extends Controller
{
   
    public function index()
    {
        return view('admin.driver.index');
    }

    public function ajaxDriver(Request $request)
    {
        $columns = array(
            0 => 'first_name',
            1 => 'email_id',
            2 => 'isd_code',
        );
        
        $totalData =  User::where('user_scope', 'driver-service')->count();
        $limit = $request->input('length');
        $limit = (integer)$limit;
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if ($dir == 'asc') {
            $dir = 1;
        } else {
            $dir = -1;
        }

        if(empty($request->input('search.value'))){
            $drivers = User::where('user_scope', 'driver-service')
                ->get();
            foreach ($drivers as $key => $driver) {
                $df = DriverProfiles::where('user_id',$driver->_id)
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->first();
                $driver->first_name = $df->first_name;
                $driver->last_name  = $df->last_name;
                $driver->email_id   = $df->email_id;
                $driver->mobile_no  = $df->mobile_no;
                $driver->isd_code   = $df->isd_code;
                $driver->service_status   = $df->service_status;
            }
            
            $totalFiltered = User::where('user_scope', 'driver-service')->count();
        }else{
            $search = $request->input('search.value');
            $drivers = DriverProfiles::where(function($q) use ($search){
                        $q->where('first_name', 'like', "%{$search}%")
                        ->orWhere('last_name','like',"%{$search}%")
                        ->orWhere('email_id','like',"%{$search}%")
                        ->orWhere('isd_code','like',"%{$search}%")
                        ->orWhere('mobile_no','like',"%{$search}%");
                    })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
            
            foreach ($drivers as $key => $driver) {
                $user = User::where('_id', $driver->user_id)->first();
                $driver->_id = $user->_id;
                $driver->username = $user->username;  
            }
            
            $totalFiltered = DriverProfiles::where(function($q) use ($search){
                    $q->where('first_name', 'like', "%{$search}%")
                    ->orWhere('last_name','like',"%{$search}%")
                    ->orWhere('email_id','like',"%{$search}%")
                    ->orWhere('isd_code','like',"%{$search}%")
                    ->orWhere('mobile_no','like',"%{$search}%");
                })
                ->count();
        }       

        $data = array();

        if($drivers){
            foreach($drivers as $d){
                if (isset($d->service_status)) {
                    if($d->service_status == "inactive" || $d->service_status == "block"){
                        $activationButton = '<a href="driver/activation/'.$d->id.'" class="btn btn-danger driver-status" data-toggle="tooltip" data-value="Activate" data-id="'.$d->id.'" title="Click to Active">Inactive</a>';
                    }else{
                        $activationButton = '<a href="driver/activation/'.$d->id.'" class="btn btn-success driver-status" data-toggle="tooltip" data-value="Inactivate" data-id="'.$d->id.'" title="Click to Inactive">Active</a>';
                    }
                }else{
                    $activationButton = null;
                }

                // $nestedData['id']     = $d->_id;
                $nestedData['driver_name']   = $d->first_name." ".$d->last_name;
                $nestedData['email']   = $d->username;
                $nestedData['mobile']  = $d->isd_code.'-'.$d->mobile_no;
                $nestedData['action'] = '<span style="line-height: 33px;"><a href="driver/'.$d->_id.'/edit" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a> <a href="drivers/document/'.$d->_id.'" class="btn btn-info"><i class="fa fa-file"></i> Document</a>' .$activationButton.'</span>';
                    
                    // <a href="driver/carimages/'.$d->id.'" class="btn btn-info"><i class="fa fa-file"></i> Car Images</a>
                    // <a href="driver/transaction/'.$d->id.'" class="btn btn-info"><i class="fa fa-file"></i> Package</a>

                    // <a href="drivers/service-type/'.$d->_id.'" class="btn btn-info"><i class="fa fa-file"></i> Service</a>

                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        
        echo json_encode($json_data);
    }

    public function listDriverServiceType($id)
    {
        try{
            $ServiceType=DriverServiceType::select('user_id','service_type_id','registration_no','registration_expire','model','vechile_identification_no','driver_service_type_id','updated_at','created_at')->where("user_id",$id)->first();
            if (isset($ServiceType)){
                $ServiceTypeMst=new ServiceTypeMst();
                $ServiceTypeMstData=$ServiceTypeMst->accessGetNameByID($ServiceType);
                $ServiceType->serviceName=$ServiceTypeMstData->name;
                return view('admin.driver.service',compact('ServiceType'));
            } else {
                 return redirect()->back()->with('flash_error', 'Service type not found!');
            }
        }
        catch(\Illuminate\Database\QueryException  $e){
            return redirect()->back()->with('flash_error', 'Something went wrong!');
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return redirect()->back()->with('flash_error', 'Something went wrong!');
        }
        catch(ModelNotFoundException $e){
            return redirect()->back()->with('flash_error', 'You are not registered with us!');
        }
    }
  
    public function create()
    {
        return view('admin.driver.create');
    }

    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'first_name'        => 'required|max:255',
                'last_name'         => 'required|max:255',
                'email'             => 'required|email|max:255|unique:users,username',
                'picture'           => 'mimes:jpeg,jpg,bmp,png|max:5242880',
                'password'          => 'required|confirmed|min:6',
                'mobile'            => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:6|max:16|unique:drivers_profile,mobile_no'
            ]);
            
            if (empty($request->mobile)) {
                $request->code = null;
            } else {
                $request->code = '+'.$request->code;
            }
            // creating new users
            $User = new User();
            $User->password=bcrypt(trim($request->password));
            $User->user_scope="driver-service";
            $User->username=$request->email;
            $User->save();

            $DriverProfiles=new DriverProfiles();
            $DriverProfiles->user_id=$User->_id;
            $DriverProfiles->first_name=$request->first_name;
            $DriverProfiles->last_name=$request->last_name;
            $DriverProfiles->email_id = $request->email;
            if(isset($request->picture) && !empty($request->picture)){
                $picture = $request->picture->store('public/driver/profile');
                $picture = str_replace("public", "storage", $picture);
                $DriverProfiles->picture=$picture;
            }
            $DriverProfiles->mobile_no=$request->mobile;
            $DriverProfiles->isd_code=$request->code;
            $DriverProfiles->dob=$request->dob;
            $DriverProfiles->gender=$request->gender;

            $DriverProfiles->save();
            
            if(!empty($User)){

                if($User->_id>0){
                    return redirect()->back()->with('flash_success', 'Thank you for registering with us!');
                }
            }
            return redirect()->back()->with('flash_error', 'Registration not possible!');
        }

        catch(\Illuminate\Database\QueryException  $e){
            return redirect()->back()->with('flash_error', 'Something went wrong!');
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return redirect()->back()->with('flash_error', 'Something went wrong!');
        }

    }

   
    public function show($id)
    {
        //
    }

    
    public function edit(Request $request,$id)
    {
        try {
            $driver = User::find($id);
            $Profile=new DriversProfileService();
            $request->user_id = $id;
            $ProfileData = $Profile->accessGetProfile($request);
            $userProfile = $Profile->setProfileData($ProfileData['data'],$driver->login_type);
            $userProfile['dob'] = $ProfileData['data']->dob;
            $driver->driver_profile = $userProfile;
            return view('admin.driver.edit',compact('driver'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

   
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'first_name'    => 'required|max:255',
            'last_name'     => 'required|max:255',
            'email'         => 'required|email|max:255|unique:users,username,'.$id.',_id',
            'picture'       => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'mobile'        => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:6|max:16|unique:drivers_profile,mobile_no,'.$id.',user_id'
        ]);
        
        if (empty($request->mobile)) {
            $request->code = null;
        } else {
            $request->code = '+'.(integer)$request->code;
        }
        try {
            $driver = User::find($id);
            $driver->username = $request->email;
            $driver->save();

            $DriverProfiles = DriverProfiles::where('user_id', $id)->first();
            $DriverProfiles->first_name    = $request->first_name;
            $DriverProfiles->last_name     = $request->last_name;
            $DriverProfiles->email_id      = $request->email;
            if(isset($request->picture) && !empty($request->picture)){
                $picture = $request->picture->store('public/driver/'.$id.'/profile');
                $picture = str_replace("public", "storage", $picture);
                $DriverProfiles->picture=$picture;
            }
            $DriverProfiles->mobile_no     = $request->mobile;
            $DriverProfiles->isd_code      = $request->code;
            $DriverProfiles->dob           =$request->dob;
            $DriverProfiles->gender        =$request->gender;
            $DriverProfiles->save();

            return redirect()->route('admin.driver.index')->with('flash_success', 'Driver Updated Successfully');    
        }

        catch (Exception $e) {
            return back()->with('flash_error', 'Driver Not Found');
        }
    }

    public function listDriverDocument($id)
    {
        $driver_documents = DriverDocuments::where('user_id', $id)->get();
        if ($driver_documents->count()>0) {
            return view('admin.driver.document', compact('driver_documents'));
        } else {
            return redirect()->back()->with('flash_error', 'Document not found!');
        }
    }

    public function driverDocumentVerification(Request $request,$id)
    {
        $user = User::find($request->user_id);
        $document = DriverDocuments::where('_id', $request->document_id)->first();

        if ($request->status == 'ACTIVE') {
            $document->status = $request->status;
        }
        if ($request->status == 'INVALID') {
            $document->status = $request->status;
            // //send email
            // $msg="Your document ".$document->document->name." has been invalided by admin. The reason for invalidation is that ".$request->reason;
            // $sub = 'Document Invalid Notification';
            // Notification::send($user, new EmailNotification($msg,$sub));
            // //send sms
            // $bdmsg = "Your document ".$document->document->name." has been invalided by admin.";
            // $this->twilioMobileNotification($user->isdCode,$user->mobile,$bdmsg);
        }
        if ($request->status == 'EXPIRE') {
            $document->status = $request->status;
            // //send email
            // $msg="Your document ".$document->document->name." has been expired by admin. The reason for expiration is that ".$request->reason;
            // $sub = 'Document Expire Notification';
            // Notification::send($user, new EmailNotification($msg,$sub));
            // //send sms
            // $bdmsg = "Your document ".$document->document->name." has been expired by admin.";
            // $this->twilioMobileNotification($user->isdCode,$user->mobile,$bdmsg);
        }
        $document->save();

        return response()->json([
            'success' => true,
            'message' => 'Document verified successfully'
        ], 200);

    }

    public function driverActivationProcess($id)
    {
        $user = User::find($id);
        $driver_profile = DriverProfiles::where('user_id', $id)->first();
        
        if ($driver_profile->service_status == 'active') {
            //$driver_profile = DriverProfiles::where('user_id', $id)->get();
            $driver_profile->service_status = 'block';
            $driver_profile->save();
            // //send email
            // $msg="Your driver account has been Inactivated by admin.";
            // $sub = 'ZoomXoom Driver InActivation Notification';
            // Notification::send($user, new EmailNotification($msg,$sub));
            //send sms
            // $bdmsg = 'Your ZoomXoom driver account has been Inactivated by admin.';
            // $this->twilioMobileNotification($user->isdCode,$user->mobile,$bdmsg);
            return redirect()->back()->with('flash_success', 'Driver is inactivated successfully');

        } else {

        if (isset($driver_profile)) {

        if (isset($driver_profile->isMobileverified)&&$driver_profile->isMobileverified != 0) {

            $driver_service = DriverServiceType::where('user_id', $id)->first();
        
                if (isset($driver_service)) {

                        if (isset($driver_service->registration_no)) {
                            // if ($driver_service->registration_expire > date('Y-m-d')) {

                                $documents = Document::where('status', 1)->get();
                               
                                if ($documents->count()>0) {

                                  foreach ($documents as $key => $document) {
                                    $document = DriverDocuments::where('user_id', $id)->where('document_id',$document->id)->first();
                                    if (!isset($document)) {
                                        return redirect()->back()->with('flash_error', 'Not allow to activate driver! All documents are not uploaded. Please upload all documents!');
                                    }
                                  }

                                  foreach ($documents as $key => $document) {
                                    $active_document = DriverDocuments::where('user_id', $id)->where('document_id',$document->id)->where('status','ACTIVE')->first();
                                    if (!isset($active_document)) {
                                        return redirect()->back()->with('flash_error', 'Uploaded documents are not active document. Not allow to activate driver! Please activate all uploaded documents!');
                                    }
                                  }

                                  //$driver_profile = DriverProfiles::where('user_id', $id)->get();
                                  
                                    $driver_profile->service_status = 'active';
                                    $driver_profile->save();
                                    // //send email
                                    // $msg="Welcome! Your driver account is activated successfully.";
                                    // $sub = 'ZoomXoom Driver Activation Notification';
                                    // Notification::send($user, new EmailNotification($msg,$sub));
                                    // //send sms
                                    // $bdmsg = 'Welcome! Your ZoomXoom driver account is activated successfully.';
                                    // $this->twilioMobileNotification($user->isdCode,$user->mobile,$bdmsg);

                                    return redirect()->back()->with('flash_success', 'Driver is activated successfully');
                                 
                                } else {
                                    return redirect()->back()->with('flash_error', 'Document not found. Not allow to activate driver!');
                                }

                            // } else {
                            //     return redirect()->back()->with('flash_error', 'Driver\'s car number has expired. Not allow to activate driver!');
                            // }

                        }else{
                            return redirect()->back()->with('flash_error', 'Car registration number not found! Not allow to activate driver');
                        }

                } else {
                    return redirect()->back()->with('flash_error', 'Driver service not found! Not allow to activate driver');
                }
              
            } else {
                return redirect()->back()->with('flash_error', 'Mobile is not verified! Not allow to activate driver');
            }

        } else {
            return redirect()->back()->with('flash_error', 'Driver profile not found. Not allow to activate driver!');
        }

      }

    }
   
    
    public function driverCarImages($id)
    {
        $service = DriverServices::where('user_id', $id)->first();
        if (!empty($service)) {
            $carimages = DriverCarImages::where('service_id', $service->service_id)->get();
            if ($carimages->count()>0) {
                return view('admin.driver.carimage', compact('carimages'));
            } else {
                return redirect()->back()->with('flash_error', 'Driver car images not found!');
            }
        } else {
            return redirect()->back()->with('flash_error', 'Driver service not found!');
        }
    }

    public function destroy($id)
    {
        //
    }

    public function driverTransaction($id)
    {
        $transaction_obj = new TransactionController;
        $transaction = $transaction_obj->getPackagesBrought($id);

        if($transaction['statusCode'] == 200){
            return view('admin.driver.package', compact('transaction'));
        } elseif ($transaction['statusCode'] == 500) {
            return redirect()->back()->with('flash_error', 'Something went wrong!');
        } elseif ($transaction['statusCode'] == 400) {
            return redirect()->back()->with('flash_error', 'Something went wrong!');
        } else {
            return redirect()->back()->with('flash_error', 'Your not authorized to access!');
        }
    }


    public function twilioMobileNotification($isdCode,$mobile,$bdmsg)
    {
        $twilio_id="AC8a3e5f8b2adfaa3f1af9cbeb265c06a7";
        $twilio_token="073e05c75c6f5211fa3fbe961ddba752";
        $authBase64=base64_encode($twilio_id.":".$twilio_token);
        $twilio_ph_no="+14159656861";
        //curl of twillo
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.twilio.com/2010-04-01/Accounts/$twilio_id/Messages.json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_USERPWD=>"$twilio_id:$twilio_token",
            CURLOPT_HTTPAUTH=>CURLAUTH_BASIC,
            CURLOPT_POSTFIELDS => "Body=$bdmsg&From=$twilio_ph_no&To=$isdCode.$mobile",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
                "Authorization: Basic $authBase64"
            ),
        ));
        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $response=json_decode($response,true);
        $msg="OTP sent successfully!";
        if($httpcode!==201){
            $msg= $response['message'];
        }
        return ["message"=>$msg,"status"=>$httpcode];
    }

    public function documentVerificationReason(Request $request,$ids)
    {
        $this->validate($request, [
            'reason'    => 'required',
        ]);
        $reason = DriverDocumentReason::where('driver_id',$request->user_id)->where('document_id',$request->id)->first();
        if(empty($reason))
        {
            $reason = new DriverDocumentReason;
            $reason->driver_id = $request->user_id;
            $reason->document_id = $request->id;
            if ($request->status == 'INVALID') {
                $reason->reason_for_invalid = $request->reason;
            }
            if ($request->status == 'EXPIRE') {
                $reason->reason_for_expire = $request->reason;
            }
            $reason->save();
        } else {
            $reason->driver_id = $request->user_id;
            $reason->document_id = $request->id;
            if ($request->status == 'INVALID') {
                $reason->reason_for_invalid = $request->reason;
                $reason->reason_for_expire = null;
            }
            if ($request->status == 'EXPIRE') {
                $reason->reason_for_expire = $request->reason;
                $reason->reason_for_invalid = null;
            }
            $reason->save();
        }

        return response()->json([
            "success" => true,
            "message" => "Reason inserted successfully",
            "errors" => array("exception"=>["Everything is OK."]),
            "reason" => $request->reason
        ],201);
    }

    public function driverInactiveBannedReason(Request $request,$ids)
    {
        $this->validate($request, [
            'reason'    => 'required',
        ]);
        $user = User::find($request->id);

        $reason = DriverReason::where('driver_id',$request->id)->first();

        if(empty($reason))
        {
            $reason = new DriverReason;
            $reason->driver_id = $request->id;
            $reason->reason = $request->reason;
            $reason->save();
        } else {
            $reason->driver_id = $request->id;
            $reason->reason = $request->reason;
            $reason->save();
        }

        //send email
        $msg="Your driver account has been Inactivated by admin. The reason for inactivation is that ".$request->reason;
        $sub = 'ZoomXoom Driver InActivation Notification';
        Notification::send($user, new EmailNotification($msg,$sub));

        return response()->json([
            "success" => true,
            "message" => "Reason inserted successfully",
            "errors" => array("exception"=>["Everything is OK."])
        ],201);
    }

}
