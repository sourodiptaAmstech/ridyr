<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

//Route::get('bootstrap', 'Api\SettingController@bootstrap')->name("setting.bootstrap");


Route::post('passenger/signup', 'Api\Passenger\RegistrationController@register')->name("passenger.registration");
Route::post('passenger/login', 'Api\Passenger\AuthController@login')->name("passenger.login");
Route::post('passenger/forget/password', 'Api\Passenger\AuthController@forgetPassword')->name("passenger.forgetPassword");
Route::post('passenger/reset/password', 'Api\Passenger\AuthController@resetPassword')->name("passenger.resetPassword");
Route::post('payment/verify/transaction', 'Api\Payment\PayStackController@verifyTransaction')->name("verifyTransaction");

Route::group(['middleware' => ['auth:api','scope:temporary-customer-service']], function () {
    //Mobile no update and verification
    Route::post('passenger/mobile', 'Api\Passenger\ProfileController@updateMobileNo')->name("passenger.mobile");
    Route::put('passenger/mobile/verify', 'Api\Passenger\ProfileController@verifyMobileNo')->name("passenger.mobileVerify");
    Route::get('passenger/mobile/reverify', 'Api\Passenger\ProfileController@resetMobileVerificationOtp')->name("passenger.mobileReVerify");
    Route::post('passenger/profile/image/update', 'Api\Passenger\ProfileController@profileImageUpdate')->name("passenger.profileImage");
});
Route::group(['middleware' => ['auth:api','scope:passenger-service']], function () {
    //logout
    Route::get('passenger/logout', 'Api\Passenger\AuthController@logout')->name("passenger.logout");

    //profile images update
    Route::post('passenger/profile/image', 'Api\Passenger\ProfileController@profileImageUpdate')->name("passenger.profileImage");

    //profile update  getProfile
    Route::get('passenger/profile', 'Api\Passenger\ProfileController@getProfile')->name("passenger.profile");
    Route::post('passenger/profile/update', 'Api\Passenger\ProfileController@updateProfile')->name("passenger.updateProfile");


    // password
    Route::post('passenger/change/password', 'Api\Passenger\ProfileController@changePassword')->name("passenger.changePassword");

    //ride request.
    Route::post('passenger/get/estimated/fare','Api\Passenger\RequestController@estimated')->name("passenger.estimatedFare");
    Route::post('passenger/request/ride','Api\Passenger\RequestController@requestRide')->name("passenger.requestRide");
    Route::get('passenger/preferences','Api\Passenger\RequestController@getPreferences')->name("passenger.getPreferences");

    // background api
    Route::post('passenger/background/data', 'Api\Background\PassengerBackgroundController@get')->name("passenger.backgroundDb");




});
