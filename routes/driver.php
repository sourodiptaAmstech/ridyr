<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::post('driver/signup', 'Api\Driver\RegistrationController@register')->name("driver.registration");
Route::post('driver/login', 'Api\Driver\AuthController@login')->name("Driver.login");
Route::post('driver/forget/password', 'Api\Driver\AuthController@forgetPassword')->name("Driver.forgetPassword");
Route::post('driver/reset/password', 'Api\Driver\AuthController@resetPassword')->name("Driver.resetPassword");
Route::group(['middleware' => ['auth:api','scope:temporary-customer-service']], function () {
    //Mobile no update and verification
    Route::post('driver/mobile', 'Api\Driver\ProfileController@updateMobileNo')->name("driver.mobile");
    Route::put('driver/mobile/verify', 'Api\Driver\ProfileController@verifyMobileNo')->name("driver.mobileVerify");
    Route::get('driver/mobile/reverify', 'Api\Driver\ProfileController@resetMobileVerificationOtp')->name("driver.mobileReVerify");
    Route::post('driver/profile/image/update', 'Api\Driver\ProfileController@profileImageUpdate')->name("driver.profileImage");
});
Route::group(['middleware' => ['auth:api','scope:driver-service']], function () {
    //logout
    Route::get('driver/logout', 'Api\Driver\AuthController@logout')->name("driver.logout");

    //profile images update
    Route::post('driver/profile/image', 'Api\Driver\ProfileController@profileImageUpdate')->name("driver.profileImage");

    //profile update  getProfile
     Route::get('driver/profile', 'Api\Driver\ProfileController@getProfile')->name("driver.profile");
     Route::post('driver/profile/update', 'Api\Driver\ProfileController@updateProfile')->name("driver.updateProfile");
     Route::put('driver/on/off/update', 'Api\Driver\ProfileController@onOff')->name("driver.onOff");

   // document update
   Route::get('driver/document/list', 'Api\Driver\DocumentController@list')->name("driver.document.list");
   Route::post('driver/document/upload/{document_id}', 'Api\Driver\DocumentController@uploadDocument')->name("driver.document.upload");
  // Route::get('driver/document/completion/percentage', 'Api\Driver\StatusController@DocumentCompletion')->name("DocumentCompletion");


    // password
    Route::post('driver/change/password', 'Api\Driver\ProfileController@changePassword')->name("driver.changePassword");

    // background api
    Route::post('driver/background/data', 'Api\Background\DriversBackgroundController@get')->name("driver.backgroundDb");

    //preference
    Route::put('driver/profile/preference', 'Api\Driver\ProfileController@updatePrefernce')->name("driver.updatePrefernce");
    Route::get('driver/profile/preference', 'Api\Driver\ProfileController@getPrefernce')->name("driver.getPrefernce");

    //request action

    Route::post('driver/request/reject', 'Api\Driver\TripController@reject')->name("driver.request.reject");
    Route::post('driver/trip/control', 'Api\Driver\TripController@tripControl')->name("driver.trip.control");




});


