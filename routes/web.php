<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/login', 'Admin\Auth\LoginController@showLoginForm')->name('/login');
Route::get('/', 'Admin\Auth\LoginController@showLoginForm')->name('/');
Route::post('/loggedin', 'Admin\Auth\LoginController@login');
Route::get('/password/reset', 'Admin\Auth\ForgotPasswordController@otpRequestForm')->name('password.reset');
Route::post('password/email', 'Admin\Auth\ForgotPasswordController@sendOTP')->name('password.email');
Route::get('password/otp', 'Admin\Auth\ForgotPasswordController@otpForm')->name('password.otp');
Route::get('password/otpvarify', 'Admin\Auth\ForgotPasswordController@otpVarification')->name('password.otpvarify');
Route::get('password/edit', 'Admin\Auth\ForgotPasswordController@passworUpdateForm')->name('password.edit');
Route::post('password/update', 'Admin\Auth\ForgotPasswordController@updatePassword')->name('password.update');

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {

	Route::post('logout', 'Auth\LoginController@logout')->name('logout');
	Route::resource('dashboard', 'Dashboard\DashboardController');

	Route::get('password', 'AdminController@password')->name('password');
	Route::post('password/update', 'AdminController@changePassword')->name('password.update');

	Route::get('profile', 'AdminController@profile')->name('profile');
	Route::post('profile/update', 'AdminController@profileUpdate')->name('profile.update');

	Route::resource('setting', 'Setting\SettingController');
	// Route::get('privacy/policy', 'Setting\SettingController@privacyPolicy')->name('privacy.policy');
	// Route::put('privacy-policy/update/{id}', 'Setting\SettingController@privacyPolicyUpdate')->name('privacy-policy.update');

	Route::resource('document', 'Document\DocumentController');
	Route::get('document/active/{id}', 'Document\DocumentController@changeStatus')->name('document.active');

	// Route::resource('service', 'Service\ServiceTypeController');

	// Route::resource('vehicle', 'Vehicle\VehicleController');
	// Route::post('vehicles','Vehicle\VehicleController@getVehicles')->name('dataProcessing');

	Route::resource('passenger', 'Passenger\PassengerController');
	Route::post('ajax-passenger','Passenger\PassengerController@ajaxPassenger')->name('ajax-passenger');

	Route::resource('driver', 'Driver\DriverController');
	Route::post('ajax-driver','Driver\DriverController@ajaxDriver')->name('ajax-driver');

	Route::get('drivers/document/{id}', 'Driver\DriverController@listDriverDocument')->name('drivers.document');
	// Route::get('drivers/service-type/{id}', 'Driver\DriverController@listDriverServiceType')->name('drivers.service-type');
	Route::get('driver/document/verification/{id}', 'Driver\DriverController@driverDocumentVerification')->name('driver.document.verification');
	Route::get('driver/activation/{id}', 'Driver\DriverController@driverActivationProcess')->name('driver.activation');
	// Route::get('driver/carimages/{id}', 'Driver\DriverController@driverCarImages')->name('driver.carimages');
	// Route::get('driver/transaction/{id}', 'Driver\DriverController@driverTransaction')->name('driver.transaction');
	// Route::post('driver/document-reason/{id}', 'Driver\DriverController@documentVerificationReason')->name('driver.document-reason');

	// Route::resource('credit', 'CreditPackage\CreditPackageController');
	// Route::resource('email-control', 'Email\EmailController');

	// //Driver Document verification.........

	// Route::get('driver/check-document/{id}', 'Driver\DocumentVerificationController@allDocumentCheck')->name('driver.check-document');
	// Route::get('driver/rating-review/{id}', 'Driver\DriverRatingController@driverRatingReview')->name('driver.rating-review');
	// Route::get('driver/runcron/{id}', 'Driver\DocumentVerificationController@cronjobToMailDriver')->name('driver.runcron');
	// //.....................................

	// Route::get('driver/rating-list/{id}/{last_id}/{first_id}', 'Driver\DriverRatingController@ratingList')->name('driver.rating-list');
	// Route::post('driver/reason/{id}', 'Driver\DriverController@driverInactiveBannedReason')->name('driver.reason');
	// Route::get('driver/rating-search/{id}', 'Driver\DriverRatingController@searchRating')->name('driver.rating-search');

	// Route::post('driver/rating-alert/{id}', 'Driver\DriverRatingController@driverRatingAlert')->name('driver.rating-alert');
	// //payment

	// Route::get('payment/index', 'Payment\PaymentController@paymentList')->name('payment.index');

	// Route::get('package/sales/report', 'PackageSales\PackageSalesReportController@packageSalesReport')->name('package.sales.report');
	// Route::get('package/sales/report/search', 'PackageSales\PackageSalesReportController@searchPackageSalesReport')->name('package.sales.report.search');
	// Route::get('package/sales/report/details/{id}', 'PackageSales\PackageSalesReportController@packageSalesReportDetails')->name('package.sales.report.details');
	// Route::get('package/sales/report/detail/{id}/{from_date}/{to_date}', 'PackageSales\PackageSalesReportController@packageSalesReportDetailsBySearch')->name('package.sales.report.detail');

	// //ride history
	// Route::get('statement/{type}', 'Statement\StatementController@statement')->name('statement');
	// 	//not use
	// 	Route::get('statement/today', 'Statement\StatementController@statement_today')->name('statement.today');
	// 	Route::get('statement/weekly', 'Statement\StatementController@statement_weekly')->name('statement.weekly');
	// 	Route::get('statement/monthly', 'Statement\StatementController@statement_monthly')->name('statement.monthly');
	// 	Route::get('statement/yearly', 'Statement\StatementController@statement_yearly')->name('statement.yearly');
	// 	//not use
	// Route::post('ajax-statement','Statement\StatementController@ajaxStatement')->name('ajax-statement');
	// //............

	// Route::get('statement/details/{id}/{param}', 'Statement\StatementController@rideDetails')->name('statement.details');

	// Route::get('statement/driver/index', 'Statement\StatementController@driverStatementIndex')->name('statement.driver.index');
	// Route::post('ajax-driver-statement','Statement\StatementController@ajaxDriverStatement')->name('ajax-driver-statement');

	// Route::get('statement/driver/details/{id}', 'Statement\StatementController@driverStatementDetails')->name('statement.driver.details');
	// Route::get('statement/driver/detailsbybid/{id}', 'Statement\StatementController@driverStatementDetailsByBid')->name('statement.driver.detailsbybid');
	
	// Route::resource('promocode', 'Promocode\PromocodeController');

	// Route::get('report-issue/bydriver', 'ReportIssue\ReportIssueController@reportedByDriver')->name('report-issue.bydriver');
	// Route::get('report-issue/bypassenger', 'ReportIssue\ReportIssueController@reportedByPassenger')->name('report-issue.bypassenger');
	// Route::get('report-issue/details/{id}/{param}', 'ReportIssue\ReportIssueController@reportIssueDetails')->name('report-issue.details');

	

});